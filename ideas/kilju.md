
## Recipe
https://moonshiners.club/make-homemade-moonshine-sugar-yeast/

 * 35L fermentation vessel. (with 30L you will probably end up overflow when brewing)
 * 8L+ syrup cooking pot. (8L works, but barely)
 * Thermometer 20C - 100C preferred range, needs to be exact for yeast startup at 35C, and for killing yeast at 50C. liquid temperature measurement.
 * large enought cooking pot to boil the wash (when killing the yeast).
 * aquarium heater, to keep the yeast and fermantation going on, preferred temperature 30C - 35C

### Guides / Sources
 * https://www.reddit.com/r/firewater/comments/cv4bu8/methanol_some_information/
 * https://www.reddit.com/r/firewater/
 * https://homedistiller.org/forum/viewtopic.php?f=63&t=13261
 * http://www.artisan-distiller.net/phpBB3/viewtopic.php?f=59&t=8707
 * https://www.reddit.com/r/firewater/comments/f9kllw/my_first_stripping_run_im_sort_of_having_trouble/

### ingredients
All the ingredients are available in local super markets (citymarket / prisma).

Citric acid is cheaper to buy from spice racks (E330), rather than from the beer/alchohol parts of the store (its the same citric acid, just cheaper).

I might end up buying bentonite clay from ruohonjuuri.. but might need better source for clarification of the product.
https://www.ruohonjuuri.fi/bentoniittisavi-fitnes-6430048231045

### Batches

#### #3

_22.3.2020_ day0:

* Followed the instructions.
* Inverted sugars method. heated the syrup for an hour, it really looked very well this time.
* Used only 20Liters of cold water. added syrup on it, mixed yeast.
* heater on from day 0.
* gravity meter showed 1.096, which should produce 10-15%.
Seems like everything went well.

#### #2

_13.3.2020_ day0:

* Followed the instructions.
* Inverted sugars method. 
* Used only 20Liters of cold water. added syrup on it, mixed yeast.
* heater on from day 0.
Seems like everything went well, I think I just need to buy bigger wessel for this recipe.

I think doing everything in washing room/sauna, makes things easier, as it there is not so much worry about splash damage on the floor.

The gravity meter showed +4, before adding syrup, and after adding syrup it indicated +92.

according to: https://www.clawhammersupply.com/blogs/moonshine-still-blog/12044309-how-to-use-a-hydrometer

if the calculations go like last time:

1.092 minus  1.004 equals 0.088.

0.088 X 131= ‭11,528‬

Our wash is 11,528‬% alcohol.

this means: 11,528‬% * 28L = ‭3,22784‬L of pure alcohol.

_20.3.2020_ day7:

I've stirred the container from day to day, to relief the gasses. Its still bubbling a bit. I am going to mix the bentonite now, in ahead of time, to be able to have smooth bentonite mixture when needed. "2-3 tbsp of bentonite in a coffee-grinder and dissolve it in 250 ml of warm water.".

_21.3.2020_ day8:

The bentonite was really smooth mixture.. nice.. The container was still bubbling. but I mixed bentonite in.

_22.3.2020_ day9:

I transferred the liquid to another container.. let it settle down in that.. I needed the fermentation container to next batch.

#### #1

_27.2.2020_ day0:

Followed the instructions.
Inverted sugars method. 
Used only 20Liters of water, as the 30Liter container cant hold 24liters + 6kg sugar + 3liters of water.

Boiled 4.5 liters of water, and used 15.5 liters of cold water to produce 20liters of room temperature water -> might be mistake.

Boiled the syrup on 8L container, JUST barely big enought for the 3L + 6kg of syrup, if it had foamed I would have been in trouble.

mixed Super yeast with warm water (35Celsius warm), it activated nicely in the 0.5L pot that I was using (mixed in a bit of the syrup from 8L pot..).

_28.2.2020_ day1:

The progress had slowed down to stop.. the temperature of 22-23C is too low to keep reaction going. In the evening I opened up, and added aquarium heater (made hole on top of the vessel, probably it still leaks.. meh..) The mixture was still sweet.. I set the heater to max (33C I think, it was designed for 15-50L water tanks). Hopefully by morning, it will be active again. 

_29.2.2020_ day2:

the seal with the heater cord was leaking, I resealed it with silicone and the bubling seemed to commence again

_1.3.2020_ day3:

the seal with the heater cord was leaking again, the silicone didnt seal it fully, in the end.. I got the idea of making a better seal on both sides of the lid. I used a lot more silicone and plastic wrap to ensure tight seal. The bubling continued and it seems the seal holds. The bubbling is really intense, lock releases bubbles every half a second. "3-10 days (usually 4-7 days)", as I have had difficulties, I think the product will be reasy on 5.3 or 6.3 so, 4-5 days still. The smell that comes out is sweet scent ("Inverted sugars give wash fermentation a pleasant caramel scent").

_5.3.2020_ day7:

seal has worked, I've shaken the container occasionally, so that the bubbles get loose from the liquid.. not very much, as the container is totally different kind than what is in the website.. I should really buy those closed bottles, with 35L+ of space, and have couple of them going at the same time.

The bubbling has almost stopped completely, I think I better kill the yeast tomorrow.

_6.3.2020_ day8:

It still is generating some gas, not much.. Maybe bottling tomorrow.

_8.3.2020_ day10:

no bubbling observed. heated to 50C (took 5Gallon ~19L kettle + 8L + 1.5L kettles, to heat all.. I need a bigger kettle.), this hopefully killed the yeast. Added bentonite clay, it becomes sort of organicky when introduced to water, and making it into flowing liquid substance requires some work (I was planning on using electric mixer, but didnt, maybe next time. Ohh and it is small rock particles, so it is abrasive stuff, basically eventually it will ruin the container & tools you whisk it with. oh and it is like concrete, so, all the tools that you used bentonite with, should be washed outside of the plumming system, so that your plumming doesnt get clamped down. I just filled a bucket with water, and rinsed all the equipment there, and dumped the water outside (its natural clay, nature doesnt get affected.)).

Kilju started clearing right away. It should be clear tomorrow, this is inorganic process, so it shouldnt really matter when I bottle the result.
The hydrometer showed 2 units, so .. 4! .. I have no idea what that means. but as I did not measure in the beginning the sugar content, I have no way of calculating alcohol proof.. I speculate it is around 10%.

I am now planning on having it stay till 10.3.2020, tuesday, so it has now 2 days to clear. And bottle it then.

_9.3.2020_ day11:

I emptied the barrel, trying to recover as much of liquid.. although, it might be bad idea to try to save too much from the yeast/clay mixture.. Anyways, I emptied the barrel, semicarefully.. The material at the bottom takes 5L of the 29L canister.. thats a lot.. I probably should prepare bigger batches, so that I can more easily discard enought of the bad stuff, and be left with highquality kilju. sigh..

I am processing everything with heating it 2nd time, to make 100% sure, that the yeast is dead. then I planned to sieve it through fine cloth, to get any random clay/yeast particles filtered.

Active carbon might also be very good idea.

Anyhow, the liquid warms very slowly, around 2-3h to 60C :( ..

_10.3.2020_ day12:

External hot plate, (2000W) heats the kettle sort of nicely, unfortunately the kettle is not true, so it start wobbling in a rhythm, oscillating -> BAD! you dont want warm liquid with mind of its own.

Hypothetically:

The amount of spirit this would produce in a 5 gallon kettle:

1st run (full run), 2 litres of heads &  hearts & tails (Must be a mistake, the first run should be bigger than 2nd). 
2nd run, mixed with heads & tails from 1st, should produce full heads (3dl, of nailpolish), 2-3L of hearts, ?tails?.

in total theoretical pure spirit ~2 - 3L of spirits.

It is not exact science, but the first spirit drops start come out at 80C. water should start circulating after 70-75C.

##### observations
 * you shouldnt boil the 4.5L of water, it reduces oxygen and the 8L of boiling syrup is hot enough to bring the whole 20L of water to temperature for the yeast (my 20L of water actually went 40C+, so withouth boiling any of the water, and using 24L instead, I think the water will come to perfect temperature for mixing the yeast).
 * starting sugar/water ratio should be measured, before fermantation / adding yeast. then it can be calculated how much sugar was consumed by the yeast after the fermentation.
