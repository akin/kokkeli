## World generation
  * No external objects
  * Only natural world simulation
  * Earth
  * Trees
  * Weather

      * Weather areas?
      * Weather voxels?
      * Weather patterns change over time?
#### Boxes
  1. Simulation box
    * Each node is responsible of everything that happens inside the box

#### Layered approach
  1. Base layer
    * Nodes dictating geometry.
    * Objects define single objects in scene
    * Areas defining whole areas contents.
  2. Generation
    * Samplers interpret base layer
    * Samplers areas and populate


  1. base layer, world
    * rules system
      * Roads
      * Rivers
      * Areas what grows where
        * Object generation
          * trees
            * node points, that instruct the existance of certain trees, placed in certain pattern
          * rocks
            * node points, that instruct the existance of certain rocks, placed in certain patterns
          * bushes
            * node points, that instruct the existance of certain rocks, placed in certain patterns
          * rivers?
            * node points, that instruct the existance of certain underground springs, placed in certain patterns
            * Calculation of water collection from the hills/mountains, creating rivers
    * scene graphs
      * objects
      * trees
      * earth
    * volumes
      * weather patters
      * effects
    * primitives
      * spheres
      * planes
      * meshes
      * polygons
      * beziers
  2. Voxel generation, generate voxels of everything
    * fat voxels, voxel component system (ecs inspired)
  4. Simulation
    * Weather
    * Plants
    * Rules
  5. Post object generation, buildings etc.
  6. Simulation

