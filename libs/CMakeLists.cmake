
find_package(stb REQUIRED)

set(GLM_TEST_ENABLE OFF CACHE BOOL "" FORCE)
set(GLM_INSTALL_ENABLE OFF CACHE BOOL "" FORCE)
find_package(glm CONFIG REQUIRED)

set(FMT_TEST OFF)
set(FMT_INSTALL OFF)
set(FMT_DOC OFF)

find_package(fmt CONFIG REQUIRED)
find_package(nlohmann_json REQUIRED)

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/volk")

# unfortunately VOLK does not include the header. :( 
target_include_directories(
	volk 
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/volk"
)
set_target_properties(volk PROPERTIES FOLDER "libs")

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
find_package(glfw3 CONFIG REQUIRED)
