# This Python file uses the following encoding: utf-8
import os
import argparse as arggs
from datetime import datetime
from pathlib import Path
import json

# Generate world
class World():
    def __init__(self):
        pass
    
    def run(self, args):
        # Output world "setup files"
        # Boundaries / Dimensions
        workspace = args["workspace"]
        Path(workspace).mkdir(parents=True, exist_ok=True)
        worldfile_path = os.path.join(workspace, 'world.json')

        currentTime = str(datetime.now())

        data = {}
        data['config'] = {
            'width': args["width"],
            'depth': args["depth"],
            'name': args["name"],
            'units': "meter",
            'created': currentTime,
            'modified': currentTime
        }

        with open(worldfile_path, 'w') as outfile:
            json.dump(data, indent=4, fp=outfile)
        pass

# if this file is "main", handle the arg parsing too..
if __name__ == "__main__": 
    # python.exe world.py -w c:\tmp\world --width 1000 --depth 1000 --name test
    arguments_parser = arggs.ArgumentParser(description='Do world')
    arguments_parser.add_argument('-w','--workspace', help='workspace folder', required=True)

    arguments_parser.add_argument('--width', help='world width', default=1000, type=int, required=True)
    arguments_parser.add_argument('--depth', help='world depth', default=1000, type=int, required=True)
    arguments_parser.add_argument('--name', help='world name', default="default", required=True)

    args = vars(arguments_parser.parse_args())
    model = World()
    model.run(args)
