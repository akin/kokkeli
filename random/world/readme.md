
# World..
I've been thinking of world generation for a while.. years.. and.. I've done some tests on unity.. and.. now my test is to create unix philosophy style world generation..
Small tools, run on dataset, input -> output.

# World simulation
preferrably all data in text format.

1. Generate world.
  * commit to git.
2. Simulate world
  * commit to git, after each run.

World events can refer to git commits, where the effect began.. When effect fades out of the world, then the "effect item" can also be removed from the simulation/scene.

