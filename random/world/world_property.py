# This Python file uses the following encoding: utf-8
import os
import argparse as arggs
from datetime import datetime
from pathlib import Path
import json

# Generate world
class WorldProperty():
    def __init__(self):
        pass
    
    def run(self, args):
        # add remove/modify/properties of world
        workspace = args["workspace"]
        Path(workspace).mkdir(parents=True, exist_ok=True)
        worldfile_path = os.path.join(workspace, 'world.json')

        currentTime = str(datetime.now())

        data = None
        with open(worldfile_path) as json_file:
            data = json.load(json_file)

        if data == None:
            raise Exception("Failed to load workspace")

        if "properties" not in data:
            data["properties"] = {}
        properties = data["properties"]
        
        properties[args["key"]] = args["value"]

        with open(worldfile_path, 'w') as outfile:
            json.dump(data, indent=4, fp=outfile)
        pass

# if this file is "main", handle the arg parsing too..
if __name__ == "__main__": 
    # python.exe world_property.py -w c:\tmp\world --key foo --value test
    arguments_parser = arggs.ArgumentParser(description='Do world')
    arguments_parser.add_argument('-w','--workspace', help='workspace folder', required=True)

    arguments_parser.add_argument('--key', required=True)
    arguments_parser.add_argument('--value', required=False)

    args = vars(arguments_parser.parse_args())
    model = WorldProperty()
    model.run(args)
