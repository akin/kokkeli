from qtpynodeeditor import (NodeData, NodeDataModel, NodeDataType, PortType, Port, NodeValidationState)
from qtpy.QtWidgets import QFileDialog, QWidget, QLineEdit

from models.DataTypes import ColorData, PathData


class MaterialModel(NodeDataModel):
    input_node_names = ["color", "metalness", "specular", "specular", "roughness", "normal", "emissive", "glossiness", "heightmap"]
    output_node_names = []

    # Data required by NodeDataModel
    num_ports = {PortType.input: len(input_node_names), PortType.output: len(output_node_names)}
    name = "Material"
    caption_visible = True
    port_caption = None
    data_type = NodeDataType("material", "Material")

    def __new__(cls, *args, **kwargs):
        """Creating Instance"""
        # overriding __new__ due to use of __init_subclass__ in super class.. which required _everything_ to be in place
        # before __init__.
        instance = super(MaterialModel, cls).__new__(cls, *args, **kwargs)
        # construct the object
        instance.port_caption = {
            PortType.input: {index: instance.input_node_names[index] for index in range(0, len(instance.input_node_names))},
            PortType.output: {index: instance.output_node_names[index] for index in range(0, len(instance.output_node_names))}
        }
        instance.port_caption_visible = {
            PortType.input: {index: True for index in range(0, len(instance.input_node_names))},
            PortType.output: {index: True for index in range(0, len(instance.output_node_names))}
        }
        return instance

    def __init__(self, style=None, parent=None):
        super().__init__(style=style, parent=parent)
        self._edit = QLineEdit()
        self._data = None

        self._input = {}
        for strtype in self.input_node_names:
            if strtype == "color":
                self._input[strtype] = ColorData()
            else:
                self._input[strtype] = PathData()

    def save(self) -> dict:
        """Add to the JSON dictionary to save the state of the NumberSource"""
        doc = super().save()

        input_data = {}

        for strtype in self.input_node_names:
            input_data[strtype] = self._input[strtype].save()

        doc['material'] = {
            'input': input_data
        }

        return doc

    def restore(self, state: dict):
        """Restore the number from the JSON dictionary"""
        try:
            value = state["material"]
        except Exception:
            ...
        else:
            ...
#            well I think it should unpack the json .. and give it to _data
#            self.set_texture(str(value))

    def out_data(self, port: int) -> NodeData:
        return self._data

    def set_in_data(self, data: NodeData, port: Port):
        key = self.input_node_names[port.index]
        self._input[key] = data

        self._validation_state = NodeValidationState.valid
        self._validation_message = 'Yeaaahhhhh'
        #self._label.setText(self._number.number_as_text())
        """
        if number_ok:
            self._validation_state = NodeValidationState.valid
            self._validation_message = ''
            self._label.setText(self._number.number_as_text())
        else:
            self._validation_state = NodeValidationState.warning
            self._validation_message = "Missing or incorrect inputs"
            self._label.clear()

        self._label.adjustSize()
        """

    def embedded_widget(self) -> QWidget:
        return self._edit


