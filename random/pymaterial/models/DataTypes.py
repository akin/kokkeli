
from qtpy.QtGui import QColor
import threading
from qtpynodeeditor import (NodeData)


class ColorData(NodeData):
    def __init__(self, color: QColor = QColor(0, 128, 128)):
        self._color = color
        self._lock = threading.RLock()

    @property
    def lock(self):
        return self._lock

    @property
    def color(self) -> QColor:
        """access color"""
        return self._color

    def save(self) -> dict:
        """Add to the JSON dictionary to save the state of the NumberSource"""
        doc = {'red': self._color.red(),
               'green': self._color.green(),
               'blue': self._color.blue(),
               'alpha': self._color.alpha()}
        return doc


class PathData(NodeData):
    def __init__(self, path: str = None):
        self._path = path
        self._lock = threading.RLock()

    @property
    def lock(self):
        return self._lock

    @property
    def path(self) -> str:
        return self._path

    def path(self, path: str):
        self._path = path

    def save(self) -> str:
        return self._path
