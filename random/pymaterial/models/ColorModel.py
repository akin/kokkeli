
from qtpy.QtGui import QColor
from qtpynodeeditor import (NodeData, NodeDataModel, PortType, NodeDataType)
from qtpy.QtWidgets import QColorDialog, QWidget, QToolButton
from models.DataTypes import ColorData


class ColorModel(NodeDataModel):
    name = "Color"
    caption_visible = False
    num_ports = {PortType.input: 0, PortType.output: 1}
    port_caption = {'output': {0: 'Result'}}
    data_type = NodeDataType("color", "Color")

    def __init__(self, style=None, parent=None):
        super().__init__(style=style, parent=parent)
        self._data = ColorData(QColor(0, 128, 255))
        self._color_edit = QToolButton()
        self._color_edit.setMaximumSize(25, 25)
        self._color_edit.setMinimumSize(25, 25)
        self._color_edit.setStyleSheet("background-color: rgb({}, {}, {});".format(self._data.color.red(), self._data.color.green(), self._data.color.blue()))
        self._color_edit.clicked.connect(self.on_button_clicked)

    @property
    def color(self):
        return self._data

    def save(self) -> dict:
        """Add to the JSON dictionary to save the state of the NumberSource"""
        doc = super().save()
        if self._data:
            doc['color'] = self._data.save()
        return doc

    def restore(self, state: dict):
        """Restore the number from the JSON dictionary"""
        try:
            value = QColor(state["color"])
        except Exception:
            ...
        else:
            self.set_color(value)

    def out_data(self, port: int) -> NodeData:
        return self._data

    def embedded_widget(self) -> QWidget:
        return self._color_edit

    def on_button_clicked(self):
        """
        Color item was clicked on the ui

        Parameters
        ----------
        """
        color = QColorDialog.getColor(self._data.color)
        if color.isValid():
            self.set_color(color)

    def set_color(self, color: QColor):
        """
        set color to widget

        Parameters
        ----------
        :param color: QColor
        """
        self._data.color = color
        self._color_edit.setStyleSheet("background-color: rgb({}, {}, {});".format(self._data.color.red(), self._data.color.green(), self._data.color.blue()))
