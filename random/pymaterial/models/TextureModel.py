
from qtpy.QtCore import QSize
from qtpy.QtGui import QIcon
from qtpynodeeditor import (NodeData, NodeDataModel, PortType, NodeDataType)
from qtpy.QtWidgets import QFileDialog, QWidget, QToolButton
from models.DataTypes import PathData


class TextureModel(NodeDataModel):
    name = "Texture"
    caption_visible = False
    num_ports = {PortType.input: 0, PortType.output: 1}
    port_caption = {'output': {0: 'Result'}}
    data_type = NodeDataType("texture", "Texture")
    size = QSize(100, 100)

    def __init__(self, style=None, parent=None):
        super().__init__(style=style, parent=parent)
        self._data = PathData()
        self._path_edit = QToolButton()
        self._path_edit.setMaximumSize(self.size)
        self._path_edit.setMinimumSize(self.size)
        self._path_edit.clicked.connect(self.on_button_clicked)

    def set_texture(self, path):
        self._data.path = path
        icon = QIcon(self._data.path)
        self._path_edit.setIcon(icon)
        self._path_edit.setIconSize(self.size)

    @property
    def path(self):
        return self._path

    def save(self) -> dict:
        """Add to the JSON dictionary to save the state of the NumberSource"""
        doc = super().save()
        if self._data:
            doc['texture'] = self._data.save()
        return doc

    def restore(self, state: dict):
        """Restore the number from the JSON dictionary"""
        try:
            value = state["texture"]
        except Exception:
            ...
        else:
            self.set_texture(str(value))

    def out_data(self, port: int) -> NodeData:
        return self._data

    def embedded_widget(self) -> QWidget:
        return self._path_edit

    def on_button_clicked(self):
        filename, _ = QFileDialog.getOpenFileName(None, 'Open file', 'c:\\', "Image files (*.jpg *.gif *.png)")

        if filename:
            self.set_texture(filename)
