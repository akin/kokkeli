# pip install qtpy qtpynodeeditor PyQt5

from qtpy.QtWidgets import QApplication
from NodeEditor import *
import sys

sys.path.append("../../modules/mop/generated")

#from Mop import Material

app = QApplication([])
editor = NodeEditor()

editor.getView().setWindowTitle("Material Editor V667.9")
editor.getView().resize(800, 600)

node3 = editor.createNode(ColorModel)
node3.position = 100, 200

node4 = editor.createNode(TextureModel)
node4.position = 100, 300

node4 = editor.createNode(MaterialModel)
node4.position = 300, 400

#editor.getScene().create_connection(node[PortType.output][0], node2[PortType.input][0])

editor.getView().show()
app.exec_()
