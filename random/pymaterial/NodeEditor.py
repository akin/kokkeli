
from models.ColorModel import *
from models.TextureModel import *
from models.MaterialModel import *

import qtpynodeeditor as nodeeditor

class NodeEditor:
    """Class to control all Node editor stuffs"""
    def __init__(self, parent=None):
        self._registry = nodeeditor.DataModelRegistry()

        # Register models
        self._registry.register_model(ColorModel, category='Source', style=None)
        self._registry.register_model(TextureModel, category='Source', style=None)
        self._registry.register_model(MaterialModel, category='Input', style=None)

        self._scene = nodeeditor.FlowScene(registry=self._registry)
        self._view = nodeeditor.FlowView(scene=self._scene, parent=parent)

    def getView(self):
        return self._view

    def createNode(self, type):
        return self._scene.create_node(type)

    def getScene(self):
        return self._scene
