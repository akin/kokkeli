#  PyMaterial
this is more of a prototype project for creation of tool to create materials in visual way. drag n drop.

![alt text](shot.png "work in progress")

## Progress
 * nodegraph works, somewhat (even though python is all the time in the way).
should use [mop project](../../modules/mop/readme.md) flatbuffers format.

## TODO
 * dialogs to start in correct directories (last used directory or something, or project root)
 * Texture node to have more options (data type etc. conversion nodes?)
 * Material node to have shader specifications etc.
 * save graph
 * load graph
 * export MOP flatbuffers

## Dependencies
 * qtpy
 * qtpynodeeditor
 * PyQt5
 * (probably others, which Ill have to figure out later, what)
 
```
pip install qtpy qtpynodeeditor PyQt5
```

