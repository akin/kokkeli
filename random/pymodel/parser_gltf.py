# This Python file uses the following encoding: utf-8
import os
from model import *
from gltflib import *

# https://pypi.org/project/gltflib/
class ParserGltf():
    def load(path):
        fullPath = os.path.join(os.path.dirname(__file__),path)
        file = GLTF.load(fullPath)

        if file == None:
            return None
        if file.model == None:
            return None

        data = ModelData()
        data.setData("buffers", importBuffers(file.model))
        data.setData("bufferViews", importBufferViews(file.model))
        data.setData("images", importImages(file.model))
        data.setData("samplers", importSamplers(file.model))
        data.setData("materials", importMaterials(file.model))
        data.setData("scenes", importScenes(file.model))
        data.setData("animations", importAnimations(file.model))
        
        return data
    
    def save(model, path):
        pass
    
    def getSkinCount(self):
        if not self.validate():
            return 0
        return self.getCount(self.file.model.skins)
    
    def getExtensionsRequired(self):
        if not self.validate():
            return None
        return self.getItems(self.file.model.extensionsRequired)
    
    def getExtensionsUsed(self):
        if not self.validate():
            return None
        return self.getItems(self.file.model.extensionsUsed)

#define GL_BOOL                           0x8B56
#define GL_BYTE                           0x1400
#define GL_UNSIGNED_BYTE                  0x1401
#define GL_SHORT                          0x1402
#define GL_UNSIGNED_SHORT                 0x1403
#define GL_INT                            0x1404
#define GL_UNSIGNED_INT                   0x1405
#define GL_FLOAT                          0x1406
#define GL_HALF_FLOAT                     0x140B
#define GL_DOUBLE                         0x140A
gltypes = {
    0x8B56: "bool",
    0x1400: "sint8",
    0x1401: "uint8",
    0x1402: "sint16",
    0x1403: "uint16",
    0x1404: "sint32",
    0x1405: "uint32",
    0x1406: "float32",
    0x140B: "float16",
    0x140A: "float64",
}
glformats = {
    "SCALAR": "scalar",
    "VEC2": "vec2",
    "VEC3": "vec3",
    "VEC4": "vec4",
}

def importCommon(item, index, out):
    out.setIndex(index)
    if item.name != None:
        out.setName(item.name)
    if item.extensions != None:
        out.setExtensions(item.extensions)
    if item.extras != None:
        out.setExtras(item.extras)

def importBuffers(model):
    if model.buffers == None:
        return None
    data = []
    src = model.buffers
    for index in range(len(src)):
        item = src[index]
        out = ModelBuffer()
        
        importCommon(item, index, out)
        if item.uri != None:
            out.setUri(item.uri)
        out.setByteSize(item.byteLength)

        data.append(out)
    return data

def importBufferViews(model):
    if model.bufferViews == None:
        return None
    data = []
    src = model.bufferViews
    for index in range(len(src)):
        item = src[index]
        out = ModelBufferView()
        
        importCommon(item, index, out)
        if item.buffer != None:
            out.setBufferIndex(item.buffer)
        if item.byteOffset != None:
            out.setByteOffset(item.byteOffset)
        if item.byteLength != None:
            out.setByteSize(item.byteLength)
        if item.byteStride != None:
            out.setByteStride(item.byteStride)
        if item.target != None:
            out.setTarget(item.target)

        data.append(out)
    return data

def importImages(model):
    if model.images == None:
        return None
    data = []
    src = model.images
    for index in range(len(src)):
        item = src[index]
        out = ModelImage()
        
        importCommon(item, index, out)
        if item.uri != None:
            out.setUri(item.uri)
        if item.mimeType != None:
            out.setMime(item.mimeType)
        if item.bufferView != None:
            out.setBufferViewIndex(item.bufferView)

        data.append(out)
    return data

def importSamplers(model):
    if model.samplers == None:
        return None
    data = []
    src = model.samplers
    for index in range(len(src)):
        item = src[index]
        out = ModelSampler()
        
        importCommon(item, index, out)
        if item.magFilter != None:
            out.setMag(item.magFilter)
        if item.minFilter != None:
            out.setMin(item.minFilter)
        if item.wrapS != None:
            out.setWrapS(item.wrapS)
        if item.wrapT != None:
            out.setWrapT(item.wrapT)

        data.append(out)
    return data

def importMaterials(model):
    data = []
    src = model.materials
    for index in range(len(src)):
        item = src[index]
        out = ModelMaterial()
        
        importCommon(item, index, out)
        importMaterial(model, item, out)

        data.append(out)
    return data

def importMaterial(model, material, out):
    # Material is constructed of multitude of BULLSHIT items.
    # METAL!
    if material.pbrMetallicRoughness != None:
        src = material.pbrMetallicRoughness
        if src.baseColorFactor != None or src.baseColorTexture != None:
            base = ModelMaterialProperty()
            base.setName("base")

            if src.baseColorFactor != None:
                base.setFactor(src.baseColorFactor)

            if src.baseColorTexture != None:
                texture = model.textures[src.baseColorTexture.index]
                base.setTextureCoordinateIndex(0)
                if src.baseColorTexture.texCoord != None:
                    base.setTextureCoordinateIndex(src.metallicRoughnessTexture.texCoord)
                if texture.source != None:
                    base.setImageIndex(texture.source)
                if texture.sampler != None:
                    base.setSamplerIndex(texture.sampler)
            out.addProperty(base)
        if src.roughnessFactor != None or src.metallicFactor != None or src.metallicRoughnessTexture != None:
            metal = ModelMaterialProperty()
            metal.setName("metalness")
            metal.setChannel("blue")

            if src.metallicFactor != None:
                metal.setFactor(src.metallicFactor)

            rough = ModelMaterialProperty()
            rough.setName("roughness")
            rough.setChannel("green")

            if src.roughnessFactor != None:
                rough.setFactor(src.roughnessFactor)

            if src.metallicRoughnessTexture != None:
                texture = model.textures[src.metallicRoughnessTexture.index]
                metal.setTextureCoordinateIndex(0)
                rough.setTextureCoordinateIndex(0)
                if src.metallicRoughnessTexture.texCoord != None:
                    metal.setTextureCoordinateIndex(src.metallicRoughnessTexture.texCoord)
                    rough.setTextureCoordinateIndex(src.metallicRoughnessTexture.texCoord)
                if texture.source != None:
                    metal.setImageIndex(texture.source)
                    rough.setImageIndex(texture.source)
                if texture.sampler != None:
                    metal.setSamplerIndex(texture.sampler)
                    rough.setSamplerIndex(texture.sampler)
            out.addProperty(metal)
            out.addProperty(rough)
    
    # NORMAL!
    if material.normalTexture != None:
        normal = ModelMaterialProperty()
        normal.setName("normal")
        src = material.normalTexture
        normal.setTextureCoordinateIndex(0)
        if src.texCoord != None:
            normal.setTextureCoordinateIndex(src.texCoord)
        if src.scale != None:
            normal.setScale(src.scale)
        texture = model.textures[src.index]
        if texture.source != None:
            normal.setImageIndex(texture.source)
        if texture.sampler != None:
            normal.setSamplerIndex(texture.sampler)
        out.addProperty(normal)
    
    # OCCLUSION!
    if material.occlusionTexture != None:
        occlusion = ModelMaterialProperty()
        occlusion.setName("occlusion")
        src = material.occlusionTexture
        occlusion.setTextureCoordinateIndex(0)
        if src.texCoord != None:
            occlusion.setTextureCoordinateIndex(src.texCoord)
        if src.strength != None:
            normal.setScale(src.strength)
        texture = model.textures[src.index]
        if texture.source != None:
            occlusion.setImageIndex(texture.source)
        if texture.sampler != None:
            occlusion.setSamplerIndex(texture.sampler)
        out.addProperty(occlusion)
    
    # EMISSIVIE!
    if material.emissiveTexture != None or material.emissiveFactor != None:
        emissive = ModelMaterialProperty()
        emissive.setName("emissive")
        if material.emissiveTexture != None:
            src = material.emissiveTexture
            emissive.setTextureCoordinateIndex(0)
            if src.texCoord != None:
                emissive.setTextureCoordinateIndex(src.texCoord)
            texture = model.textures[src.index]
            if texture.source != None:
                emissive.setImageIndex(texture.source)
            if texture.sampler != None:
                emissive.setSamplerIndex(texture.sampler)
        if material.emissiveFactor != None:
            normal.setFactor(material.emissiveFactor)
        out.addProperty(emissive)
    
    if material.alphaMode != None:
        out.setMode(material.alphaMode)
    if material.alphaCutoff != None:
        out.setAlphaCutoff(material.alphaCutoff)
    if material.doubleSided != None:
        out.setDoubleSided(material.doubleSided)

def importMeshes(model):
    if model.meshes == None:
        return None
    data = []
    for index in range(len(model.meshes)):
        data.append(importMesh(model, index))
    return data

def importMesh(model, index):
    out = ModelMesh()
    mesh = model.meshes[index]
    importCommon(mesh, index, out)
    for src in mesh.primitives:
        primitive = ModelPrimitive()
        if src.mode != None:
            # https://github.com/KhronosGroup/OpenGL-Registry/blob/master/api/GL/glcorearb.h
            # #define GL_POINTS 0x0000 ...
            if src.mode == 0:
                primitive.setMode("points")
            elif src.mode == 1:
                primitive.setMode("lines")
            elif src.mode == 4:
                primitive.setMode("triangles")
            else:
                print("ERROR, wtf gl")
        if src.material != None:
            primitive.setMaterialIndex(src.material)
        if src.indices != None:
            primitive.setIndices(importAttribute("indices", model.accessors[src.indices]))
        if src.attributes != None:
            if src.attributes.POSITION:
                primitive.addAttribute(importAttribute("position", model.accessors[src.attributes.POSITION]))
            if src.attributes.NORMAL:
                primitive.addAttribute(importAttribute("normal", model.accessors[src.attributes.NORMAL]))
            if src.attributes.TANGENT:
                primitive.addAttribute(importAttribute("tangent", model.accessors[src.attributes.TANGENT]))
            if src.attributes.TEXCOORD_0:
                primitive.addAttribute(importAttribute("texcoord0", model.accessors[src.attributes.TEXCOORD_0]))
            if src.attributes.TEXCOORD_1:
                primitive.addAttribute(importAttribute("texcoord1", model.accessors[src.attributes.TEXCOORD_1]))
            if src.attributes.COLOR_0:
                primitive.addAttribute(importAttribute("color", model.accessors[src.attributes.COLOR_0]))
            if src.attributes.JOINTS_0:
                primitive.addAttribute(importAttribute("joint", model.accessors[src.attributes.JOINTS_0]))
            if src.attributes.WEIGHTS_0:
                primitive.addAttribute(importAttribute("weight", model.accessors[src.attributes.WEIGHTS_0]))
        out.addPrimitive(primitive)
    return out

def importAttribute(name, accessor):
    result = ModelAttribute()
    importCommon(accessor, None, result)
    if accessor.bufferView != None:
        result.setBufferViewIndex(accessor.bufferView)
    if accessor.byteOffset != None:
        result.setByteOffset(accessor.byteOffset)
    if accessor.count != None:
        result.setCount(accessor.count)
    
    # name override
    if name != None:
        result.setName(name)

    components = "unknown"
    format = "unknown"
    if accessor.componentType in gltypes:
        components = gltypes[accessor.componentType]
    if accessor.type in glformats:
        format = glformats[accessor.type]
    
    result.setFormat(format + ":" + components)

    return result

def importCamera(model, index):
    out = ModelCamera()
    camera = model.cameras[index]
    importCommon(camera, index, out)

    if camera.orthographic != None:
        out.setMode("ortho")
        mode = camera.orthographic
        if mode.znear != None:
            out.setNear(mode.znear)
        if mode.zfar != None:
            out.setFar(mode.zfar)
        if mode.xmag != None:
            out.setXMagnification(mode.xmag)
        if mode.ymag != None:
            out.setYMagnification(mode.ymag)
    if camera.perspective != None:
        out.setMode("perspective")
        mode = camera.perspective
        if mode.znear != None:
            out.setNear(mode.znear)
        if mode.zfar != None:
            out.setFar(mode.zfar)
        if mode.aspectRatio != None:
            out.setAspectRatio(mode.aspectRatio)
        if mode.yfov != None:
            out.setFov(mode.yfov)
    return out

def importScenes(model):
    if model.scenes == None:
        return None
    data = []
    src = model.scenes
    for index in range(len(src)):
        item = src[index]
        out = ModelScene()
        importCommon(item, index, out)
        out.setNodes(importNodes(model, item.nodes))
        data.append(out)
    return data

def importNodes(model, indexes):
    if indexes == None or len(indexes) == 0:
        return None
    nodes = []
    for index in indexes:
        nodes.append(importNode(model, index))
    return nodes

def importNode(model, index):
    target = ModelNode()
    src = model.nodes[index]

    if src.translation != None:
        target.setPosition(src.translation)
    if src.rotation != None:
        target.setRotation(src.rotation)
    if src.scale != None:
        target.setScale(src.scale)
    if src.mesh != None:
        target.setMesh(importMesh(model, src.mesh))
    if src.camera != None:
        target.setCamera(importCamera(model, src.camera))
    
    if src.children != None:
        target.setNodes(importNodes(model, src.children))
    return target

def importAnimations(model):
    if model.animations == None:
        return None
    
    data = []
    src = model.animations
    for index in range(len(src)):
        item = src[index]
        out = ModelAnimation()
        importCommon(item, index, out)
        out.setChannels(importChannels(model, item.channels))
        out.setSamplers(importAnimationSamplers(model, item.samplers))
        data.append(out)
    return data

def importChannels(model, channels):
    if channels == None or len(channels) == 0:
        return None
    out = []
    for channel in channels:
        out.append(importChannel(model, channel))
    return out

def importChannel(model, src):
    target = ModelAnimationChannel()
    target.setSampler(src.sampler)
    if src.target != None:
        target.setTargetPath(src.target.path)
        if src.target.node != None:
            target.setNodeIndex(src.target.node)
    return target

def importAnimationSamplers(model, samplers):
    if samplers == None or len(samplers) == 0:
        return None
    out = []
    for sampler in samplers:
        out.append(importAnimationSampler(model, sampler))
    return out

def importAnimationSampler(model, src):
    target = ModelAnimationSampler()
    if src.interpolation != None:
        target.setInterpolation(src.interpolation)
    
    if src.input != None:
        target.setInputAttribute(importAttribute("input", model.accessors[src.input]))
    if src.output != None:
        target.setOutputAttribute(importAttribute("output", model.accessors[src.output]))

    return target

