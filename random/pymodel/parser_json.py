# This Python file uses the following encoding: utf-8
import os, json
from model import *

exporters = {}

class ParserJson():
    def load(path):
        return None
    
    def save(model, path):
        fullPath = os.path.join(os.path.dirname(__file__), path)
        if type(model) in exporters:
            exp = exporters[type(model)](model)
        else:
            print("unknown type " + str(model))
    
        with open(fullPath, 'w') as outfile:
            json.dump(exp, outfile, sort_keys=True, indent=4)

def exportModelData(item):
    ser = {}
    data = item.getData()
    for key in data:
        if data[key] == None:
            continue
        arr = []
        for item in data[key]:
            if type(item) in exporters:
                arr.append(exporters[type(item)](item))
            else:
                print("unknown type " + str(item))
        ser[key] = arr
    return ser
exporters[ModelData] = exportModelData

def exportModelCommon(item):
    ser = {}
    if item.getIndex() != None:
        ser["index"] = item.getIndex()
    if item.getName() != None:
        ser["name"] = item.getName()
    if item.getExtensions() != None:
        ser["extensions"] = item.getExtensions()
    if item.getExtras() != None:
        ser["extras"] = item.getExtras()
    return ser
exporters[ModelCommon] = exportModelCommon

def exportModelBuffer(item):
    ser = exportModelCommon(item)
    if item.getUri() != None:
        ser["uri"] = item.getUri()
    ser["size"] = item.getByteSize()
    return ser
exporters[ModelBuffer] = exportModelBuffer

def exportModelBufferView(item):
    ser = exportModelCommon(item)
    ser.update({
        "bufferIndex": item.getBufferIndex(),
        "offset": item.getByteOffset(),
        "size": item.getByteSize()
    })
    if item.getByteStride() != None:
        ser["stride"] = item.getByteStride()
    if item.getTarget() != None:
        ser["target"] = item.getTarget()
    return ser
exporters[ModelBufferView] = exportModelBufferView

def exportModelImage(item):
    ser = exportModelCommon(item)
    if item.getUri() != None:
        ser["uri"] = item.getUri()
    if item.getMime() != None:
        ser["mime"] = item.getMime()
    if item.getBufferViewIndex() != None:
        ser["bufferViewIndex"] = item.getBufferViewIndex()
    return ser
exporters[ModelImage] = exportModelImage

def exportModelSampler(item):
    ser = exportModelCommon(item)
    if item.getMag() != None:
        ser["magFilter"] = item.getMag()
    if item.getMin() != None:
        ser["minFilter"] = item.getMin()
    if item.getWrapS() != None:
        ser["wrapS"] = item.getWrapS()
    if item.getWrapT() != None:
        ser["wrapT"] = item.getWrapT()
    return ser
exporters[ModelSampler] = exportModelSampler

def exportModelMaterialProperty(item):
    ser = exportModelCommon(item)
    if item.getImageIndex() != None:
        ser["imageIndex"] = item.getImageIndex()
    if item.getSamplerIndex() != None:
        ser["samplerIndex"] = item.getSamplerIndex()
    if item.getTextureCoordinateIndex() != None:
        ser["textureCoordinateIndex"] = item.getTextureCoordinateIndex()
    if item.getChannel() != None:
        ser["channel"] = item.getChannel()
    if item.getMode() != None:
        ser["mode"] = item.getMode()
    if item.getFactor() != None:
        ser["factor"] = item.getFactor()
    if item.getScale() != None:
        ser["scale"] = item.getScale()
    return ser 
exporters[ModelMaterialProperty] = exportModelMaterialProperty

def exportModelMaterial(item):
    ser = exportModelCommon(item)
    for property in item.getProperties():
        ser[property.getName()] = exportModelMaterialProperty(property)
    return ser
exporters[ModelMaterial] = exportModelMaterial

def exportModelMaterial(item):
    ser = exportModelCommon(item)
    for property in item.getProperties():
        ser[property.getName()] = exportModelMaterialProperty(property)
    if item.getDoubleSided() != None:
        ser["doubleSided"] = item.getDoubleSided()
    if item.getMode() != None:
        ser["mode"] = item.getMode()
    if item.getAlphaCutoff() != None:
        ser["alphaCutOff"] = item.getAlphaCutoff()
    return ser
exporters[ModelMaterial] = exportModelMaterial

def exportModelCamera(item):
    ser = exportModelCommon(item)
    if item.getMode() != None:
        ser["mode"] = item.getMode()
        if item.getMode() == "ortho":
            ser.update({
                "xMagnification": item.getXMagnification(),
                "yMagnification": item.getYMagnification(),
            })
        else:
            ser.update({
                "fov": item.getFov(),
                "aspectRatio": item.getAspectRatio(),
            })
    return ser
exporters[ModelCamera] = exportModelCamera

def exportModelAttribute(item):
    ser = exportModelCommon(item)
    if item.getBufferViewIndex() != None:
        ser["bufferViewIndex"] = item.getBufferViewIndex()
    if item.getByteOffset() != None:
        ser["offset"] = item.getByteOffset()
    if item.getCount() != None:
        ser["count"] = item.getCount()
    if item.getFormat() != None:
        ser["format"] = item.getFormat()
    return ser
exporters[ModelAttribute] = exportModelAttribute

def exportModelPrimitive(item):
    ser = exportModelCommon(item)
    if item.getMode() != None:
        ser["mode"] = item.getMode()
    if item.getIndices() != None:
        ser["indices"] = exportModelAttribute(item.getIndices())
    if item.getAttributes() != None:
        atr = []
        for attribute in item.getAttributes():
            atr.append(exportModelAttribute(attribute))
        ser["attributes"] = atr
    if item.getMaterialIndex() != None:
        ser["materialIndex"] = item.getMaterialIndex()
    return ser
exporters[ModelPrimitive] = exportModelPrimitive

def exportModelMesh(item):
    ser = exportModelCommon(item)
    primitives = []
    for primitive in item.getPrimitives():
        primitives.append(exportModelPrimitive(primitive))
    if len(primitives) > 0:
        ser["primitives"] = primitives
    return ser
exporters[ModelMesh] = exportModelMesh

def exportModelNode(item):
    ser = exportModelCommon(item)
    nodes = []
    for node in item.getNodes():
        nodes.append(exportModelNode(node))
    ser.update({
        "position": item.getPosition(),
        "rotation": item.getRotation(),
        "scale": item.getScale()
    })
    if item.getCamera() != None:
        ser["camera"] = exportModelCamera(item.getCamera())
    if item.getMesh() != None:
        ser["mesh"] = exportModelMesh(item.getMesh())
    if len(nodes) > 0:
        ser["nodes"] = nodes
    return ser
exporters[ModelNode] = exportModelNode

def exportOutModelScene(item):
    ser = exportModelCommon(item)
    nodes = []
    for node in item.getNodes():
        nodes.append(exportModelNode(node))
    ser["nodes"] = nodes
    return ser
exporters[ModelScene] = exportOutModelScene
    