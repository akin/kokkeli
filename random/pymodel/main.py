# This Python file uses the following encoding: utf-8
import os
import argparse as arggs
from parser_gltf import ParserGltf
from parser_json import ParserJson

# python main.py -i DamagedHelmet.glb -o tmp.json

arguments_parser = arggs.ArgumentParser(description='Do model conversion/debug')
arguments_parser.add_argument('-i','--in', help='input file', required=True)
arguments_parser.add_argument('-o','--out', help='output file', required=False)
args = vars(arguments_parser.parse_args())

parsers = {}
parsers["gltf"] = ParserGltf
parsers["glb"] = ParserGltf
parsers["json"] = ParserJson

# typical python, missing features, no switch-case..
def parsePathInfo(path):
    _, ext = os.path.splitext(path)
    if len(ext) > 0:
        ext = ext.lower()[1:]
    return path, ext

input, inputExtension = parsePathInfo(args['in'])
if inputExtension in parsers:
    model = parsers[inputExtension].load(input)

output, outputExtension = parsePathInfo(args['out'])
if outputExtension in parsers:
    parsers[outputExtension].save(model, output)
