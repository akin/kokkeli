# This Python file uses the following encoding: utf-8
class ModelData():
    def __init__(self):
        self.data = {}

    def setData(self, key, value):
        self.data[key] = value
    
    def getData(self):
        return self.data

class ModelCommon():
    def __init__(self):
        self.name = None
        self.index = None
        self.extensions = None
        self.extras = None
        self.raw = None
                                 
    def getName(self):                                                                                   
        return self.name                                                                               
                                                                                                    
    def setName(self,val):                                                                                
        self.name = val  
                                 
    def getExtensions(self):                                                                                   
        return self.extensions                                                                               
                                                                                                    
    def setExtensions(self,val):                                                                                
        self.extensions = val  
                                 
    def getExtras(self):                                                                                   
        return self.extras                                                                               
                                                                                                    
    def setExtras(self,val):                                                                                
        self.extras = val   
                                 
    def getIndex(self):                                                                                   
        return self.index                                                                               
                                                                                                    
    def setIndex(self,val):                                                                                
        self.index = val

class ModelBuffer(ModelCommon):
    def __init__(self):
        super().__init__()
        self.uri = None
        self.byteSize = 0

    def getUri(self):                                                                                   
        return self.uri                                                                               
                                                                                                    
    def setUri(self,val):                                                                                
        self.uri = val
                                 
    def getByteSize(self):                                                                                   
        return self.byteSize                                                                               
                                                                                                    
    def setByteSize(self,val):                                                                                
        self.byteSize = val

class ModelBufferView(ModelCommon):
    def __init__(self):
        super().__init__()
        self.bufferIndex = None
        self.offset = 0
        self.byteSize = 0
        self.stride = 0
        self.target = None

    def getBufferIndex(self):                                                                                   
        return self.bufferIndex                                                                               
                                                                                                    
    def setBufferIndex(self,val):                                                                                
        self.bufferIndex = val
                                 
    def getByteOffset(self):                                                                                   
        return self.offset                                                                               
                                                                                                    
    def setByteOffset(self,val):                                                                                
        self.offset = val 
                                 
    def getByteSize(self):                                                                                   
        return self.byteSize                                                                               
                                                                                                    
    def setByteSize(self,val):                                                                                
        self.byteSize = val
                      
    def getByteStride(self):                                                                                   
        return self.stride                                                                               
                                                                                                    
    def setByteStride(self,val):                                                                               
        self.stride = val  
                                 
    def getTarget(self):                                                                                   
        return self.target                                                                               
                                                                                                    
    def setTarget(self,val):                                                                               
        self.target = val         

class ModelImage(ModelCommon):
    def __init__(self):
        super().__init__()
        self.uri = None
        self.mime = None
        self.bufferViewIndex = None

    def getUri(self):                                                                                   
        return self.uri                                                                               
                                                                                                    
    def setUri(self,val):                                                                                
        self.uri = val

    def getMime(self):                                                                                   
        return self.mime                                                                        
                                                                                                    
    def setMime(self,val):                                                                                
        self.mime = val
                                 
    def getBufferViewIndex(self):                                                                                   
        return self.bufferViewIndex                                                                               
                                                                                                    
    def setBufferViewIndex(self,val):                                                                                
        self.bufferViewIndex = val

class ModelSampler(ModelCommon):
    def __init__(self):
        super().__init__()
        self.magFilter = None
        self.minFilter = None
        self.wrapS = None
        self.wrapT = None
    
    def getMag(self):                                                                                   
        return self.magFilter                                                                               
                                                                                                    
    def setMag(self,val):                                                                                
        self.magFilter = val

    def getMin(self):                                                                                   
        return self.minFilter                                                                               
                                                                                                    
    def setMin(self,val):                                                                                
        self.minFilter = val

    def getWrapS(self):                                                                                   
        return self.wrapS                                                                               
                                                                                                    
    def setWrapS(self,val):                                                                                
        self.wrapS = val

    def getWrapT(self):                                                                                   
        return self.wrapT                                                                               
                                                                                                    
    def setWrapT(self,val):                                                                                
        self.wrapT = val       

class ModelMaterialProperty(ModelCommon):
    def __init__(self):
        super().__init__()
        self.imageIndex = None
        self.samplerIndex = None
        self.textureCoordinateIndex = None
        self.channel = None
        self.mode = None
        self.factor = None
        self.scale = None

    def getImageIndex(self):                                                                                   
        return self.imageIndex                                                                               
                                                                                                    
    def setImageIndex(self,val):                                                                                
        self.imageIndex = val    

    def getSamplerIndex(self):                                                                                   
        return self.samplerIndex                                                                               
                                                                                                    
    def setSamplerIndex(self,val):                                                                                
        self.samplerIndex = val   

    def getTextureCoordinateIndex(self):                                                                                   
        return self.textureCoordinateIndex                                                                               
                                                                                                    
    def setTextureCoordinateIndex(self,val):                                                                                
        self.textureCoordinateIndex = val      

    def getChannel(self):                                                                                   
        return self.channel                                                                               
                                                                                                    
    def setChannel(self,val):                                                                                
        self.channel = val                                                                                 
                                                                                                    
    def getMode(self):                                                                                
        return self.mode                                                                            
                                                                                                    
    def setMode(self,val):                                                                                
        self.mode = val                                                                                
                                                                                                    
    def getFactor(self):                                                                                
        return self.factor                                                                     
                                                                                                    
    def setFactor(self,val):                                                                                
        self.factor = val                                                                              
                                                                                                    
    def getScale(self):                                                                                
        return self.scale                                                                   
                                                                                                    
    def setScale(self,val):                                                                                
        self.scale = val         

class ModelMaterial(ModelCommon):
    def __init__(self):
        super().__init__()
        self.properties = []
        self.doubleSided = False
        self.mode = None
        self.alpha = None
    
    def addProperty(self, property):
        self.properties.append(property)

    def setProperties(self, properties):
        self.properties = properties

    def getProperties(self):
        return self.properties

    def setDoubleSided(self, val):
        self.doubleSided = val

    def getDoubleSided(self):
        return self.doubleSided

    def setMode(self, val):
        self.mode = val

    def getMode(self):
        return self.mode

    def setAlphaCutoff(self, val):
        self.alpha = val

    def getAlphaCutoff(self):
        return self.alpha  

class ModelCamera(ModelCommon):
    def __init__(self):
        self.projection = None
        self.mode = None
        self.near = None
        self.far = None
        self.fov = None
        self.aspectRatio = None
        self.xmag = None
        self.ymag = None
    
    def getMode(self):                                                                                   
        return self.mode                                                                               

    def setMode(self,val):                                                                                
        self.mode = val
    
    def getNear(self):                                                                                   
        return self.near                                                                               

    def setNear(self,val):                                                                                
        self.near = val
    
    def getFar(self):                                                                                   
        return self.far                                                                               

    def setFar(self,val):                                                                                
        self.far = val
    
    def getFov(self):                                                                                   
        return self.fov                                                                             

    def setFov(self,val):                                                                                
        self.fov = val
    
    def getAspectRatio(self):                                                                                   
        return self.aspectRatio                                                                             

    def setAspectRatio(self,val):                                                                                
        self.aspectRatio = val
    
    def getXMagnification(self):                                                                                   
        return self.xmag                                                                               

    def setXMagnification(self,val):                                                                                
        self.xmag = val
    
    def getYMagnification(self):                                                                                   
        return self.ymag                                                                               

    def setYMagnification(self,val):                                                                                
        self.ymag = val

class ModelAttribute(ModelCommon):
    def __init__(self):
        super().__init__()
        self.bufferViewIndex = None
        self.offset = None
        self.count = None
        self.format = None
                                 
    def getBufferViewIndex(self):                                                                                   
        return self.bufferViewIndex                                                                               
                                                                                                    
    def setBufferViewIndex(self,val):                                                                                
        self.bufferViewIndex = val 
                                 
    def getByteOffset(self):                                                                                   
        return self.offset                                                                               
                                                                                                    
    def setByteOffset(self,val):                                                                                
        self.offset = val 
                                 
    def getCount(self):                                                                                   
        return self.count                                                                               
                                                                                                    
    def setCount(self,val):                                                                                
        self.count = val 
                                 
    def getFormat(self):                                                                                   
        return self.format                                                                               
                                                                                                    
    def setFormat(self,val):                                                                                
        self.format = val

class ModelPrimitive(ModelCommon):
    def __init__(self):
        super().__init__()
        self.mode = None
        self.indices = None
        self.attributes = []
        self.materialIndex = None
    
    def getMode(self):                                                                                   
        return self.mode                                                                               

    def setMode(self,val):                                                                                
        self.mode = val

    def getIndices(self):                                                                                   
        return self.indices                                                                               

    def setIndices(self,val):                                                                                
        self.indices = val                                                                              

    def addAttribute(self,val):
        self.attributes.append(val)

    def getAttributes(self):                                                                                   
        return self.attributes                                                                               

    def setAttributes(self,val):                                                                                
        self.attributes = val

    def getMaterialIndex(self):                                                                                   
        return self.materialIndex                                                                               

    def setMaterialIndex(self,val):                                                                                
        self.materialIndex = val

class ModelMesh(ModelCommon):
    def __init__(self):
        super().__init__()
        self.primitives = []

    def addPrimitive(self, val):                                                                                   
        self.primitives.append(val)

    def getPrimitives(self):                                                                                   
        return self.primitives                                                                               

    def setPrimitives(self,val):                                                                                
        self.primitives = val

class ModelAnimationSampler(ModelCommon):
    def __init__(self):
        super().__init__()
        self.inputAttribute = None
        self.outputAttribute = None
        self.interpolation = None

    def getInputAttribute(self):                                                                                   
        return self.inputAttribute                                                                               
                                                                                                    
    def setInputAttribute(self,val):                                                                                
        self.inputAttribute = val

    def getOutputAttribute(self):                                                                                   
        return self.outputAttribute                                                                               
                                                                                                    
    def setOutputAttribute(self,val):                                                                                
        self.outputAttribute = val
                                 
    def getInterpolation(self):                                                                                   
        return self.interpolation                                                                               
                                                                                                    
    def setInterpolation(self,val):                                                                                
        self.interpolation = val

class ModelAnimationChannel(ModelCommon):
    def __init__(self):
        super().__init__()
        self.sampler = None
        self.nodeIndex = None
        self.targetPath = None

    def getSampler(self):                                                                                   
        return self.sampler                                                                               
                                                                                                    
    def setSampler(self,val):                                                                                
        self.sampler = val

    def getNodeIndex(self):                                                                                   
        return self.nodeIndex                                                                               
                                                                                                    
    def setNodeIndex(self,val):                                                                                
        self.nodeIndex = val
                                 
    def getTargetPath(self):                                                                                   
        return self.targetPath                                                                               
                                                                                                    
    def setTargetPath(self,val):                                                                                
        self.targetPath = val
    
class ModelAnimation(ModelCommon):
    def __init__(self):
        super().__init__()
        self.channels = None
        self.samplers = None

    def getChannels(self):                                                                                   
        return self.channels                                                                               
                                                                                                    
    def setChannels(self,val):                                                                                
        self.channels = val
                                 
    def getSamplers(self):                                                                                   
        return self.samplers                                                                               
                                                                                                    
    def setSamplers(self,val):                                                                                
        self.samplers = val

class ModelNode(ModelCommon):
    def __init__(self):
        super().__init__()
        self.position = None
        self.rotation = None
        self.scale = None
        self.nodes = []
        self.camera = None
        self.mesh = None
    
    def getPosition(self):                                                                                   
        return self.position                                                                               
                                                                                                    
    def setPosition(self,val):                                                                                
        self.position = val 

    def getRotation(self):                                                                                   
        return self.rotation                                                                               
                                                                                                    
    def setRotation(self,val):                                                                                
        self.rotation = val 

    def getScale(self):                                                                                   
        return self.scale                                                                               
                                                                                                    
    def setScale(self,val):                                                                                
        self.scale = val 

    def getCamera(self):                                                                                   
        return self.camera                                                                               
                                                                                                    
    def setCamera(self,val):                                                                                
        self.camera = val 

    def getMesh(self):                                                                                   
        return self.mesh                                                                               
                                                                                                    
    def setMesh(self,val):                                                                                
        self.mesh = val 

    def getNodes(self):                                                                                   
        return self.nodes                                                                               
                                                                                                    
    def setNodes(self,val):                                                                                
        self.nodes = val                       

class ModelScene(ModelCommon):
    def __init__(self):
        super().__init__()
        self.nodes = []
    
    def getNodes(self):                                                                                   
        return self.nodes                                                                               
                                                                                                    
    def setNodes(self,val):                                                                                
        self.nodes = val
