﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicIngredient : MonoBehaviour
{
    public string Name = "Sand";
    private bool Activated = false;

    public void SetActivated(bool state)
    {
        Activated = state;
    }

    public bool IsActivated()
    {
        return Activated;
    }
}
