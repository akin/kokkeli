﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicArea : MonoBehaviour
{
    private List<MagicIngredient> currentIngredients = new List<MagicIngredient>();
    private bool changed = false;
    public MagicSystem magicSystem = null;

    void OnCollisionEnter(Collision other)
    {
        MagicIngredient ingredient = other.gameObject.GetComponent<MagicIngredient>();
        if (ingredient != null && (!ingredient.IsActivated()))
        {
            currentIngredients.Add(ingredient);
            ingredient.SetActivated(true);
            changed = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        MagicIngredient ingredient = other.gameObject.GetComponent<MagicIngredient>();
        if (ingredient != null)
        {
            currentIngredients.Remove(ingredient);
            ingredient.SetActivated(false);
            changed = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        MagicIngredient ingredient = other.gameObject.GetComponent<MagicIngredient>();
        if (ingredient != null && (!ingredient.IsActivated()))
        {
            currentIngredients.Add(ingredient);
            ingredient.SetActivated(true);
            changed = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        MagicIngredient ingredient = other.gameObject.GetComponent<MagicIngredient>();
        if (ingredient != null)
        {
            currentIngredients.Remove(ingredient);
            ingredient.SetActivated(false);
            changed = true;
        }
    }

    void FixedUpdate()
    {
        if (changed)
        {
            if (magicSystem != null)
            {
                magicSystem.Resolve(gameObject, currentIngredients);
            }
            changed = false;
        }
    }
}
