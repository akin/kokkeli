﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicSystem : MonoBehaviour
{
    public void Resolve(GameObject parent, List<MagicIngredient> ingredients)
    {
        // Consume all ingredients possible.
        bool continuos;
        do
        {
            continuos = false;
            var recipes = GetComponents<MagicRecipe>();
            foreach (MagicRecipe recipe in recipes)
            {
                if (recipe.Matches(ingredients))
                {
                    recipe.Apply(parent, ingredients);
                    continuos = true;
                    break;
                }
            }
        }
        while (continuos);
    }
}
