﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicRecipe : MonoBehaviour
{
    public string Name = "";
    public GameObject Effect = null;
    public List<string> Ingredients;

    private bool Matches(MagicIngredient ingredient, string type)
    {
        return ingredient.Name == type;
    }

    public virtual bool Matches(List<MagicIngredient> items)
    {
        List<MagicIngredient> cpItems = new List<MagicIngredient>(items);
        foreach (string type in Ingredients)
        {
            bool found = false;
            foreach (MagicIngredient item in cpItems)
            {
                if (Matches(item, type))
                {
                    found = true;
                    cpItems.Remove(item);
                    break;
                }
            }
            if (!found)
            {
                return false;
            }
        }
        return true;
    }

    public virtual void Apply(GameObject parent, List<MagicIngredient> items)
    {
        foreach (string type in Ingredients)
        {
            bool found = false;
            foreach (MagicIngredient item in items)
            {
                if (Matches(item, type))
                {
                    found = true;
                    items.Remove(item);
                    GameObject.Destroy(item.gameObject);
                    break;
                }
            }
            if (!found)
            {
                Debug.Log("Consumed items, but didnt have compatibility!");
                return;
            }
        }

        if (Effect != null)
        {
            GameObject oo = Instantiate(Effect, parent.transform.position, parent.transform.rotation);
        }
    }
}
