﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRecipe : MagicRecipe
{
    private bool Matches(MagicIngredient ingredient, string type)
    {
        return ingredient.Name == type;
    }

    private bool Has(string type, List<MagicIngredient> items)
    {
        foreach (MagicIngredient item in items)
        {
            if (Matches(item, type))
            {
                return true;
            }
        }
        return false;
    }

    public override bool Matches(List<MagicIngredient> items)
    {
        return Has("Mud", items) && Has("Alcohol", items);
    }

    public override void Apply(GameObject parent, List<MagicIngredient> items)
    {
        for (int i = items.Count - 1; i >= 0; i--)
        {
            var item = items[i];
            if (Matches(item, "Mud") || Matches(item, "Alcohol"))
            {
                items.RemoveAt(i);
                GameObject.Destroy(item.gameObject);
            }
        }

        if (Effect != null)
        {
            GameObject oo = Instantiate(Effect, parent.transform.position, parent.transform.rotation);
        }
    }
}
