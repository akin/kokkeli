# This Python file uses the following encoding: utf-8
import sys
from lib.Main import Main
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Main(app)
    app.exec_()
