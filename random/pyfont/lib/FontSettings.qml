import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Item {
    id: root
    width: 400
    height: 300

    Component.onCompleted: {
        framework.setupObject(root)
    }

    RowLayout {
        x: 31
        y: 39

        Text {
            id: element
            text: qsTr("Font:")
            font.pixelSize: 12
        }

        TextInput {
            id: textInput
            text: qsTr("Calibri")
            horizontalAlignment: Text.AlignHCenter
            Layout.preferredHeight: 20
            Layout.preferredWidth: 80
            font.pixelSize: 12
        }
    }

    Text {
        id: element1
        x: 25
        y: 87
        text: qsTr("Size: ")
        font.pixelSize: 12
    }

    SpinBox {
        id: spinBox
        x: 60
        y: 74
    }

    CheckBox {
        id: checkBox
        x: 25
        y: 131
        text: qsTr("Bold")
    }

    CheckBox {
        id: checkBox1
        x: 31
        y: 182
        text: qsTr("Italic")
    }
}

