# This Python file uses the following encoding: utf-8
import os, sys, urllib.request, json

import importlib
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from .Glyph import GlyphImageProvider, FontInfo
from .UnicodeRange import UnicodeRange, UnicodeRangeSet

def exitOnNone(item, url):
    if item is None:
        sys.exit(-1)

class Main(QObject):
    def __init__(self, application):
        super(Main, self).__init__(application)
        self.app = application
        
        # Engine
        self.engine = QQmlApplicationEngine()
        self.engine.objectCreated.connect(exitOnNone)
        self.engine.addImageProvider("glyph", GlyphImageProvider())

        # Context
        self.ctx = self.engine.rootContext()
        self.ctx.setContextProperty("main", self.engine)
        self.ctx.setContextProperty("FontInfo", FontInfo)
        self.ctx.setContextProperty('framework', self)

        # setup
        qmlRegisterType(UnicodeRange, 'Unicode', 1, 0, 'UnicodeRange')
        qmlRegisterType(UnicodeRangeSet, 'Unicode', 1, 0, 'UnicodeRangeSet')

        # Load
        qml_file = os.path.join(os.path.dirname(__file__),"Main.qml")
        self.engine.load(QUrl.fromLocalFile(qml_file))
        if len(self.engine.rootObjects()) == -1:
            sys.exit(-1)

    @Slot('QJSValue')
    def setupObject(self, object):
        if not object.isQObject():
            return
        qobject = object.toQObject()
        if qobject is None:
            return
        # I cant get directly classname out.. but I get ExportSettings_QMLTYPE_10
        # so lets just split the useless stuff out of it.
        itemClass = qobject.metaObject().className().split("_")[0]
        if itemClass == "" or itemClass is None:
            return
        
        # as typical, python being retarded with relative paths, have to be "relative to running dir".. guess
        # they didnt think of situation where you dont know/care about running dir.. 
        # one day someone will get idea of putting python into medical, and we will have lots of dead people.
        module = importlib.import_module("lib." + itemClass)
        clazz = getattr(module, itemClass)
        instance = clazz(self, qobject)

