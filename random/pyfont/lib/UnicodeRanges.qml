import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

import Unicode 1.0

Item {
    id: root
    anchors.fill: parent
    
    ScrollView {
        anchors.fill: parent
        clip: true
        padding: 1
        Grid {
            id: list
            columns: 1
            spacing: 0
        }
    }

    UnicodeRangeSet {
        id: unicodeRangeSet

        Component.onCompleted: {
            this.refreshRanges();
        }

        function refreshRanges() 
        {
            var ranges = unicodeRangeSet.ranges;
            var component = Qt.createComponent("UnicodeRange.qml");
            
            if (component.status != Component.Ready) 
            {
                console.log("Failed to load component!");
            }

            for (let index = 0; index < ranges.length; index++) {
                const element = ranges[index];
                var object = component.createObject(list);
                if (object == null) 
                {
                    // Error Handling
                    console.log("Error creating object");
                    continue;
                }
                object.text = element.getName();
            }
        }
    }
}

