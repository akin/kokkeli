# This Python file uses the following encoding: utf-8
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from . import QMLTools
from . import QMLDebug

class ExportSettings(QObject):
    def __init__(self, parent, qobject):
        super(ExportSettings, self).__init__(parent)
        self.qobject = qobject
        self.imageChannelOptions = ["Alpha","RGB","RGBA"]
        self.imageChannelTypes = ["uint8","uint32","float32"]
        self.loadForm()

    def loadForm(self):
        QMLTools.setupComboBox(self.qobject, "imageChannels", self.imageChannelOptions)
        QMLTools.setupComboBox(self.qobject, "imageChannelType", self.imageChannelTypes)
        QMLTools.setComboBoxValue(self.qobject, "imageChannelType", self.imageChannelTypes[1])
