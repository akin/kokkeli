# This Python file uses the following encoding: utf-8
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

def setupComboBox(qobject, name, values):
    property = getProperty(qobject, name, "model")
    if property is None:
        print("Could not find model property for '" + name + "' Combobox")
        return
    property.write(values)

def setComboBoxValue(qobject, name, value):
    property = getProperty(qobject, name, "model")
    if property is None:
        print("Could not find model property for '" + name + "' Combobox")
        return
    index = property.read().index(value)

    currentIndexProperty = getProperty(qobject, name, "currentIndex")
    if currentIndexProperty is None:
        print("Could not find currentIndex property for '" + name + "' Combobox")
        return
    currentIndexProperty.write(index)

def setValue(qobject, name, value):
    property = getProperty(qobject, name, "value")
    if property is None:
        print("Could not find value property for '" + name + "'.")
        return
    property.write(value)

def getValue(qobject, name):
    property = getProperty(qobject, name, "value")
    if property is None:
        print("Could not find value property for '" + name + "'.")
        return None
    return property.read()

def getProperty(qobject, name, propertyName):
    target = qobject.findChild(QObject, name)
    if target is None:
        return None
    property = QQmlProperty(target, propertyName)
    if property is None:
        print("Could not find property for '" + name + "' for '" + name + "'.")
        return None
    return property
