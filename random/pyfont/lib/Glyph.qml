import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle {
    id: glyph
    width: 64
    height: 64

    Image {
        id: image
        sourceSize.width: 600
        sourceSize.height: 60
        fillMode: Image.PreserveAspectFit
        source: "image://glyph/calibri.ttf@30/test"
    }
}
