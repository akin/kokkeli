import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Item {
    CheckBox {
        id: unicodeRange
        objectName: "unicodeRange"
        y: 0
        text: qsTr("???")
        Layout.fillWidth: true
    }
}
