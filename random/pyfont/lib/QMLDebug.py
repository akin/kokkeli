# This Python file uses the following encoding: utf-8
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

def printChildren(qobject):
    if qobject is None:
        return
    printChild(qobject)
    for val in qobject.children():
        printChildren(val)

def printChild(qobject):
    print("Child:")
    print(qobject.objectName())
    print("---")
