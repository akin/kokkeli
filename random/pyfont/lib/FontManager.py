
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from PIL import ImageFont, ImageDraw, Image, ImageQt

class Glyph(QObject):
    def __init__(self, code):
        QObject.__init__(self)
        self.code = code
    
    def getCode(self):
        return self.code

class FontManager(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.font = ImageFont.load_default()
        self.reset()

    def reset(self):
        self.glyphs = {}

    def loadFont(self, name, size):
        try:
            self.font = ImageFont.truetype(name, size)
            return True
        except:
            pass
        return False
    
    def getFont(self):
        return self.font

    def getGlyph(self, code):
        if code not in self.glyphs:
            glyph = Glyph(code)
            self.glyphs[code] = glyph
            return glyph
        else:
            return self.glyphs[code]
    