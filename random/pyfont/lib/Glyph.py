
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from PIL import ImageFont, ImageDraw, Image, ImageQt

class GlyphImageProvider(QQuickImageProvider):
    def __init__(self):
        super(GlyphImageProvider, self).__init__(QQuickImageProvider.Image)

    def getFont(self, name):
        data = name.split("@")
        size = 20
        if len(data) > 1:
            name = data[0]
            size = int(data[-1])
        
        try:
            font = ImageFont.truetype(name, size)
            return font
        except:
            pass
        print("Getting default font")
        return ImageFont.load_default()

    def requestImage(self, path, size, reqsize):
        size = (reqsize.width(), reqsize.height())
        if size[0] < 1 or size[1] < 1:
            size = (1, 1)
        
        data = path.split("/")
        font = self.getFont(data[-2])
        image = Image.new('RGBA', size, (255,255,255,0))
        draw = ImageDraw.Draw(image)

        draw.text((20, 10), data[-1], font=font, fill=(0,0,0,255))

        img = ImageQt.ImageQt(image)
        size = img.size()
        return img

class FontInfo(QObject):
    def __init__(self, parent):
        QObject.__init__(self, parent)                       
        self.fileValue = ""

    def setFile(self, value):
        self.fileValue = value
    
    def getFile(self):
        return self.fileValue
        
    @Slot(result=bool)
    def exists(self):
        try:
            ImageFont.truetype(self.fileValue)
            return True
        except:
            pass
        return False
    
    file = Property(str, getFile, setFile)
