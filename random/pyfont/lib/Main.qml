import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Window {
    id: main
    visible: true
    width: 800
    height: 600
    minimumWidth: 800
    minimumHeight: 600
    title: "PyFont bitmap font creator"

    ToolBar {
        id: toolBar
        height: 40
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        RowLayout {
            y: 0
            width: 300
            anchors.right: parent.right
            anchors.rightMargin: 0

            ToolButton {
                id: fontSettingsButton
                text: qsTr("Font Settings")
                onClicked: fontPopup.open()
            }
            ToolButton {
                id: loadButton
                text: qsTr("Load")
            }
            ToolButton {
                id: saveButton
                text: qsTr("Save")
            }
            ToolButton {
                id: exportButton
                text: qsTr("Export")
                onClicked: exportPopup.open()
            }
            ToolButton {
                id: aboutButton
                text: qsTr("About")
                onClicked: about.open()
            }
        }
    }

    Glyph {
    }

    UnicodeRanges {
        y: 40
    }

    Popup {
        id: exportPopup
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        parent: ApplicationWindow.overlay
        anchors.centerIn: parent

        ColumnLayout {
            ExportSettings {
                id: exportSettings
            }
            Button {
                text: "Close"
                onClicked: {
                    exportPopup.close()
                }
            }
        }
    }

    Popup {
        id: fontPopup
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        parent: ApplicationWindow.overlay
        anchors.centerIn: parent

        ColumnLayout {
            FontSettings {
                id: fontSettings
            }
            Button {
                text: "Close"
                onClicked: {
                    fontPopup.close()
                }
            }
        }
    }

    About {
        id: about
    }
}


