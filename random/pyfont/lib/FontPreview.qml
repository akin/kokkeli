import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Item {
    /*
    Here be a rectangle, that is filled with glyph previews..
    each representing one glyph.
    The view will have scrollbars.
    Each glyph can open a settings menu (each glyph has a edit button?)
    Each glyph can be replaced by another CodePoint or set an image.
    */
    
    ScrollView {
        anchors.fill: parent
        clip: true
        padding: 1
        Grid {
            id: list
            columns: 1
            spacing: 0
        }
    }

    function setRange(min, max) {
        
    }
}