# This Python file uses the following encoding: utf-8
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from . import QMLTools
from . import QMLDebug

import os, sys, urllib.request, json

class UnicodeRange(QObject):
    def __init__(self, parent, name, min, max):
        super(UnicodeRange, self).__init__(parent)
        self.name = name
        self.min = min
        self.max = max

    @Slot(result=str)
    def getName(self):
        return self.name

    @Slot(result=int)
    def getMin(self):
        return self.min
    
    @Slot(result=int)
    def getMax(self):
        return self.max


# as usual, python doesnt support lamdas in same way as c/c++.. 
# sigh.. I dont understand what people see in this language
def retCat(val):
    return val['range'][0]

class UnicodeRangeSet(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.rangeSet = []
        self.loadRanges()

    def loadRanges(self):
        """
        {'category': 'Control Character', 'hexrange': ['0000', '001F'], 'range': [0, 31]}
        """
        self.rangeSet = []

        json_file = os.path.join(os.path.dirname(__file__),"../unicode-ranges.json")
        with open(json_file, 'r') as f:
            data = json.load(f)
            data.sort(key=retCat)
            for item in data:
                range = item['range']
                self.rangeSet.append(UnicodeRange(self, item['category'], range[0], range[1]))
    
    def getRanges(self):
        return self.rangeSet
    
    modelChanged = Signal()
    ranges = Property("QVariantList", fget=getRanges, notify=modelChanged)
