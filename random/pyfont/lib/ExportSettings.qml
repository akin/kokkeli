import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Item {
    id: root
    width: 400
    height: 300

    Component.onCompleted: {
        framework.setupObject(root)
    }

    Text {
        id: widthLabel
        width: 75
        text: qsTr("Width:")
        anchors.top: imageWidth.verticalCenter
        anchors.topMargin: 0
        font.pixelSize: 12
    }

    SpinBox {
        id: imageWidth
        wheelEnabled: true
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        Layout.fillWidth: true
        anchors.left: widthLabel.right;
        editable: true
        to: 65536
        value: 512
        transformOrigin: Item.Center
        layer.smooth: false
    }

    Text {
        id: heightLabel
        width: 75
        text: qsTr("Height:")
        anchors.top: imageHeight.verticalCenter
        anchors.topMargin: 0
        font.pixelSize: 12
    }

    SpinBox {
        id: imageHeight
        wheelEnabled: true
        anchors.leftMargin: 0
        anchors.top: imageWidth.bottom
        anchors.right: parent.right
        anchors.topMargin: 0
        Layout.fillHeight: false
        Layout.fillWidth: true
        anchors.left: heightLabel.right
        editable: true
        to: 65536
        value: 512
        transformOrigin: Item.Center
        layer.smooth: false
    }

    ComboBox {
        id: imageChannels
        objectName: "imageChannels"
        anchors.top: imageHeight.bottom
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }

    ComboBox {
        id: imageChannelType
        objectName: "imageChannelType"
        anchors.top: imageChannels.bottom
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }
}
/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
