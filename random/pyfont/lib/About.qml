import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Popup {
    id: root
    width: 400
    height: 400
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    parent: ApplicationWindow.overlay
    anchors.centerIn: parent

    Button {
        x: 292
        y: 252
        text: "Close"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.right: parent.right
        anchors.rightMargin: 8
        onClicked: {
            root.close()
        }
    }

    Text {
        id: header
        x: 8
        y: 8
        text: qsTr("Project PyFont")
        font.family: "Arial"
        font.pixelSize: 32
    }

    TextEdit {
        id: textEdit
        x: 8
        text: qsTr("This project came to be, after using BMFont to do all my bitmap font heavylifting. Currently it is still in pieces, but I hope it will be extensible and highly usable bitmap font generation tool.
The biggest pains in this project so far has been QML and Python, QML is in its infancy _still_ after many years.. hopefully theyll hammer out all the nasty issues it has. Python... being python.. I honestly hope it will never find its way to airplanes or medical, or we will have lots of dead people.

The project home is at: 
https://bitbucket.org/akin/kokkeli/src/master/random/pyfont/

Hopefully you enjoy this .. hack..
t. Mikael Korpela")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 52
        anchors.leftMargin: 8
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.left: parent.left
        anchors.top: header.bottom
wrapMode: TextEdit.Wrap
        anchors.topMargin: 8
        selectByKeyboard: true
        selectByMouse: true
        cursorVisible: false
        readOnly: true
        font.family: "Arial"
        font.pixelSize: 12
    }
}
/*##^##
Designer {
    D{i:1;anchors_x:292}D{i:3;anchors_height:196;anchors_width:384;anchors_x:8;anchors_y:50}
}
##^##*/
