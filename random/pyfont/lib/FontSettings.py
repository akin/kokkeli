# This Python file uses the following encoding: utf-8
from PySide2.QtQml import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from . import QMLTools
from . import QMLDebug

class FontSettings(QObject):
    def __init__(self, parent, qobject):
        super(FontSettings, self).__init__(parent)
        self.qobject = qobject
        self.loadForm()

    def loadForm(self):
        """
        """
