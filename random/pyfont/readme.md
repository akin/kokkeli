#  PyFont
I had to work with font rendering, and I was using BMFont to generate bitmaps and do everything.. I got the idea of trying to make something similar in python.

## TODO
 * everythin
 * signed distance fields
 * font import
 * texture export
 * dialogs
 * selecting which runes
 * packing the glyphs (maxrects still good atlas packing?)
 * export formats? uu, might aswell use flatbuffers for for the file.. that way whoever writes importer, has some autogeneration going on.

## Dependencies
 * PyQt5
 * PySide2
 * Pillow
```
pip install PyQt5 PySide2 Pillow
```

