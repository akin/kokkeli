# This Python file uses the following encoding: utf-8
import sys
import argparse as arggs
from stitcher import Stitcher

# So I guess the sane thing would be to:
# python main.py --type stitcher --in img.png --out flat.png --config <[equirectangular] | cubemap> --preview

parser = arggs.ArgumentParser(description='Do images things')
parser.add_argument('-t','--type', help='action to do', required=True)
parser.add_argument('-i','--in', help='input image file', required=True)
parser.add_argument('-o','--out', help='output image file', required=False)
parser.add_argument('-c','--config', help='configuration', required=False)
parser.add_argument('--preview', dest='preview', default=False, action='store_true')
args = vars(parser.parse_args())

# typical python, missing features, no switch-case..
t = args['type']
if t is 'stitcher':
    stitcher = Stitcher()

    if 'config' in args:
        stitcher.setConfig(args['config'])

    if 'out' in args:
        stitcher.setOutFile(args['out'])

    if 'in' in args:
        stitcher.setInFile(args['in'])

    stitcher.process(args['preview'])
    exit()