# This Python file uses the following encoding: utf-8
import sys
import os
import cv2

class Stitcher():
    def __init__(self):
        self.config = "equirectangular" # <[equirectangular] | cubemap>
        self.outfile = None
        self.infile = None
    
    def setConfig(self, config):
        self.config = config
    
    def setOutFile(self, file):
        self.outfile = file
        
    def setInFile(self, file):
        self.infile = file

    def getCameraMatrix(self, index):
        return None
    
    def getCoefficients(self, index):
        return None
    
    def process(self, preview):
        print("load " + self.infile)

        infile = os.path.join(os.path.dirname(__file__), self.infile)
        inputImage = cv2.imread(infile)

        halfWidth = int(inputImage.shape[1] / 2)

        imageA = inputImage[0:inputImage.shape[0],    0:halfWidth]
        imageB = inputImage[0:inputImage.shape[0],    halfWidth:inputImage.shape[1] ]

        imageAU = cv2.fisheye.undistortImage(imageA, self.getCameraMatrix(0), self.getCoefficients(0))
        imageBU = cv2.fisheye.undistortImage(imageB, self.getCameraMatrix(1), self.getCoefficients(1))

        # Magic.. now I have 2 undistorted 360 images? thats not possible :D

        if self.outfile != None:
            outfile = os.path.join(os.path.dirname(__file__), self.outfile)
            cv2.imwrite(outfile, outputImage)
        
        if preview:
            cv2.imshow('imageA', imageA)
            cv2.imshow('imageB', imageB)
            cv2.waitKey()
