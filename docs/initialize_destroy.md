# initialize/destroy pattern

## reasonings
### initialize
  * constructors cannot indicate failure (without exceptions).
  * class can be a member of another class, without needing of being fully constructed -> initialize constructs fully.

### destroy
  * destroy deconstruct the instance, releases ownerships and lets go of responsibilities in controlled manner.
  * class can be reused, after destroy, initialize can be called to remake the object.
  * calling destroy on destroyed object is ok.

## cons
  * pattern is extra on top of constructor & destructor, people feel that it invites trouble or why not just use ctor/dtor and unique_ptr.
