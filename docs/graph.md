
# Graph 
![](graph.png)

## Elements
### Graph
Graph is the main handler of the pipeline, it has the function to do the actual rendering.

Pattern on the pipeline is:
```
Create graph.
Create Resource(s), Output(s), Stage(s), PostProcess(es) according to what you need.
Connect the Stage(s) and PostProcess(es) with Resource(s) and Output(s).
Call initialize() on graph.
Create renderpackages send them to graph.???
Call graph->render() to do the rendering.
```

TODO:
```
How to send renderpackes to rendering in the graph??
Those should be able to be created on a separate threads, and parallel, and all..

RenderBlock -> CreateRenderBlock(RenderType)
RenderBlock contains callback that gets called if the renderblock is set dirty.
Callback contains VkCommand which should be recorded.
```


Sketching the what could actually be the "thing how to do stuff"
TODO just realized whole input attachment thing, so we arent really rendering to textures etc. need to take that into account!
https://www.saschawillems.de/blog/2018/07/19/vulkan-input-attachments-and-sub-passes/
```
IntrisivePtr<Effect> opaque = resourceManager.getEffect("opaqueEffect");
IntrisivePtr<Effect> transparent = resourceManager.getEffect("transparentEffect");

IntrusivePtr<Graph> graph = new Graph{context};

IntrusivePtr<Stage> opaqueStage = graph->createStage();
IntrusivePtr<Stage> transparentStage = graph->createStage();

auto opaqueNode = new ResourceNode{};
auto depthNode = new ResourceNode{};

auto outputNode = new OutputNode{surface}; 

{
    opaqueNode->setImageFormat(RGBA_8888_UNORM);
    opaqueNode->setDimensions(outputNode.getDimensions());
    opaqueNode->initialize();

    depthNode->setDepthFormat(DEPTH24_STENCIL8_SFLOAT);
    depthNode->setDimensions(outputNode.getDimensions());
    depthNode->initialize();

    opaqueStage->setOutput("depth", depthNode);
    opaqueStage->setOutput("opaque", opaqueNode);
    
    transparentStage->setInput("opaque", opaqueNode);
    transparentStage->setInput("depth", depthNode);

    transparentStage->setOutput("opaque", outputNode);

    // Or if I override operator >>, I could do:
    /// opaqueStage >> depthNode >> transparentStage;
    /// opaqueStage >> opaqueNode >> transparentStage >> outputNode;
}
// Make the graph final & true
graph->initialize();

// The rendering:
IntrusivePtr<Chunk> chunk = graph.createChunk("opaqueStage");
chunk->callback = [&opaque, &material](const VkCommandBuffer& commandBuffer, uint32_t userDescriptorsetOffset){
    // Before this point.. inside Stage, the VkCommmandBuffer has done the following:
    // vkCmdBeginRenderPass
    // vkCmdBindPipeline
    // vkCmdBindDescriptorSets (binding descriptorsets that are before the user descriptorsets)
    // this callback will be called for each object.
    const std::vector<VkDescriptorSet>& descset = material->getDescriptorSets();
    vkCmdBindDescriptorSets(
        commandBuffer, 
        VK_PIPELINE_BIND_POINT_GRAPHICS, 
        opaque->getPipelineLayout()->getLayout(), 
        userDescriptorsetOffset, 
        static_cast<uint32_t>(descset.size()), 
        descset.data(), 
        0, 
        nullptr);
    
    // After this point.. inside Stage. will be done the following:
    // vkCmdBindVertexBuffers
    // vkCmdBindIndexBuffer
    // vkCmdDrawIndexed
    // vkCmdEndRenderPass
});
chunk->setDirty(true);

graph->beginFrame();
graph->endFrame();
```





```
vkCmdBeginRenderPass
vkCmdBindPipeline
vkCmdBindDescriptorSets -> bind sets: 0, 1
vkCmdBindVertexBuffers
vkCmdDraw




vkCmdBeginRenderPass
vkCmdBindPipeline
vkCmdBindDescriptorSets -> bind sets: 0, 1
vkCmdBindVertexBuffers
vkCmdDraw
vkCmdNextSubpass
vkCmdBindPipeline
vkCmdBindDescriptorSets -> bind set: 0 -> error about set 1
```

### Node
#### Output
#### Resource
### Stage
### PostProcess

## Guidelines

