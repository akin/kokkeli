# IntrusivePtr

## reasonings
  * explicitly makes it clear that the class itself is managed by reference counting.
  * very C interface compatible.
  * acts as a typical raw pointer.
  * degrades into raw pointer and back to smart pointer easily.

## cons
  * overhead.
  * custom code.