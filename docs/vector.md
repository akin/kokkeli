# Vector

## reasonings
  * a nice container to wrap dynamic arrays.
  * wraps both data and the data count
  * could be switched to data pointer + count pair, but this feels like giving up on niceties of c++, not a big drop, but drop nonetheless.

## cons
  * overhead.
  * resize, reserve, shrink_to_fit are not guarantees, to really release a assignment of empty vector must be used. 
  * its C++ templates, does not play well with C intefaces.
  * adding might cause invalidation of iterators -> needs careful manipulation.