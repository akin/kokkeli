# to install depencencies run:
# cmake -P dependencies.cmake
cmake_minimum_required(VERSION 3.11)
cmake_policy(VERSION 3.11)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

include("${CMAKE_CURRENT_LIST_DIR}/cmake/vcpkg_dog.cmake")

if(NOT DEFINED VCPKG_TARGET_TRIPLET)
    message(FATAL_ERROR "***** FATAL ERROR: VCPKG_TARGET_TRIPLET not set *****")
    return()
endif()

message(STATUS "***** Checking project third party dependencies in ${VCPKG_ROOT} *****")
execute_process(COMMAND ${VCPKG_EXEC} install glfw3 --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install glm --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install nlohmann-json --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install fmt --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install stb --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install catch2 --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install spdlog --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})

# zstd seems nice compression library https://gregoryszorc.com/blog/2017/03/07/better-compression-with-zstandard/
execute_process(COMMAND ${VCPKG_EXEC} install zstd --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install tinygltf --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT}) # stb, nholmann-json
execute_process(COMMAND ${VCPKG_EXEC} install tinyspline --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
execute_process(COMMAND ${VCPKG_EXEC} install nlohmann-json --triplet ${VCPKG_TARGET_TRIPLET} WORKING_DIRECTORY ${VCPKG_ROOT})
