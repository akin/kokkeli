#include <memcopy/test.h>
#include <core/common.h>
#include <core/util.h>
#include <thread>
#include <future>
#include <atomic>

namespace MemCopy
{
namespace 
{
	bool runTest(Core::Times& timings, size_t data_size, size_t item_count, size_t iteration_count, const std::function<void(uint8_t*, size_t)>& reset, const std::function<bool(uint8_t*, size_t)>& verify)
	{
		timings.resize(iteration_count);

		std::vector<uint8_t> source;
		std::vector<uint8_t> target;

		size_t total = data_size * item_count;
		source.resize(total);
		target.resize(total);

		for (size_t iter = 0; iter < iteration_count; ++iter)
		{
			reset(source.data(), total);
			auto start_time = Core::Clock::now();

			size_t offset = 0;
			for (size_t i = 0; i < item_count; ++i)
			{
				std::memcpy(target.data() + offset, source.data() + offset, data_size);
				offset += data_size;
			}

			auto end_time = Core::Clock::now();
			if (!verify(target.data(), total))
			{
				return false;
			}

			timings[iter] = std::chrono::duration_cast<Core::TimeUnit>(end_time - start_time).count();
		}
		return true;
	}
} // ns anonymous

void Test::setItemSize(size_t size)
{
	m_itemSize = size;
}

void Test::setItemCount(size_t count)
{
	m_itemCount = count;
}

void Test::setIterationCount(size_t count)
{
	m_iterationCount = count;
}

void Test::setThreadCount(size_t count)
{
	m_threadCount = count;
}

void Test::setAffinity(bool state)
{
	m_affinity = state;
}

bool Test::run(const std::function<void(uint8_t*, size_t)>& data_reset, const std::function<bool(uint8_t*, size_t)>& data_verify)
{
	auto maxthreads = std::thread::hardware_concurrency();
	if (maxthreads < 1)
	{
		maxthreads = 1;
	}

	std::promise<bool> initializationFinished;
	std::vector<std::thread> threads;

	// Initialze data structures
	threads.resize(m_threadCount);
	m_result.resize(m_threadCount);

	for (size_t tid = 0; tid < m_threadCount; ++tid)
	{
		m_result[tid].resize(m_itemCount);
	}

	// Setup threads
	auto future = initializationFinished.get_future();
	std::atomic_bool failed = false;
	for (size_t tid = 0; tid < m_threadCount; ++tid)
	{
		threads[tid] = std::thread([this, &future, &failed, tid, &data_reset, &data_verify]()
		{
			future.wait();
			if (!runTest(m_result[tid], m_itemSize, m_itemCount, m_iterationCount, data_reset, data_verify))
			{
				failed = true;
			}
		});
		if (m_affinity)
		{
			Core::setAffinity(threads[tid], tid % maxthreads);
		}
	}

	// All setup done, Let threads go
	initializationFinished.set_value(true);

	// Wait for tests to finish
	for (auto& thread : threads)
	{
		thread.join();
	}

	return !failed;
}

const std::vector<Core::Times>& Test::getResult() const
{
	return m_result;
}

} // ns MemCopy

