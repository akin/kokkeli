
#ifndef MEMCOPY_TEST_H_
#define MEMCOPY_TEST_H_

#include <core/common.h>
#include <functional>
#include <vector>

namespace MemCopy
{
	class Test
	{
	private:
		std::vector<Core::Times> m_result;

		size_t m_itemSize = 1024;
		size_t m_itemCount = 1000;
		size_t m_iterationCount = 1000;
		size_t m_threadCount = 1;
		bool m_affinity = true;
	public:
		void setItemSize(size_t size);
		void setItemCount(size_t count);
		void setIterationCount(size_t count);
		void setThreadCount(size_t count);
		void setAffinity(bool state);

		bool run(const std::function<void(uint8_t*, size_t)>& data_reset, const std::function<bool(uint8_t*, size_t)>& data_verify);

		const std::vector<Core::Times>& getResult() const;
	};
} // ns MemCopy

#endif // MEMCOPY_TEST_H_
