#include <bnwindow/window.h>
#include <assert.h>
#include <glfw/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WGL
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>

namespace bnwindow {
Window::Window()
{
}

Window::~Window()
{
}

void Window::setTitle(const std::string& title)
{
    m_title = title;
    if(m_window != nullptr)
    {
        glfwSetWindowTitle(m_window, m_title.c_str());
    }
}

bool Window::init(uvec2 resolution)
{
    m_resolution = resolution;
    
    if (!glfwInit()) 
    {
        assert(false && "GLFW init failed");
        return false;
    }

    if (!glfwVulkanSupported()) 
    {
        assert(false && "Vulkan support failed");
        return false;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, m_resizable ? GLFW_TRUE : GLFW_FALSE);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

    GLFWwindow* window = glfwCreateWindow(
        static_cast<int>(m_resolution.x),
        static_cast<int>(m_resolution.y),
        m_title.c_str(),
        nullptr, 
        nullptr
    );

    if (!window) 
    {
        assert(false && "GLFW window init failed");
        return false;
    }

    // self is responsible for the blody things.
    glfwSetWindowUserPointer(window, this);

    glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
        Window* self = static_cast<Window*>(glfwGetWindowUserPointer(window));
    });
    glfwSetMouseButtonCallback(window, [](GLFWwindow* window, int button, int action, int mods) {
        Window* self = static_cast<Window*>(glfwGetWindowUserPointer(window));
    });
    glfwSetCursorPosCallback(window, [](GLFWwindow* window, double x, double y) {
        Window* self = static_cast<Window*>(glfwGetWindowUserPointer(window));
    });

    m_window = window;

    return true;
}

void Window::deinit()
{
}

void Window::show(bool state)
{
    if(state)
    {
        glfwShowWindow(m_window);
    }
    else
    {
        glfwHideWindow(m_window);
    }
}

uint32_t Window::getWidth() const
{
    return m_resolution.x;
}

uint32_t Window::getHeight() const
{
    return m_resolution.y;
}

bntypes::ImageFormat Window::getSurfaceFormat() const
{
    return bntypes::ImageFormat::BGRA_unorm8;
}

const uvec2& Window::getDimensions() const
{
    return m_resolution;
}

WindowHandle Window::getHandle()
{
    WindowHandle handle;
    handle.hwnd = glfwGetWin32Window(m_window);
    handle.hinstance = nullptr;
    return handle;
}

bool Window::update()
{
    glfwPollEvents();
    return !glfwWindowShouldClose(m_window);
}
} // ns bnwindow