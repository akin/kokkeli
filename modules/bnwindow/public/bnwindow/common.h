#pragma once
#ifndef BONA_WINDOW_COMMON_ABC
#define BONA_WINDOW_COMMON_ABC

#include <bncommon/platform.h>
#include <bncommon/math.h>

namespace bnwindow {
    using namespace bncommon;
} // ns bnwindow

#endif // BONA_WINDOW_COMMON_ABC