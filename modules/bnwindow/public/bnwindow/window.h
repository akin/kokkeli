#pragma once
#ifndef BONA_WINDOW_WINDOW_ABC
#define BONA_WINDOW_WINDOW_ABC

#include <string>
#include <bncommon/platform.h>
#include <bncommon/math.h>
#include <bnwindow/common.h>
#include <functional>
#include <bntypes/image.h>

struct GLFWwindow;
namespace bnwindow {
class Window
{
private:
    GLFWwindow* m_window = nullptr;
    std::string m_title;
    uvec2 m_resolution;
    bool m_resizable = false;
public:
    Window();
    virtual ~Window();

    void setTitle(const std::string& title);

    bool init(uvec2 resolution);
    virtual void deinit();

    void show(bool state = true);

    uint32_t getWidth() const;
    uint32_t getHeight() const;
    bntypes::ImageFormat getSurfaceFormat() const;
    const uvec2& getDimensions() const;

    WindowHandle getHandle();

    bool update();
};
} // ns bnwindow

#endif // BONA_WINDOW_WINDOW_ABC