
#ifndef CACHE_COHERENCY_COMMON_H_
#define CACHE_COHERENCY_COMMON_H_

// enable SSE2
#define GLM_FORCE_SSE2 1
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES 1

#define GLM_ENABLE_EXPERIMENTAL 1
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace CacheCoherecy
{
    using Matrix = glm::mat4;
	using EntityID = size_t;
} // ns CacheCoherecy

#endif // CACHE_COHERENCY_COMMON_H_
