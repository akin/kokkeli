
#ifndef CACHE_COHERENCY_TYPES_H_
#define CACHE_COHERENCY_TYPES_H_

#include "common.h"
#include <vector>
#include <unordered_map>

namespace CacheCoherecy 
{
	class Transform
	{
	public:
		glm::vec3 location;
		glm::vec3 scale;
		glm::quat rotation;
		glm::mat4 localMatrix;
		bool dirty;

		Transform()
		: location{ 0,0,0 }
		, scale{ 1,1,1 }
		, rotation{ 0.f,0.f, 0.f, 0.f }
		, localMatrix(1.0f)
		, dirty(true)
		{
		}
	};

	class TransformData
	{
	public:
		std::vector<glm::vec3> location;
		std::vector<glm::vec3> scale;
		std::vector<glm::quat> rotation;
		std::vector<bool> dirty; // !!!!! Does this go into the vector<bool> hack???
		std::vector<glm::mat4> localMatrix;
		std::vector<glm::mat4> worldMatrix;
		std::vector<EntityID> entityId;
		std::vector<size_t> parent; // points to indexes in the vectors.
		std::vector<std::vector<size_t>> childs; // points to indexes in the vectors.
		std::unordered_map<EntityID, size_t> entityMap; // map of indexes
	};
	
	using ArrayOfStructs = std::vector<Transform>;
	using StructOfArrays = TransformData;

	// free functions to help manipulate StructOfArrays
	void resize(StructOfArrays& data, size_t size);
	void setLocation(StructOfArrays& data, EntityID entityID, const glm::vec3& position);
	void setRotation(StructOfArrays& data, EntityID entityID, const glm::quat& rotation);
	void setScale(StructOfArrays& data, EntityID entityID, const glm::vec3& scale);

} // ns CacheCoherecy

#endif // CACHE_COHERENCY_TYPES_H_
