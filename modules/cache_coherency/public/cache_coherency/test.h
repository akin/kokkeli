
#ifndef CACHE_COHERENCY_TEST_H_
#define CACHE_COHERENCY_TEST_H_

#include "types.h"
#include <core/common.h>
#include <functional>
#include <vector>

namespace CacheCoherecy
{
	// Time refers to time, how much it took to go through all the items one time.
	std::vector<Core::Times> run_ArrayOfStructs(size_t item_count, size_t iteration_count, size_t thread_count, bool affinity_locked, const std::function<void(ArrayOfStructs&)>& data_reset);
	std::vector<Core::Times> run_StructOfArrays(size_t item_count, size_t iteration_count, size_t thread_count, bool affinity_locked, const std::function<void(StructOfArrays&)>& data_reset);
} // ns CacheCoherecy

#endif // CACHE_COHERENCY_TEST_H_
