#include <cache_coherency/test.h>
#include <core/common.h>
#include <core/util.h>
#include <thread>
#include <future>
#include <glm/gtx/quaternion.hpp>


namespace CacheCoherecy
{
namespace 
{
	void runTest(Core::Times& timings, size_t iteration_count, size_t item_count, const std::function<void(ArrayOfStructs&)>& reset)
	{
		timings.resize(iteration_count);

		ArrayOfStructs data;
		data.resize(item_count);

		for (size_t iter = 0; iter < iteration_count; ++iter)
		{
			reset(data);
			auto start_time = Core::Clock::now();

			for (size_t i = 0; i < item_count; ++i)
			{
				auto& transform = data[i];
				if (transform.dirty)
				{
					glm::mat4 mat(1.0f);
					transform.localMatrix = glm::translate(mat, transform.location);
					transform.localMatrix *= glm::toMat4(transform.rotation);
					transform.localMatrix *= glm::scale(mat, transform.scale);
					transform.dirty = false;
				}
			}

			auto end_time = Core::Clock::now();
			timings[iter] = std::chrono::duration_cast<Core::TimeUnit>(end_time - start_time).count();
		}
	}
} // ns anonymous

std::vector<Core::Times> run_ArrayOfStructs(size_t item_count, size_t iteration_count, size_t thread_count, bool affinity_locked, const std::function<void(ArrayOfStructs&)>& data_reset)
{
	auto maxthreads = std::thread::hardware_concurrency();
	if (maxthreads < 1)
	{
		maxthreads = 1;
	}

	std::promise<bool> initializationFinished;
	std::vector<std::thread> threads;
	std::vector<Core::Times> times;

	// Initialze data structures
	threads.resize(thread_count);
	times.resize(thread_count);

	for (size_t tid = 0; tid < thread_count; ++tid)
	{
		times[tid].resize(item_count);
	}

	// Setup threads
	auto future = initializationFinished.get_future();
	for (size_t tid = 0; tid < thread_count; ++tid)
	{
		threads[tid] = std::thread([&future, &times, tid, item_count, iteration_count, &data_reset]()
		{
			future.wait();
			runTest(times[tid], iteration_count, item_count, data_reset);
		});
		if (affinity_locked)
		{
			Core::setAffinity(threads[tid], tid % maxthreads);
		}
	}

	// All setup done, Let threads go
	initializationFinished.set_value(true);

	// Wait for tests to finish
	for (auto& thread : threads)
	{
		thread.join();
	}

	return std::move(times);
}

} // ns CacheCoherecy

