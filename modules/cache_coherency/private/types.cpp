#include <cache_coherency/types.h>
#include <glm/gtx/quaternion.hpp>

namespace CacheCoherecy
{
void resize(TransformData& data, size_t size)
{
    data.location.resize(size);
    data.scale.resize(size);
    data.dirty.resize(size);
    data.rotation.resize(size);
    data.worldMatrix.resize(size);
    data.localMatrix.resize(size);
    data.entityId.resize(size);
    data.parent.resize(size);
    data.childs.resize(size);

    for (size_t i = 0; i < size; ++i)
    {
        data.entityMap[i] = i;
    }
}

void setLocation(TransformData& data, EntityID entityID, const glm::vec3& position)
{
    auto iter = data.entityMap.find(entityID);
    if (iter == data.entityMap.end())
    {
        return;
    }
    size_t index = iter->second;
    data.dirty[index] = true;
    data.location[index] = position;
}

void setRotation(TransformData& data, EntityID entityID, const glm::quat& rotation)
{
    auto iter = data.entityMap.find(entityID);
    if (iter == data.entityMap.end())
    {
        return;
    }
    size_t index = iter->second;
    data.dirty[index] = true;
    data.rotation[index] = rotation;
}

void setScale(TransformData& data, EntityID entityID, const glm::vec3& scale)
{
    auto iter = data.entityMap.find(entityID);
    if (iter == data.entityMap.end())
    {
        return;
    }
    size_t index = iter->second;
    data.dirty[index] = true;
    data.scale[index] = scale;
}
} // ns CacheCoherecy