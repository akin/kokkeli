#include <cache_coherency/test.h>
#include <core/common.h>
#include <core/util.h>
#include <thread>
#include <future>
#include <glm/gtx/quaternion.hpp>

namespace CacheCoherecy
{
namespace 
{
	void runTest(Core::Times& timings, size_t iteration_count, size_t item_count, const std::function<void(StructOfArrays&)>& reset)
	{
		timings.resize(iteration_count);

		StructOfArrays data;
		resize(data, item_count);

		for (size_t iter = 0; iter < iteration_count; ++iter)
		{
			reset(data);
			auto start_time = Core::Clock::now();

			for (size_t i = 0; i < item_count; ++i)
			{
				if (data.dirty[i])
				{
					glm::mat4 mat(1.0f);
					glm::mat4 matrix = glm::translate(mat, data.location[i]);
					matrix *= glm::toMat4(data.rotation[i]);
					matrix *= glm::scale(mat, data.scale[i]);
					data.dirty[i] = false;

					data.localMatrix[i] = matrix;
				}
			}

			auto end_time = Core::Clock::now();
			timings[iter] = std::chrono::duration_cast<Core::TimeUnit>(end_time - start_time).count();
		}
	}
} // ns anonymous

std::vector<Core::Times> run_StructOfArrays(size_t item_count, size_t iteration_count, size_t thread_count, bool affinity_locked, const std::function<void(StructOfArrays&)>& data_reset)
{
    auto maxthreads = std::thread::hardware_concurrency();
    if (maxthreads < 1)
    {
        maxthreads = 1;
    }

    std::promise<bool> initializationFinished;
    std::vector<std::thread> threads;
	std::vector<Core::Times> times;

	// Initialze data structures
	threads.resize(thread_count);
	times.resize(thread_count);

	for (size_t tid = 0; tid < thread_count; ++tid)
	{
		times[tid].resize(item_count);
	}

	// Setup threads
    auto future = initializationFinished.get_future();
    for (size_t tid = 0; tid < thread_count; ++tid)
    {
        threads[tid] = std::thread([&future, &times, tid, item_count, iteration_count, &data_reset]()
        {
            future.wait();
            runTest(times[tid], iteration_count, item_count, data_reset);
        });
        if (affinity_locked)
        {
			Core::setAffinity(threads[tid], tid % maxthreads);
        }
    }

	// All setup done, Let threads go
    initializationFinished.set_value(true);

	// Wait for tests to finish
    for (auto& thread : threads)
    {
        thread.join();
    }

    return std::move(times);
}

} // ns CacheCoherecy

