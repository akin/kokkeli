#ifndef BONA_DATA_DATANODE_H_
#define BONA_DATA_DATANODE_H_

#include <bndata/node.h>

namespace bndata
{
class DataNode : public Node
{
public:
    virtual ~DataNode() = default;

    virtual size_t readBytes(void* buffer, size_t count, size_t offset = 0) = 0;
};
} // ns bndata

#endif // BONA_DATA_DATANODE_H_