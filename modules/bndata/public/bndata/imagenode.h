#ifndef BONA_DATA_IMAGENODE_H_
#define BONA_DATA_IMAGENODE_H_

#include <bndata/node.h>
#include <bntypes/image.h>

namespace bndata
{
class ImageNode : public Node
{
public:
    virtual ~ImageNode() = default;

    const bntypes::ImageInfo& getImageInfo() const;

    size_t readBytes(void* buffer, size_t count);
    virtual size_t readBytes(void* buffer, size_t count, size_t offset) = 0;
protected:
    bntypes::ImageInfo m_imageInfo = {};
};
} // ns bndata

#endif // BONA_DATA_IMAGENODE_H_