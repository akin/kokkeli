#ifndef BONA_DATA_DATAMANAGER_H_
#define BONA_DATA_DATAMANAGER_H_

#include <string>
#include <bndata/datanode.h>
#include <bndata/imagenode.h>
#include <bndata/scenenode.h>

namespace bndata
{
class DataManager
{
public:
    virtual ~DataManager();

    virtual DataNode* getData(const std::string& path);
    virtual ImageNode* getImage(const std::string& path);
    virtual SceneNode* getScene(const std::string& path);
    
    void incrementReferenceCount();
    void decrementReferenceCount();
protected:
    virtual void requestDestroy();
private:
    size_t m_refCount = 0;
};
} // ns bndata

#endif // BONA_DATA_DATAMANAGER_H_