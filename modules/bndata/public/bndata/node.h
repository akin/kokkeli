#ifndef BONA_DATA_NODE_H_
#define BONA_DATA_NODE_H_

#include <bntypes/data.h>

namespace bndata
{
class Node
{
public:
    virtual ~Node();

    const bntypes::DataInfo& getDataInfo() const;
    
    void incrementReferenceCount();
    void decrementReferenceCount();
protected:
    virtual void requestDestroy();
    
    bntypes::DataInfo m_dataInfo = {};
private:
    size_t m_refCount = 0;
};
} // ns bndata

#endif // BONA_DATA_NODE_H_