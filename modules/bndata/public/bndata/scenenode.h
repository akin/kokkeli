#ifndef BONA_DATA_SCENENODE_H_
#define BONA_DATA_SCENENODE_H_

#include <bndata/node.h>

namespace bndata
{
class SceneNode : public Node
{
public:
    virtual ~SceneNode() = default;

    virtual bool read() = 0;
};
} // ns bndata

#endif // BONA_DATA_SCENENODE_H_