#ifndef BONA_DATA_DATANODEFILE_H_
#define BONA_DATA_DATANODEFILE_H_

#include <bndata/datanode.h>
#include <string>

namespace bndata
{
class DataNodeFile : public DataNode
{
public:
    ~DataNodeFile() final;

    bool setPath(const std::string& path);

    size_t readBytes(void* buffer, size_t count, size_t offset) final;
private:
    std::string m_path;
};
} // ns bndata

#endif // BONA_DATA_DATANODEFILE_H_