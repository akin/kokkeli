#ifndef BONA_DATA_IMAGENODEFILE_H_
#define BONA_DATA_IMAGENODEFILE_H_

#include <bndata/imagenode.h>
#include <string>

namespace bndata
{
class ImageNodeFile : public ImageNode
{
public:
    ~ImageNodeFile() final;

    bool setPath(const std::string& path);

    size_t readBytes(void* buffer, size_t count, size_t offset) final;
private:
    std::string m_path;
    size_t m_components;
};
} // ns bndata

#endif // BONA_DATA_IMAGENODEFILE_H_