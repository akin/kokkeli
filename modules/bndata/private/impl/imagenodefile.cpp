#include <impl/imagenodefile.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace bndata
{
ImageNodeFile::~ImageNodeFile()
{
}

bool ImageNodeFile::setPath(const std::string& path)
{
    m_path = path;

    FILE* fp = fopen(m_path.c_str(), "rb" );
    if(fp == nullptr)
    {
        return false;
    }

    m_components = 0;
    int x, y, components;
    int result = stbi_info_from_file(fp, &x, &y, &components);
    fclose(fp);
    if(result == 0)
    {
        return false;
    }

    m_imageInfo.flags = bntypes::ImageFlag_none;
    switch(components)
    {
        case 4:
        {
            m_imageInfo.format = bntypes::ImageFormat::RGBA_unorm8;
            m_imageInfo.flags |= bntypes::ImageFlag_alpha;
            break;
        }
        case 3:
        {
            components = 4;
            m_imageInfo.format = bntypes::ImageFormat::RGBA_unorm8;
            break;
        }
        case 2:
        {
            m_imageInfo.format = bntypes::ImageFormat::RG_unorm8;
            break;
        }
        case 1:
        {
            m_imageInfo.format = bntypes::ImageFormat::R_unorm8;
            break;
        }
        default:
            return false;
    }

    m_components = components;
    m_imageInfo.width = static_cast<uint32_t>(x);
    m_imageInfo.height = static_cast<uint32_t>(y);
    m_imageInfo.type = bntypes::ImageType::type2D;
    m_imageInfo.depth = 1;
    m_imageInfo.mipCount = 1;
    m_imageInfo.arrayCount = 1;
    m_dataInfo.byteSize = x * y * components;

    return true;
}

size_t ImageNodeFile::readBytes(void* buffer, size_t count, size_t offset)
{
    if(count > m_dataInfo.byteSize)
    {
        assert(false && "Trying to read too much data!");
        return 0;
    }
    
    FILE* fp = fopen(m_path.c_str(), "rb" );
    if(fp == nullptr)
    {
        return 0;
    }

    int x, y, components;
    stbi_uc *data = stbi_load_from_file(fp, &x, &y, &components, m_components);
    fclose(fp);

    if( data == nullptr || 
        x != m_imageInfo.width ||
        y != m_imageInfo.height)
    {
        assert(false && "Bad data!");
        return 0;
    }
    memcpy(buffer, data, count);
    stbi_image_free(data);

    return count;
}
} // ns bndata
