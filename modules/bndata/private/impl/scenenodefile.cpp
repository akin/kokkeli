#include <impl/scenenodefile.h>

#define TINYGLTF_NO_INCLUDE_JSON
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#define JSON_NOEXCEPTION
#include <nlohmann/json.hpp> // used by tiny_gltf
#include <tiny_gltf.h>
#include <spdlog/spdlog.h>

namespace bndata
{
SceneNodeFile::~SceneNodeFile()
{
}

bool SceneNodeFile::setPath(const std::string& path)
{
    m_path = path;

    return true;
}

bool SceneNodeFile::read()
{
    tinygltf::Model model;
	std::string err;
	std::string warn;
	tinygltf::TinyGLTF loader;

	bool res = loader.LoadBinaryFromFile(&model, &err, &warn, m_path);
	if (!warn.empty()) {
        spdlog::error("Failed to load '{0}', {1}.", m_path, warn);
        return false;
	}

	if (!err.empty()) {
        spdlog::error("Failed to load '{0}', {1}.", m_path, err);
        return false;
	}

    if (!res)
    {
        spdlog::error("Failed to load glTF '{0}'.", m_path);
        assert(false);
        return false;
    }
    return true;
}
} // ns bndata
