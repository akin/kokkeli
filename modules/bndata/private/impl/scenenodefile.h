#ifndef BONA_DATA_SCENENODEFILE_H_
#define BONA_DATA_SCENENODEFILE_H_

#include <bndata/scenenode.h>
#include <string>

namespace bndata
{
class SceneNodeFile : public SceneNode
{
public:
    ~SceneNodeFile() final;

    bool setPath(const std::string& path);

    bool read() final;
private:
    std::string m_path;
};
} // ns bndata

#endif // BONA_DATA_SCENENODEFILE_H_