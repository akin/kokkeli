#include <impl/datanodefile.h>
#include <cstdio>

namespace bndata
{
DataNodeFile::~DataNodeFile()
{
}

bool DataNodeFile::setPath(const std::string& path)
{
    m_path = path;

    FILE* fp = fopen(m_path.c_str(), "rb" );
    if(fp == nullptr)
    {
        return false;
    }

    fseek(fp, 0L, SEEK_END);

    m_dataInfo.name = m_path;
    m_dataInfo.byteSize = ftell(fp);

    fclose(fp);

    return true;
}

size_t DataNodeFile::readBytes(void* buffer, size_t count, size_t offset)
{
    FILE* fp = fopen(m_path.c_str(), "rb" );
    if(fp == nullptr)
    {
        return 0;
    }

    fseek(fp, offset, SEEK_SET);
    size_t result = fread(buffer, 1, count, fp);
    fclose(fp);

    return result;
}
} // ns bndata
