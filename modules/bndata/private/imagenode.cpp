#include <bndata/imagenode.h>

namespace bndata
{
const bntypes::ImageInfo& ImageNode::getImageInfo() const
{
    return m_imageInfo;
}

size_t ImageNode::readBytes(void* buffer, size_t count)
{
    return this->readBytes(buffer, count, 0);
}
} // ns bndata
