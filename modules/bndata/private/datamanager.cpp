#include <bndata/datamanager.h>
#include <impl/datanodefile.h>
#include <impl/imagenodefile.h>
#include <impl/scenenodefile.h>

namespace bndata
{
DataManager::~DataManager()
{
}

DataNode* DataManager::getData(const std::string& path)
{
    DataNodeFile *item = new DataNodeFile{};
    if(!item->setPath(path))
    {
        delete item;
        return nullptr;
    }
    return item;
}

ImageNode* DataManager::getImage(const std::string& path)
{
    ImageNodeFile *item = new ImageNodeFile{};
    if(!item->setPath(path))
    {
        delete item;
        return nullptr;
    }
    return item;
}

SceneNode* DataManager::getScene(const std::string& path)
{
    SceneNodeFile *item = new SceneNodeFile{};
    if(!item->setPath(path))
    {
        delete item;
        return nullptr;
    }
    return item;
}

void DataManager::incrementReferenceCount()
{
    ++m_refCount;
}

void DataManager::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        this->requestDestroy();
    }
}

void DataManager::requestDestroy()
{
    delete this;
}
} // ns bndata
