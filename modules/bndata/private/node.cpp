#include <bndata/node.h>

namespace bndata
{
Node::~Node()
{
}

const bntypes::DataInfo& Node::getDataInfo() const
{
    return m_dataInfo;
}

void Node::incrementReferenceCount()
{
    ++m_refCount;
}

void Node::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        this->requestDestroy();
    }
}

void Node::requestDestroy()
{
    delete this;
}
} // ns bndata
