
#include <bncommon/lambdatask.h>

namespace bncommon {

LambdaTask::LambdaTask(const std::function<void()>& func)
: m_func(func)
{
}

LambdaTask::~LambdaTask() 
{
}

void LambdaTask::run()
{
    if(m_func)
    {
        m_func();
    }
}

} // ns bncommon
