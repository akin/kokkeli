#ifndef BONA_THREAD_SHIELDED
#define BONA_THREAD_SHIELDED

#include <iostream>
#include <thread>
#include <functional>
#include <future>

namespace bnthread {

template <typename ShieldedType>
class Shielded
{
private:
    std::mutex mutex;
    std::atomic<ShieldedType> shielded;
    std::condition_variable condition;
public:
    Shielded()
    {
    }

    Shielded(ShieldedType value)
    : shielded(value)
    {
    }

    ~Shielded()
    {
    }

    void add()
    {
        std::unique_lock<std::mutex> lock(mutex);
        ++shielded;
        condition.notify_all();
    }

    void remove()
    {
        std::unique_lock<std::mutex> lock(mutex);
        --shielded;
        condition.notify_all();
    }

    void waitUntil(ShieldedType value)
    {
        std::unique_lock<std::mutex> lock(mutex);
        condition.wait(
            lock, 
            [this, value]{
                return shielded == value;
            });
    }
};

} // ns bnthread

#endif // BONA_THREAD_SHIELDED