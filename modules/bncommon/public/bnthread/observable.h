

#ifndef BONA_THREAD_OBSERVABLE
#define BONA_THREAD_OBSERVABLE

#include <thread>
#include <functional>
 
namespace bnthread {

template <class CType>
class Observable
{
public:
    using AtomicType = std::atomic<CType>;
public:
    class Booking
    {
    private:
        AtomicType& m_count;
        std::mutex& m_mutex;
        std::condition_variable& m_condition;
    public:
        Booking(AtomicType& count , std::mutex& mutex , std::condition_variable& condition)
        : m_count(count)
        , m_mutex(mutex)
        , m_condition(condition)
        {
            m_count.fetch_add(1);
        }
        ~Booking()
        {
            m_count.fetch_sub(1);

            std::lock_guard<std::mutex> lock{m_mutex};
            m_condition.notify_one();
        }
    };
private:
    AtomicType m_value;
    std::mutex m_mutex;
    std::condition_variable m_condition;
public:
    Observable(CType value)
    : m_value{value}
    {
    }

    operator CType() const
    {
        return m_value.load();
    }

    Observable<CType>& operator= (const CType& value)
    {
        return m_value.store(value);
    }

    Booking book()
    {
        Booking booking(m_value, m_mutex, m_condition);
        return std::move(booking);
    }

    void wait(const std::function<void()>& func)
    {
        // Wait for m_running to hit 0
        std::unique_lock<std::mutex> lock{m_mutex};
        m_condition.wait(lock, func);
    }
};

} // ns bnthread

#endif // BONA_THREAD_OBSERVABLE
