
#ifndef BONA_THREAD_POOL
#define BONA_THREAD_POOL

#include <iostream>
#include <thread>
#include <functional>
#include <bninterface/manager.h>

#include "queue.h"

namespace bnthread {

class Pool
{
private:
    bninterface::Manager& m_manager;
    std::vector<std::thread> m_threads;
public:
    Pool(bninterface::Manager& manager);
    Pool(bninterface::Manager& manager, size_t count);
    ~Pool();

    size_t size() const;
    
    void close();
};

} // ns bnhread

#endif // BONA_THREAD_POOL