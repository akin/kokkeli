
#ifndef BONA_THREAD_THREADTOOLS
#define BONA_THREAD_THREADTOOLS

#include <thread>

namespace bnthread {

bool setPriority(std::thread& thread, size_t priority);
size_t getPriority(std::thread& thread);
bool setAffinity(std::thread& thread, size_t core);

} // ns bnthread

#endif // BONA_THREAD_THREADTOOLS