
#ifndef BONA_INTERFACE_TASK_H_
#define BONA_INTERFACE_TASK_H_

namespace bninterface {

class Task {
public:
    virtual ~Task() {}

    virtual void run() = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_TASK_H_
