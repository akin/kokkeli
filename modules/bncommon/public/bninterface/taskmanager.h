
#ifndef BONA_INTERFACE_TASKMANAGER_H_
#define BONA_INTERFACE_TASKMANAGER_H_

namespace bninterface {
class Task;
class TaskManager 
{
public:
    virtual ~TaskManager() {}

    virtual void clear() = 0;
    virtual void add(bninterface::Task* work) = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_TASKMANAGER_H_