
#ifndef BONA_INTERFACE_MANAGER_H_
#define BONA_INTERFACE_MANAGER_H_

namespace bninterface {

class Manager 
{
public:
    virtual ~Manager() {}

    virtual void run() = 0;
    virtual void close() = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_MANAGER_H_