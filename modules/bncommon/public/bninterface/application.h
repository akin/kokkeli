
#ifndef BONA_INTERFACE_APPLICATION_H_
#define BONA_INTERFACE_APPLICATION_H_

#include <string>

namespace bninterface {

class Application
{
private:
    std::string m_name;
public:
    Application(const std::string& name = "");
    virtual ~Application() = 0;

    void parseArgs(int argc, char *argv[]);

    virtual bool init() = 0;

    virtual int run() = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_APPLICATION_H_