
#ifndef BONA_INTERFACE_LOG_H_
#define BONA_INTERFACE_LOG_H_

#include <string>

namespace bninterface {

class Log {
public:
    enum class Level {
		Message = 1,
		Warning = 16,
		Error = 32
	};
public:
    virtual ~Log() {}

	virtual void print(const char *file, size_t line, Level level, std::string message) = 0;
	virtual void print(Level level, std::string message) = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_LOG_H_
