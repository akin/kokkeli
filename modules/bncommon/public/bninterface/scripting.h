
#ifndef BONA_INTERFACE_SCRIPTING_H_
#define BONA_INTERFACE_SCRIPTING_H_

namespace bninterface
{
// Making assumptions:
//  * All functions are called from a single thread
//  * All functions are blocking, so when run is called, it will block that thread, until it is exited.
//  * init is called once, but load possibly could be called multiple times.
class Scripting
{
public:
    virtual ~Scripting() {}

    // Call this when you are ready to make the class "real"
    // the contstructors of scripting are considered lightweight, and do not initialize anything.
    virtual bool init() = 0;

    // load a module that this scripting interface can use
    virtual bool load(const char *path) = 0;

    // run the scripting
    virtual int run(int argc, const char *argv[]) = 0;
};
} // ns bninterface

#endif // BONA_INTERFACE_SCRIPTING_H_
