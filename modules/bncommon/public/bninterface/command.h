
#ifndef BONA_INTERFACE_COMMAND_H_
#define BONA_INTERFACE_COMMAND_H_

namespace bninterface {

class Command {
public:
    virtual ~Command() {}

    virtual void execute() = 0;
    virtual void undo() = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_COMMAND_H_
