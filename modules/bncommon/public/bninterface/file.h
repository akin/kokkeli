
#ifndef BONA_INTERFACE_FILE_H_
#define BONA_INTERFACE_FILE_H_

#include <string>
#include <bncommon/types.h>

namespace bninterface {

class File {
public:
    struct Info
    {
        std::string name;
        std::string path;
        size_t size;
        uint8_t flags;
        bool exists;
    };
public:
    virtual ~File() {}

    virtual bool isOpen() const = 0;
    virtual Info info() const = 0;

    virtual bool open( uint8_t access = bncommon::Access::READ | bncommon::Access::WRITE | bncommon::Access::APPEND ) = 0;
    virtual void reset() = 0;
    virtual void close() = 0;
    virtual void flush() = 0;

    virtual void seek(size_t position) = 0;
    virtual size_t get() = 0;

    virtual size_t read(size_t amount, uint8_t *buffer) = 0;
    virtual size_t write(size_t size, const uint8_t *buffer) = 0;
    
    virtual size_t read(size_t offset, size_t amount, uint8_t *buffer) = 0;
    virtual size_t write(size_t offset, size_t size, const uint8_t *buffer) = 0;
};

} // ns bninterface

#endif // BONA_INTERFACE_FILE_H_
