
#ifndef BONA_COMMON_BITFLAGS_H_
#define BONA_COMMON_BITFLAGS_H_
#include <cstdint>

namespace bncommon {

template <typename EnumType, typename BaseUnit = uint32_t, BaseUnit emptyValue = 0>
class BitFlags
{
public:
    void set(EnumType value)
    {
        m_current |= 1 << static_cast<BaseUnit>(value);
    }
    bool is(EnumType value) const
    {
        return (m_current & (1 << static_cast<BaseUnit>(value))) != 0;
    }
private:
    BaseUnit m_current = emptyValue;
};

} // ns bncommon

#endif // BONA_COMMON_BITFLAGS_H_
