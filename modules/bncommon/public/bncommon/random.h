
#ifndef BONA_COMMON_RANDOM_H_
#define BONA_COMMON_RANDOM_H_

#include <random>

namespace bncommon 
{
class Random
{
private:
	std::random_device device;
	std::mt19937 engine;
	std::uniform_real_distribution<double> distribution;
public:
	Random(double min, double max)
	: engine(device())
	, distribution(min, max)
	{
	}

	float get()
	{
		return (float)distribution(engine);
	}
};
} // ns bncommon

#endif // BONA_COMMON_RANDOM_H_
