
#ifndef BONA_COMMON_UTIL_H_
#define BONA_COMMON_UTIL_H_

#include <thread>

namespace bncommon 
{
    bool setAffinity(std::thread& thread, size_t core);
} // ns bncommon

#endif // BONA_COMMON_UTIL_H_
