
#ifndef BONA_COMMON_TYPES_H_
#define BONA_COMMON_TYPES_H_

#include <string>

namespace bncommon {

enum Access : uint8_t
{
    NONE = 0,
    READ = 1 << 0,
    WRITE = 1 << 1,
    APPEND = 1 << 2,
};

} // ns bncommon

#endif // BONA_COMMON_TYPES_H_
