include(GNUInstallDirs)
install(TARGETS bncommon
    EXPORT bncommon-export
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

install(EXPORT bncommon-targets
  FILE
    bncommonTargets.cmake
  NAMESPACE
    bncommon::
  DESTINATION
    ${CMAKE_INSTALL_LIBDIR}/cmake/bncommon
)