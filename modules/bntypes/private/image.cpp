#include <bntypes/image.h>
#include <assert.h>

namespace bntypes
{

#define COMMON_BIT_COUNT(OUTPUT, element_count) \
    case(ImageFormat::OUTPUT##8) : return element_count * 1;\
    case(ImageFormat::OUTPUT##16) : return element_count * 2;\
    case(ImageFormat::OUTPUT##32) : return element_count * 4

#define COMMON_TYPE_PERMUTATION(MIX, element_count) \
    COMMON_BIT_COUNT(MIX##_uint, element_count);\
    COMMON_BIT_COUNT(MIX##_sint, element_count);\
    COMMON_BIT_COUNT(MIX##_ufloat, element_count);\
    COMMON_BIT_COUNT(MIX##_sfloat, element_count);\
    COMMON_BIT_COUNT(MIX##_unorm, element_count);\
    COMMON_BIT_COUNT(MIX##_snorm, element_count);\
    COMMON_BIT_COUNT(MIX##_srgb, element_count);
    
size_t getFormatSizeInBytes(DataFormat format)
{
    if (format >= static_cast<DataFormat>(ImageFormat::count))
    {
        return 0;
    }
    ImageFormat imageformat = static_cast<ImageFormat>(format);
    switch(imageformat)
    {
        COMMON_TYPE_PERMUTATION(R, 1)
        COMMON_TYPE_PERMUTATION(RG, 2)
        COMMON_TYPE_PERMUTATION(RGB, 3)
        COMMON_TYPE_PERMUTATION(RGBA, 4)
        COMMON_TYPE_PERMUTATION(BGR, 3)
        COMMON_TYPE_PERMUTATION(BGRA, 4)
        case ImageFormat::none : return 0;
        default:
            break;
    }

    return 0;
}
#undef COMMON_TYPE_PERMUTATION
#undef COMMON_BIT_COUNT

} // ns bntypes
