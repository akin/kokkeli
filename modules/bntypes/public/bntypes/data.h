#ifndef BONA_TYPES_DATA_H_
#define BONA_TYPES_DATA_H_

#include <stdint.h>
#include <string>

namespace bntypes
{
struct DataInfo
{
    std::string name;
    size_t byteSize;
};

} // ns bntypes

#endif // BONA_TYPES_DATA_H_