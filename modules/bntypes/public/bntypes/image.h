#ifndef BONA_TYPES_IMAGE_H_
#define BONA_TYPES_IMAGE_H_

#include <stdint.h>

namespace bntypes
{
using DataFormat = uint32_t;

// uint: unsigned integer
// sint: signed integer
// ufloat: unsigned float
// sfloat: signed float
// unorm: unsigned integer normalized [0.0 , 1.0]
// snorm: signed integer normalized   [-1.0 , 1.0]
#define COMMON_BIT_COUNT(OUTPUT) \
    OUTPUT##8,\
    OUTPUT##16,\
    OUTPUT##32

#define COMMON_TYPE_PERMUTATION(MIX) \
    COMMON_BIT_COUNT(MIX##_uint),\
    COMMON_BIT_COUNT(MIX##_sint),\
    COMMON_BIT_COUNT(MIX##_ufloat),\
    COMMON_BIT_COUNT(MIX##_sfloat),\
    COMMON_BIT_COUNT(MIX##_unorm),\
    COMMON_BIT_COUNT(MIX##_snorm),\
    COMMON_BIT_COUNT(MIX##_srgb),

enum class ImageFormat : DataFormat
{
    none = 0,
    packed = 1,
    COMMON_TYPE_PERMUTATION(R)
    COMMON_TYPE_PERMUTATION(RG)
    COMMON_TYPE_PERMUTATION(RGB)
    COMMON_TYPE_PERMUTATION(RGBA)
    COMMON_TYPE_PERMUTATION(BGR)
    COMMON_TYPE_PERMUTATION(BGRA)
    count,
};
#undef COMMON_TYPE_PERMUTATION
#undef COMMON_BIT_COUNT

enum class ImagePackedFormat : uint32_t
{
    none = 0,
};

size_t getFormatSizeInBytes(DataFormat format);

enum ImageFlag
{
    ImageFlag_none = 0,
    ImageFlag_alpha = 1 << 0,
};

enum class ImageType : uint32_t
{
    none = 0,
    type1D,
    type2D,
    type3D,
    typeCube,
};

struct ImageInfo
{
    ImageFormat format = ImageFormat::none;
    ImagePackedFormat packed = ImagePackedFormat::none;
    ImageType type = ImageType::none;
    uint32_t width = 1;
    uint32_t height = 1;
    uint32_t depth = 1;
    uint32_t mipCount = 1;
    uint32_t arrayCount = 1;
    uint32_t flags = ImageFlag_none;
};

} // ns bntypes

#endif // BONA_TYPES_IMAGE_H_