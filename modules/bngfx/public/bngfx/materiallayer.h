#ifndef BONA_GRAPHICS_MATERIALLAYER_H_
#define BONA_GRAPHICS_MATERIALLAYER_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <vector>
#include <unordered_set>

namespace bngfx
{
class Buffer;
class Image;
class ImageView;
class Sampler;
class MaterialLayer
{
public:
    void setBuffer(DescriptorID id, const IntrusivePtr<Buffer>& buffer);
    bool getBuffer(DescriptorID id, IntrusivePtr<Buffer>& buffer);
    
    void setImage(DescriptorID id, const IntrusivePtr<ImageView>& view, const IntrusivePtr<Sampler>& sampler);
    bool getImage(DescriptorID id, IntrusivePtr<ImageView>& view, IntrusivePtr<Sampler>& sampler);

    void add(RenderTypeID id);
    void remove(RenderTypeID id);
    bool isCompatible(RenderTypeID id) const;

    void setLayer(uint32_t layer);
    uint32_t getLayer() const;
private:
    struct BufferData {
        DescriptorID id = 0;
        IntrusivePtr<Buffer> buffer;
    };
    struct ImageData {
        DescriptorID id = 0;
        IntrusivePtr<ImageView> view;
        IntrusivePtr<Sampler> sampler;
    };
    uint32_t m_layer = 0;
    std::vector<BufferData> m_buffers;
    std::vector<ImageData> m_images;
    std::unordered_set<RenderTypeID> m_rendertypes;
};
} // ns bngfx
#endif // BONA_GRAPHICS_MATERIALLAYER_H_