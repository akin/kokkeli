#ifndef BONA_GRAPHICS_FRAMEBUFFER_H_
#define BONA_GRAPHICS_FRAMEBUFFER_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/graph/node.h>
#include <bngfx/resources/imageview.h>
#include <vector>

namespace bngfx
{
class Context;
class FrameBuffer
{
public:
    FrameBuffer(Context& context);
    ~FrameBuffer();

    void attach(const ImageView *view);

    const VkFramebuffer& getFrameBuffer() const;

//    bool initialize(const Node& node);
    void destroy();
private:
    Context& m_context;
    
    std::vector<const ImageView*> m_attachments;
    VkFramebuffer m_frameBuffer = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_FRAMEBUFFER_H_