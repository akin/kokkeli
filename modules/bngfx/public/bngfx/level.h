#ifndef BONA_GRAPHICS_LEVEL_H_
#define BONA_GRAPHICS_LEVEL_H_

#include <bngfx/common.h>
#include <bngfx/resources/mesh.h>
#include <bngfx/resources/sampler.h>
#include <bngfx/resourceblock.h>
#include <bndata/imagenode.h>
#include <vector>
#include <functional>

namespace bngfx
{
class Context;
class Level
{
public:
    explicit Level(Context& context);
    ~Level();

    IntrusivePtr<ResourceBlock> getBlock();
    IntrusivePtr<Sampler> getSampler();

    Mesh *createMesh();
    Image *createImage();

    bool initialize();
    void destroy();

    bool apply(std::function<bool(Mesh*)> func);

    size_t getSize() const;

    void incrementReferenceCount();
    void decrementReferenceCount();
public:
    IntrusivePtr<bndata::ImageNode> m_imageReader;
    Image* image = nullptr;
private:
    int32_t m_refCount = 0;
private:
    Context& m_context;
    IntrusivePtr<ResourceBlock> m_block;
    std::vector<Mesh*> m_meshes;
    IntrusivePtr<Sampler> m_sampler;
};
} // ns bngfx

#endif // BONA_GRAPHICS_LEVEL_H_