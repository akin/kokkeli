#ifndef BONA_GRAPHICS_GRAPH_NODE_H_
#define BONA_GRAPHICS_GRAPH_NODE_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>

namespace bngfx
{
class Context;
class Node
{
public:
    Node(Context& context);
    virtual ~Node();

    ivec3 getDimensions() const;

    ivec3 getOffset() const;
    ivec3 getScissor() const;

    VkFormat getImageFormat() const;
    VkFormat getDepthFormat() const;
    VkSemaphore getSemaphore() const;

    virtual bool initialize() = 0;
    virtual void destroy() = 0;
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
    std::string m_name;
protected:
    Context& m_context;

    VkFormat m_imageFormat = VK_FORMAT_UNDEFINED;
    VkFormat m_depthFormat = VK_FORMAT_UNDEFINED;

    ivec3 m_dimensions;
    ivec3 m_scissor;
    ivec3 m_offset;

    // Semaphore
    VkSemaphore m_semaphore = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_NODE_H_