#ifndef BONA_GRAPHICS_GRAPH_SUBPASS_ATTACHMENT_H_
#define BONA_GRAPHICS_GRAPH_SUBPASS_ATTACHMENT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>

namespace bngfx
{
class SubPassAttachment
{
public:
    const VkAttachmentReference& getAttachmentReference() const;

    void setIndex(uint32_t index);
    void setLayout(VkImageLayout layout);

    bool isDepthStencil() const;
    bool isColor() const;

    VkImageLayout getLayout() const;
    uint32_t getIndex() const;
private:
    VkAttachmentReference m_reference;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_SUBPASS_ATTACHMENT_H_