#ifndef BONA_GRAPHICS_GRAPH_PASS_ATTACHMENT_H_
#define BONA_GRAPHICS_GRAPH_PASS_ATTACHMENT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>

namespace bngfx
{
class PassAttachment
{
public:
    const VkAttachmentDescription& getAttachmentDescription() const;

    void setFormat(VkFormat format);
    void setSamples(VkSampleCountFlagBits samples);

    void setLoadStoreOp(VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp);
    void setStencilLoadStoreOp(VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp);

    void setLayout(VkImageLayout initialLayout, VkImageLayout finalLayout);

    void setIndex(uint32_t index);
    uint32_t getIndex() const;
private:
    VkAttachmentDescription m_attachment;
    uint32_t m_index = 0;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_PASS_ATTACHMENT_H_