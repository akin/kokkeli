#ifndef BONA_GRAPHICS_GRAPH_SUBPASS_H_
#define BONA_GRAPHICS_GRAPH_SUBPASS_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/resources/effect.h>
#include <bngfx/descriptor/pipelinelayout.h>
#include <bngfx/graph/resource.h>
#include <bntypes/image.h>
#include <bngfx/render/chunk.h>

namespace bngfx
{
class Pass;
class Subpass
{
public:
    Subpass(Context& context, Pass& pass);
    ~Subpass();

    const VkSubpassDescription& getSubpassDescription() const;
    uint32_t getIndex() const;

    bool initialize();
    void destroy();

    const VkRenderPass& getRenderPass() const;
    void getViewports(std::vector<VkViewport>& viewports) const;
    void getScissors(std::vector<VkRect2D>& scissors) const;

    void set(const IntrusivePtr<Renderer>& renderer);

    void record(const VkCommandBuffer& commandBuffer);

    /**
     * Attaches RenderObject to Subpass instance, so that it may be rendered
     * on this pass.
     *
     * @param renderObject RenderObject& instance that is to be attached to this pass.
     * @return false if error occurred, otherwise true.
     */
    bool attach(RenderObject& renderObject);
protected:
    Context& m_context;
    Pass& m_pass;
    
    std::vector<VkAttachmentReference> m_attachementReferences;
    VkSubpassDescription m_subpassDescription;

    IntrusivePtr<Renderer> m_renderer;
    std::vector<IntrusivePtr<Chunk>> m_chunks;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_SUBPASS_H_