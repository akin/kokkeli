#ifndef BONA_GRAPHICS_GRAPH_RENDERER_H_
#define BONA_GRAPHICS_GRAPH_RENDERER_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/shader/rendererinfo.h>

namespace bngfx
{
class RenderPass;
class SubPass;
class Renderer
{
public:
    Renderer(Context& context);
    ~Renderer();

    const VkPipeline& getPipeline() const;

    void setInfo(const IntrusivePtr<RendererInfo>& info);
    const IntrusivePtr<RendererInfo>& getInfo() const;

    RenderTypeID getRenderTypeID() const;

    bool initialize(const IntrusivePtr<Renderer>& previousRenderer, const SubPass* subpass);
    void destroy();
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    Context& m_context;

    IntrusivePtr<RendererInfo> m_info;

    VkPipeline m_pipeline = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_RENDERER_H_