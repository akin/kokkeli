#ifndef BONA_GRAPHICS_GRAPH_RESOURCENODE_H_
#define BONA_GRAPHICS_GRAPH_RESOURCENODE_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/graph/node.h>

namespace bngfx
{
class ResourceNode : public Node
{
public:
    ResourceNode(Context& context);
    ~ResourceNode() final;

    bool initialize() final;
    void destroy() final;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_RESOURCENODE_H_