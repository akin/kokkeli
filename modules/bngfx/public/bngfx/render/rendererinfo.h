#ifndef BONA_GRAPHICS_RENDERER_INFO_H_
#define BONA_GRAPHICS_RENDERER_INFO_H_

#include <bngfx/common.h>
#include <bngfx/descriptor/descriptorpool.h>
#include <bngfx/descriptor/descriptorsetlayout.h>
#include <bngfx/descriptor/pipelinelayout.h>
#include <bngfx/descriptor/attributelayout.h>
#include <string>

namespace bngfx
{
class Context;
class Shader;
class RendererInfo
{
public:
    RendererInfo(Context& context);
    ~RendererInfo();

    const std::vector<IntrusivePtr<Shader>>& getShaders() const;
    const bngfx::IntrusivePtr<Shader> getShader(ShaderType type) const;
    IntrusivePtr<PipelineLayout> getPipelineLayout() const;
    IntrusivePtr<AttributeLayout> getVertexInputAttributeLayout() const;

    size_t getDescriptorSetCount() const;
    size_t hasDescriptorSet(DescriptorSetID id) const;

    const DescriptorData* getDescriptorData() const;
    size_t getDescriptorCount() const;

    RenderTypeID getRenderTypeID() const;

    PrimitiveType getInputPrimitive() const;
    void setRenderTypeID(RenderTypeID id);

    bool getDiscardState() const;
    FaceType getCullFace() const;
    WindingType getWindingType() const;

    bool initialize(const std::vector<IntrusivePtr<Shader>>& shaders);
    void destroy();
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
    std::string m_name;

    size_t funnyNumber = 100; // TODO, Refactor out, this used to be FramebufferCount, but for developmental mental sanity, its ridiculous number "just to get things rolling"
private:
    Context& m_context;

    RenderTypeID m_type = 0;
    
    // All the descriptorss you could ever want!
    std::vector<IntrusivePtr<DescriptorSetLayout>> m_descriptorSetLayouts;
    IntrusivePtr<PipelineLayout> m_pipelineLayout;
    IntrusivePtr<AttributeLayout> m_vertexInputAttributeLayout;
    IntrusivePtr<DescriptorPool> m_descriptorPool;

    // All the shaderss!
    std::vector<IntrusivePtr<Shader>> m_shaders;

    // For actual runtime reflection:
    std::unordered_map<DescriptorID, DescriptorData> m_descriptorsMap;

    // For generating pipeline etc.
    std::vector<DescriptorData> m_descriptors;

    // for input assembly
    PrimitiveType m_primitive = PrimitiveType::triangles;
};
} // ns bngfx

#endif // BONA_GRAPHICS_RENDERER_INFO_H_