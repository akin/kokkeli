#ifndef BONA_GRAPHICS_FRAME_H_
#define BONA_GRAPHICS_FRAME_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>

namespace bngfx
{
class Frame
{
public:
    void setSize(size_t size);
    VkAttachmentDescription& get(size_t index);
    size_t getSize() const;
private:
    std::vector<VkAttachmentDescription> m_attachments;
};
} // ns bngfx

#endif // BONA_GRAPHICS_FRAME_H_