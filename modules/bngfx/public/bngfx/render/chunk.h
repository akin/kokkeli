#ifndef BONA_GRAPHICS_GRAPH_CHUNK_H_
#define BONA_GRAPHICS_GRAPH_CHUNK_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/descriptor/descriptorset.h>
#include <functional>

namespace bngfx
{
class Context;
class RenderObject;
class Chunk
{
public:
    explicit Chunk(Context& context, const IntrusivePtr<RenderObject>& object);
    ~Chunk();

    void destroy();

    void getDescriptorSets(VkDescriptorSet* sets);
    
    const IntrusivePtr<Mesh>& getMesh() const;

    DescriptorSet& getDescriptorSet();
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    Context& m_context;

    int32_t m_refCount = 0;
    DescriptorSet m_set;

    IntrusivePtr<RenderObject> m_object;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_CHUNK_H_