#ifndef BONA_GRAPHICS_RENDEROBJECT_H_
#define BONA_GRAPHICS_RENDEROBJECT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <functional>

namespace bngfx
{
class Context;
class Chunk;
class RenderObject
{
public:
    using UpdateBufferCallback = std::function<void(const Buffer& buffer, uint32_t offset)>;
public:
    RenderObject(Context& context);
    ~RenderObject();

    void setDirty(bool state = true);
    bool isDirty() const

    void addChunk(Chunk* chunk);
    void destroy();

    const DescriptorSet& getDescriptorSet() const;

    void setMaterial(const InstrusivePtr<Material>& material);
    const InstrusivePtr<Material>& getMaterial() const;

    void setMesh(const InstrusivePtr<Mesh>& mesh);
    const InstrusivePtr<Mesh>& getMesh() const;

    UpdateBufferCallback updateBufferCallback;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
    Context& m_context;

    DescriptorSet m_set;

    IntrusivePtr<Material> m_material;
    IntrusivePtr<Mesh> m_mesh;

    std::vector<Chunk*> m_chunks;

    bool m_dirty = true;
};
} // ns bngfx

#endif // BONA_GRAPHICS_RENDEROBJECT_H_