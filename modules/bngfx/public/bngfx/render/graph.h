#ifndef BONA_GRAPHICS_GRAPH_GRAPH_H_
#define BONA_GRAPHICS_GRAPH_GRAPH_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/graph/resourcenode.h>

namespace bngfx
{
class Graph
{
public:
    Graph(Context& context);
    ~Graph();

    IntrusivePtr<ResourceNode> createResourceNode();
    IntrusivePtr<Stage> createStage();
    
    bool initialize();
    void destroy();

    void beginFrame();
    void endFrame();

    const VkCommandPool& getCommandPool() const;
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
    std::string m_name;

    Context& m_context;

    VkCommandPool m_commandPool;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_GRAPH_H_