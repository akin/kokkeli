#ifndef BONA_GRAPHICS_GRAPH_PASS_H_
#define BONA_GRAPHICS_GRAPH_PASS_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/resources/effect.h>
#include <bngfx/descriptor/pipelinelayout.h>
#include <bngfx/graph/resource.h>
#include <bntypes/image.h>

namespace bngfx
{
class Context;
class Graph;
class Pass
{
public:
    Pass(Context& context, Graph& graph);
    ~Pass();

    /**
     * Attaches RenderObject to Pass instance, so that it may be rendered
     * on this pass.
     *
     * @param renderObject RenderObject& instance that is to be attached to this pass.
     * @return false if error occurred, otherwise true.
     */
    bool attach(RenderObject& renderObject);

    void setPrimitiveType(PrimitiveType value);
    void setScissor(ivec2 offset, uvec2 size);
    void setViewport(vec2 position, vec2 size, float minDepth, float maxDepth);
    void setRasterizerDiscard(bool value);
    void setFrontFaceWinding(WindingType value);
    void setCullingMode(FaceType value);
    void setImageFormat(bntypes::ImageFormat format);
    void setDepthFormat(VkFormat value);

    void getViewports(std::vector<VkViewport>& viewports) const;
    void getScissors(std::vector<VkRect2D>& scissors) const;

    const vec2& getViewportSize() const;

    VkFormat getImageFormat() const;
    VkFormat getDepthFormat() const;

    void addOutput(IntrusivePtr<Node> resource);
    void removeOutput(IntrusivePtr<Node> resource);

    const VkRenderPass& getRenderPass() const;

    VkCommandBuffer getCommandBuffer();

    bool initialize();
    void destroy();
    
    bool record();
protected:
    Context& m_context;
    Graph& m_graph;

    VkCommandBuffer m_commandBuffer;
    std::vector<SubPass> m_subpasses;

    VkRenderPass m_renderPass = VK_NULL_HANDLE;

    PrimitiveType m_inputPrimitive = PrimitiveType::triangles;
    ivec2 m_scissorOffset;
    uvec2 m_scissorSize;
    vec2 m_viewportPosition;
    vec2 m_viewportSize;
    float m_viewportMinDepth = 0.0f;
    float m_viewportMaxDepth = 1.0f;

    WindingType m_windingMode = WindingType::ccw;
    FaceType m_cullFace = FaceType::back;

    VkFormat m_imageFormat = VK_FORMAT_UNDEFINED;
    VkFormat m_depthFormat = VK_FORMAT_UNDEFINED;

    bool m_rasterizerDiscardEnabled = false;

    // Semaphore
    VkSemaphore m_renderFinished = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_PASS_H_