#ifndef BONA_GRAPHICS_GRAPH_OUTPUTNODE_H_
#define BONA_GRAPHICS_GRAPH_OUTPUTNODE_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/graph/node.h>

namespace bngfx
{
class OutputNode : public Node
{
public:
    Output(Context& context);
    ~Output() final;

    bool initialize() final;
    void destroy() final;
};
} // ns bngfx

#endif // BONA_GRAPHICS_GRAPH_OUTPUTNODE_H_