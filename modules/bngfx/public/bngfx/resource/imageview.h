#ifndef BONA_GRAPHICS_IMAGEVIEW_H_
#define BONA_GRAPHICS_IMAGEVIEW_H_

#include <bngfx/common.h>
#include <bngfx/context.h>

namespace bngfx
{
class ImageView
{
public:
    ImageView(Context& context);
    ~ImageView();

    void setAspectMask(VkImageAspectFlags value);
    void setMipBaseLevel(uint32_t value);
    void setMipCount(uint32_t value);
    void setArrayBaseLevel(uint32_t value);
    void setArrayCount(uint32_t value);

    const VkImageView& getView() const;
    bool initialize(VkImage image, VkImageViewType type, VkFormat format);
    void destroy();
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    Context& m_context;
    VkImageView m_view = VK_NULL_HANDLE;

    VkImageAspectFlags m_aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;
    uint32_t m_mipBaseLevel = 0;
    uint32_t m_mipCount = 1;
    uint32_t m_arrayBaseLevel = 0;
    uint32_t m_arrayCount = 1;
};
} // ns bngfx

#endif // BONA_GRAPHICS_IMAGEVIEW_H_