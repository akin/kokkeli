#ifndef BONA_GRAPHICS_RESOURCEBLOCK_H_
#define BONA_GRAPHICS_RESOURCEBLOCK_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <vector>

namespace bngfx
{
class Context;
class Image;
class Buffer;
class ResourceBlock final
{
public:
    ResourceBlock(Context& context);
    ~ResourceBlock();

    Context& getContext() const;
    const VkDeviceMemory& getMemory() const;

    void setMemoryPropertyFlags(VkMemoryPropertyFlags value);

    IntrusivePtr<Image> createImage();
    IntrusivePtr<Buffer> createBuffer();

    bool allocate();
    void destroy();
    
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    Context& m_context;

    VkDeviceMemory m_memory = VK_NULL_HANDLE;
    size_t m_byteSize = 0;
    VkMemoryPropertyFlags m_memoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    std::vector<IntrusivePtr<Image>> m_images;
    std::vector<IntrusivePtr<Buffer>> m_buffers;
private:
    int32_t m_refCount = 0;
};
} // ns bngfx

#endif // BONA_GRAPHICS_RESOURCEBLOCK_H_