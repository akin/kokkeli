#ifndef BONA_GRAPHICS_COMMANDMANAGER_H_
#define BONA_GRAPHICS_COMMANDMANAGER_H_

#include <bngfx/common.h>
#include <bngfx/resources/mesh.h>
#include <bngfx/resources/sampler.h>
#include <bngfx/resourceblock.h>
#include <vector>
#include <functional>

namespace bngfx
{
class Context;
class CommandManager
{
public:
    explicit CommandManager(Context& context);
    ~CommandManager();

    void setPoolFamilyIndex(uint32_t index);
    void setPoolFlags(uint32_t flags);
    void setQueue(VkQueue queue);

    bool initialize();
    void destroy();

    const VkQueue& getQueue() const;
    const VkCommandPool& getPool() const;
private:
    Context& m_context;

    VkCommandPool m_pool = VK_NULL_HANDLE;

    uint32_t m_flags = 0;
    uint32_t m_familyIndex = 0;
    VkQueue m_queue = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_COMMANDMANAGER_H_