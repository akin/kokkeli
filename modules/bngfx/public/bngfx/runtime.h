#ifndef BONA_GRAPHICS_RUNTIME_H_
#define BONA_GRAPHICS_RUNTIME_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/graph/node.h>

#include <bngfx/descriptor/descriptorlayoutset.h>
#include <bngfx/descriptor/pipelinelayout.h>

namespace bngfx
{
/*
class Context;
class Runtime
{
public:
    Runtime(Context& context);
    ~Runtime();

    bool initialize(const Node& node);
    void destroy();

    bool draw();
private:
    Context& m_context;
};
*/
} // ns bngfx

#endif // BONA_GRAPHICS_RUNTIME_H_