#ifndef BONA_GRAPHICS_MESH_H_
#define BONA_GRAPHICS_MESH_H_

#include <bngfx/common.h>
#include <bngfx/commondata.h>
#include <vector>

namespace bngfx
{
class Context;
class ResourceBlock;
class Buffer;
class Mesh
{
public:
    void initialize(ResourceBlock& block);

    std::vector<Vertex> vertices;
    std::vector<uint16_t> indices;

    uint32_t getVertexCount() const;
    uint32_t getIndicesCount() const;

    const IntrusivePtr<Buffer>& getVertexBuffer() const;
    const IntrusivePtr<Buffer>& getIndexBuffer() const;
public: // Resource
    void setName(const std::string& name);
    const std::string& getName() const;
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
    std::string m_name;

    IntrusivePtr<Buffer> m_vertexBuffer;
    IntrusivePtr<Buffer> m_indexBuffer;
};
} // ns bngfx

#endif // BONA_GRAPHICS_MESH_H_