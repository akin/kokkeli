#ifndef BONA_GRAPHICS_SURFACE_H_
#define BONA_GRAPHICS_SURFACE_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/graph/node.h>
#include <bngfx/resourceblock.h>
#include <bngfx/level.h>

#include <bngfx/resources/buffer.h>
#include <bngfx/resources/image.h>
#include <bngfx/resources/imageview.h>

#include <bngfx/descriptor/descriptorpool.h>
#include <bngfx/descriptor/descriptorset.h>
#include <bngfx/descriptor/pipelinelayout.h>

#include <bngfx/swapchain.h>
#include <bngfx/framebuffer.h>

#include <bntypes/image.h>
#include <bndata/imagenode.h>
#include <vector>

namespace bngfx
{
class Context;
struct DeviceInfo;

struct FrameData
{
    FrameData(Context& context)
    : framebuffer(context)
    , descriptorSet(context)
    {
    }

    VkImage image = VK_NULL_HANDLE;
    VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
    std::unique_ptr<ImageView> view;

    FrameBuffer framebuffer;
    DescriptorSet descriptorSet;

    Image* depthImage = nullptr;
    Buffer* ubo = nullptr;
};

class Surface
{
public:
    Surface(Context& context);
    ~Surface();

    const VkSurfaceKHR& getSurface() const;
    const VkSurfaceCapabilitiesKHR& getCapabilities() const;

    bool isSupportedFormat(VkFormat format, VkColorSpaceKHR colorSpace) const;
    bool isSupportedPresentMode(VkPresentModeKHR presentMode) const;

    bool initialize(const DeviceInfo& device, const WindowHandle& handle);
    //bool initializeFramebuffer(Node& node, SwapChain& swapchain, DescriptorLayoutSet *descriptorLayoutSet, PipelineLayout& layout);
    void destroy();

    bool draw(SwapChain& swapchain);

    void incrementReferenceCount();
    void decrementReferenceCount();

private:
    Context& m_context;
    
    // Surface
    VkSurfaceCapabilitiesKHR m_capabilities;
    std::vector<VkSurfaceFormatKHR> m_formats;
    std::vector<VkPresentModeKHR> m_presentModes;
    VkSurfaceKHR m_surface = VK_NULL_HANDLE;
    
    IntrusivePtr<ResourceBlock> m_stagingBlock;
    Buffer* m_stagingBuffer;

    IntrusivePtr<Level> m_level;

    // Command Pool
    VkCommandPool m_commandPoolGraphics = VK_NULL_HANDLE;
    VkCommandPool m_commandPoolTransfer = VK_NULL_HANDLE;

    // Semaphore for images
    VkSemaphore m_imageAvailable = VK_NULL_HANDLE;
    VkSemaphore m_renderFinished = VK_NULL_HANDLE;

    // frame stuffs
    VkFence m_frameFence = VK_NULL_HANDLE;

    // Descriptor thiings
    IntrusivePtr<DescriptorPool> m_descriptorPool;
    IntrusivePtr<DescriptorLayoutSet> m_descriptorLayoutSet;

    // Frame datas
    IntrusivePtr<ResourceBlock> m_frameGpuBlock;
    IntrusivePtr<ResourceBlock> m_frameCpuBlock;
    std::vector<std::unique_ptr<FrameData>> m_frames;
};
} // ns bngfx

#endif // BONA_GRAPHICS_SURFACE_H_