#ifndef BONA_GRAPHICS_SWAPCHAIN_H_
#define BONA_GRAPHICS_SWAPCHAIN_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bntypes/image.h>
#include <vector>

namespace bngfx
{
class Context;
class Surface;
class SwapChain
{
public:
    SwapChain(Context& context);
    ~SwapChain();

    void setMinimumImageCount(size_t count);
    void setPresentMode(VkPresentModeKHR presentMode);
    void setResolution(uvec2 value);
    void setFormat(bntypes::ImageFormat format);
    void setImageSpace(VkColorSpaceKHR colorSpace);

    uvec2 getResolution() const;
    VkFormat getFormat() const;

    uint32_t getImageCount() const;

    const VkSwapchainKHR& getSwapChain() const;
    const VkImage& getImage(size_t index) const;

    bool initialize(const Surface& surface);
    void destroy();
private:
    Context& m_context;
    
    VkPresentModeKHR m_presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    uint32_t m_minimumImageCount = 3;
    uint32_t m_imageCount = 0;
    VkExtent2D m_extent;
    VkSurfaceFormatKHR m_surfaceFormat;

    VkSwapchainKHR m_swapChain = VK_NULL_HANDLE;

    std::vector<VkImage> m_images;
};
} // ns bngfx

#endif // BONA_GRAPHICS_SWAPCHAIN_H_