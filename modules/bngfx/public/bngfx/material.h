#ifndef BONA_GRAPHICS_MATERIAL_H_
#define BONA_GRAPHICS_MATERIAL_H_

#include <bngfx/materiallayer.h>
#include <vector>

namespace bngfx
{
class Material
{
public:
    const std::vector<MaterialLayer>& getLayers() const;
    
    bool initialize(size_t layerCount);
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    std::vector<MaterialLayer> m_layers;
    int32_t m_refCount = 0;
};

} // ns bngfx
#endif // BONA_GRAPHICS_MATERIAL_H_