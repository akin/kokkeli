#ifndef BONA_GRAPHICS_COMMONDATA_ABCD_H_
#define BONA_GRAPHICS_COMMONDATA_ABCD_H_

#include <bngfx/common.h>

namespace bngfx
{
	struct Vertex {
		vec4 position;
		vec4 color;
		vec2 texCoord;
	};

	struct ModelViewProjection {
        mat4 model;
        mat4 view;
        mat4 projection;
	};
}

#endif // BONA_GRAPHICS_COMMONDATA_ABCD_H_