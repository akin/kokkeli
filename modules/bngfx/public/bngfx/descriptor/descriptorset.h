#ifndef BONA_GRAPHICS_DESCRIPTOR_SET_H_
#define BONA_GRAPHICS_DESCRIPTOR_SET_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/descriptor/descriptorsetlayout.h>

namespace bngfx
{
class Context;
class Buffer;
class Image;
class ImageView;
class Sampler;
class DescriptorPool;
class DescriptorSet
{
public:
    explicit DescriptorSet(Context& context);
    ~DescriptorSet();

    const VkDescriptorSet& getDescriptorSet() const;

    void setBuffer(size_t bindingIndex, const Buffer* buffer);
    void setImageViewSampler(size_t bindingIndex, const ImageView* view, const Sampler* sampler);
    void setImageSampler(size_t bindingIndex, Image* image, const Sampler* sampler);

    bool initialize(const IntrusivePtr<DescriptorPool>& pool, const IntrusivePtr<DescriptorSetLayout>& layout);
    void destroy();
    
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    Context& m_context;
    VkDescriptorSet m_set = VK_NULL_HANDLE;
private:
    int32_t m_refCount = 0;
};
} // ns bngfx

#endif // BONA_GRAPHICS_DESCRIPTOR_SET_H_