#ifndef BONA_GRAPHICS_ATTRIBUTE_LAYOUT_H_
#define BONA_GRAPHICS_ATTRIBUTE_LAYOUT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <vector>

namespace bngfx
{
class Context;
class AttributeLayout
{
public:
    const VkPipelineVertexInputStateCreateInfo& getInputStateCreateInfo() const;

    bool initialize(const std::vector<AttributeData>& attributeData);
    
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    Context& m_context;

    VkPipelineVertexInputStateCreateInfo m_vertexInputinfo{};
    std::vector<VkVertexInputBindingDescription> m_vertexInputBindingDescriptions;
    std::vector<VkVertexInputAttributeDescription> m_vertexInputAttributeDescriptions;
private:
    int32_t m_refCount = 0;
};
} // ns bngfx

#endif // BONA_GRAPHICS_ATTRIBUTE_LAYOUT_H_