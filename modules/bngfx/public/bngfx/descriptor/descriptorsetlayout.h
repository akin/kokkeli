#ifndef BONA_GRAPHICS_DESCRIPTORSET_LAYOUT_H_
#define BONA_GRAPHICS_DESCRIPTORSET_LAYOUT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <vector>

namespace bngfx
{
class Context;
class DescriptorSetLayout
{
public:
    explicit DescriptorSetLayout(Context& context);
    ~DescriptorSetLayout();

    bool isInitialized() const;

    void addBufferBinding(uint32_t slot, uint32_t count, VkShaderStageFlags flags);
    void addImageBinding(uint32_t slot, uint32_t count, VkShaderStageFlags flags);

    size_t getDescriptorTypeCount(VkDescriptorType type) const;

    const VkDescriptorSetLayout& getLayout() const;

    bool initialize();
    void destroy();
    
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    Context& m_context;

    std::vector<VkDescriptorSetLayoutBinding> m_bindings;
    VkDescriptorSetLayout m_layout = VK_NULL_HANDLE;
private:
    int32_t m_refCount = 0;
};
} // ns bngfx

#endif // BONA_GRAPHICS_DESCRIPTORSET_LAYOUT_H_