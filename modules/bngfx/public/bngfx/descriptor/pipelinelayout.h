#ifndef BONA_GRAPHICS_PIPELINE_LAYOUT_H_
#define BONA_GRAPHICS_PIPELINE_LAYOUT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/descriptor/descriptorlayoutset.h>

namespace bngfx
{
class Context;
class PipelineLayout
{
public:
    explicit PipelineLayout(Context& context);
    ~PipelineLayout();
    
    const VkPipelineLayout& getLayout() const;

    bool initialize(const std::vector<IntrusivePtr<DescriptorSetLayout>>& layouts);
    void destroy();
private:
    Context& m_context;
    VkPipelineLayout m_layout = VK_NULL_HANDLE;
};
} // ns bngfx

#endif // BONA_GRAPHICS_PIPELINE_LAYOUT_H_