#ifndef BONA_GRAPHICS_DESCRIPTOR_POOL_H_
#define BONA_GRAPHICS_DESCRIPTOR_POOL_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <unordered_map>

namespace bngfx
{
class Context;
class DescriptorPool
{
public:
    explicit DescriptorPool(Context& context);
    ~DescriptorPool();

    void setDescriptorPoolSize(VkDescriptorType type, size_t value);
    void setDescriptorSetCount(size_t value);

    const VkDescriptorPool& getPool() const;

    bool allocate();
    void destroy();
    
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    Context& m_context;

    std::unordered_map<VkDescriptorType, size_t> m_descriptorPoolSizes;
    size_t m_descriptorCount = 0;

    VkDescriptorPool m_pool = VK_NULL_HANDLE;
private:
    int32_t m_refCount = 0;
};
} // ns bngfx

#endif // BONA_GRAPHICS_DESCRIPTOR_POOL_H_