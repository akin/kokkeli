#ifndef BONA_GRAPHICS_CONTEXT_H_
#define BONA_GRAPHICS_CONTEXT_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <vector>
#include <string>
#include <memory>

/*
Contains messy Vulkan "context" implementation
*/
namespace bngfx
{
class Surface;
struct DeviceInfo
{
    size_t id;
    uint32_t vendorID;
    uint32_t deviceID;
    std::string name;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkPhysicalDeviceFeatures features;
    VkPhysicalDeviceProperties properties;
    VkPhysicalDeviceMemoryProperties memoryProperties;
};

struct QueueInfo
{
    VkQueue queue = VK_NULL_HANDLE;
    uint32_t familyIndex = 0;
    std::vector<float> priorities{0.0f};
    bool found = false;
};

class Context
{
public:
    Context();
    virtual ~Context();

    const std::vector<DeviceInfo>& getDevices() const;

    void setSettings(const Settings& settings);

    // Instance
    bool initInstance();
    void destroyInstance();
    void addInstanceExtension(const char* extension);
    const VkInstance& getInstance() const;

    // Device
    bool initDevice(const DeviceInfo& info, const Surface& surface);
    void destroyDevice();
    void addDeviceExtension(const char* extension);
    const VkDevice& getDevice() const;
    const VkPhysicalDevice& getPhysicalDevice() const;
    const DeviceInfo& getDeviceInfo() const;

    VkFormatFeatureFlags getFormatFlags(VkFormat format);

    bool flush();

    bool finish();
public:
//private:
    Settings m_settings;

    // VK instance
    std::vector<const char*> m_instanceLayers;
    std::vector<const char*> m_instanceExtensions;
    std::vector<DeviceInfo> m_devices;

    VkDebugUtilsMessengerEXT m_debugUtil;
    VkInstance m_instance = VK_NULL_HANDLE;

    // VK Device
    std::vector<const char*> m_deviceLayers;
    std::vector<const char*> m_deviceExtensions;
    DeviceInfo m_deviceInfo;
    VkDevice m_device = VK_NULL_HANDLE;

    // Queue
    QueueInfo m_graphics;
    QueueInfo m_compute;
    QueueInfo m_transfer;
    QueueInfo m_present;
};
} // ns bngfx

#endif // BONA_GRAPHICS_CONTEXT_H_