#ifndef BONA_GRAPHICS_TRANSFORM_H_
#define BONA_GRAPHICS_TRANSFORM_H_

#include <bngfx/common.h>

namespace bngfx
{
class Transform
{
public:
    mat4 localMatrix;

    quat rotation;
    vec3 translation;
    vec3 scale;

    void updateMatrix()
    {
        // TODO
    }
};
} // ns bngfx

#endif // BONA_GRAPHICS_TRANSFORM_H_