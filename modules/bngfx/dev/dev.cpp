#include <future>
#include <iostream>
#include <fmt/format.h>

#include <bncommon/common.h>
#include <bnwindow/window.h>

#include <bngfx/context.h>
#include <bngfx/renderer/basicrenderer.h>

#include <bngfx/surface.h>
#include <bngfx/swapchain.h>
#include <bngfx/descriptor/pipelinelayout.h>
#include <bngfx/descriptor/descriptorlayout.h>

#include <bndata/datamanager.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <bngfx/graph/output.h>
#include <bngfx/graph/resource.h>
#include <bngfx/graph/stage.h>

using namespace bngfx;

void makeGraph()
{
	ResourceManager resources;

	// Load effect ...
	{
		EffectResourceLoader loader(resources);
		if(!loader.load("defaultshaders", "effects.json"))
		{
			// Failed to load json file
			assert(false);
			return;
		}
	}
	IntrusivePtr<Effect> effect = resources.getEffect("opaqueDefault");

	IntrusivePtr<MaterialType> materialType = new MaterialType;
	materialType->setName("opaqueDefault");
	/// setup MaterialType, "opaque" is the stage at where this particular effect takes place
	/// -it is not yet clear how to set up effect for each stage of pipeline, but Ill figure it out at some point and I feel it should be supported, not just for "opaque stage", but for any other stages as well.
	/// -if a effect is not set for a stage, then this material is not rendered on that stage
	/// -also I am tempted on having materials latch on a graph, and get settings from graph, but on the otherhand, that sounds quite tight coupling.
	materialType.setEffect("opaque", effect);
	/// at initialize MaterialType gets finalized, all the effect reflection should now realize into descriptorsetlaouts for each stage.
	materialType.initialize();

	resources.set(materialType->getName(), materialType);

	/// Material is a child of MaterialType
	/// all the reflections are in MaterialType but the settings descriptorset is in the Material
	/// So each mesh can have unique Material instance of a MaterialType, or they can share one Material.
	IntrusivePtr<Material> material = materialType.create();

	// Load stuff ...
	{
		MeshResourceLoader loader(resources);
		if(!loader.load("helmet", "helmet.gltf"))
		{
			// Failed to load glft file
			assert(false);
			return;
		}
	}

	IntrusivePtr<Mesh> mesh = resources.getMesh("helmet");
	/*
	mesh.setMaterial(material);
	mesh.getMaterial()->getMaterialType().create();
	mesh.setVertexes(vertexes);
	mesh.setIndexes(vertexes);
	*/

	// Setup render graph.. this is bit hacky at the moment, in the future I hope itll get better.
	graph::Graph renderGraph;
	graph::Stage* stage = renderGraph.createStage("opaque");
	graph::Output ouput; // << Screen output
	stage->setOutput(&output);

	// Set hardcoded camera
	mat4 model;
	mat4 view;
	mat4 projection;

	while(true)
	{
		// Prepare to receive graphics related rendering commands, finish staging buffers and cleanup and be ready. flip the frame.
		renderGraph.beginFrame();
		
		// Send camera data? not sure if it has to be here..
		setCamera(view, projection);

		// Send Meshes renderpackages to be rendered in the graph
		renderMesh(model, mesh);

		// Render the graph
		renderGraph.endFrame();
	}
}

int main(int argc, char **argv) 
{
	bnwindow::Window window;
	bngfx::Settings settings;
	settings.validation = false;

	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "bona_log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}

	spdlog::info("Welcome to Bona!");
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	double fps = 0.0;
	window.setTitle(fmt::format("{0} - {1}fps", BUILD_PROJECT_NAME, fps));

	if(!window.init({ 800, 600 }))
	{
		spdlog::error("Failed to init window.");
		assert(false);
		return EXIT_FAILURE;
	}

	spdlog::info("window created.");
	Context context;

	context.addInstanceExtension(VK_KHR_SURFACE_EXTENSION_NAME);
	context.addInstanceExtension(PLATFORM_SURFACE_EXTENSION_NAME);

	bndata::DataManager dataManager;
	context.setSettings(settings);

	if(!context.initInstance())
	{
		spdlog::error("Failed to init context.");
		assert(false);
		return EXIT_FAILURE;
	}

	spdlog::info("Context instance initialized.");
	const auto devices = context.getDevices();
	if(!devices.size() > 0)
	{
		spdlog::error("No devices found.");
		assert(false);
		return EXIT_FAILURE;
	}

	for (auto device : devices)
	{
		spdlog::info("Device found: {0} [{1}:{2}]", device.name, device.vendorID, device.deviceID);
	}

	Surface surface(context);
	auto selectedDevice = devices[0];
	if(!surface.initialize(selectedDevice, window.getHandle()))
	{
		spdlog::error("Failed to init surface.");
		assert(false);
		return EXIT_FAILURE;
	}

	context.addDeviceExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	if(!context.initDevice(selectedDevice, surface))
	{
		spdlog::error("Failed to init selected device {0} [{1}:{2}].", selectedDevice.name, selectedDevice.vendorID, selectedDevice.deviceID);
		assert(false);
		return EXIT_FAILURE;
	}
	spdlog::info("Device {0} [{1}:{2}] initialized.", selectedDevice.name, selectedDevice.vendorID, selectedDevice.deviceID);
	{
		IntrusivePtr<DescriptorLayoutSet> descriptorLayoutSet = new DescriptorLayoutSet(context);
        {
            DescriptorLayout *descriptorLayout = descriptorLayoutSet->add(0);
			descriptorLayout->addBufferBinding(0, 1, VK_SHADER_STAGE_VERTEX_BIT);
			descriptorLayout->addImageBinding(1, 1, VK_SHADER_STAGE_FRAGMENT_BIT);
        }
		if(!descriptorLayoutSet->initialize())
		{
			spdlog::error("Failed to init descriptor layoutset.");
			assert(false);
			return EXIT_FAILURE;
		}

		PipelineLayout layout(context);
		if(!layout.initialize(descriptorLayoutSet.get()))
		{
			spdlog::error("Failed to init pipeline layout.");
			assert(false);
			return EXIT_FAILURE;
		}

		std::vector<Shader*> shaders;
		Shader vertex(context);
		{
			IntrusivePtr<bndata::DataNode> reader = dataManager.getData("01_triangle.vert.spv");
			if(!reader)
			{
				spdlog::error("Failed to open file '{0}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}
			std::vector<char> data(reader->getDataInfo().byteSize);
			if(!reader->readBytes(data.data(), data.size(), 0) == data.size())
			{
				spdlog::error("Failed to read file '{0}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}

			if(!vertex.initialize(ShaderType::vertex, data.data(), data.size()))
			{
				spdlog::error("Failed to init shader '{0}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}
			shaders.push_back(&vertex);
		}
		Shader fragment(context);
		{
			IntrusivePtr<bndata::DataNode> reader = dataManager.getData("01_triangle.frag.spv");
			if(!reader)
			{
				spdlog::error("Failed to open file '{0}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}
			std::vector<char> data(reader->getDataInfo().byteSize);
			if(!reader->readBytes(data.data(), data.size(), 0) == data.size())
			{
				spdlog::error("Failed to read file '{0}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}

			if(!fragment.initialize(ShaderType::fragment, data.data(), data.size()))
			{
				spdlog::error("Failed to init shader '{0}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}
			shaders.push_back(&fragment);
		}
		spdlog::info("Shaders initialized.");

		{
			IntrusivePtr<bndata::ImageNode> reader = dataManager.getImage("resources/fubar.png");
			if(!reader)
			{
				spdlog::error("Failed to open file '{}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}

			surface.m_imageReader = reader;
		}
		{
			IntrusivePtr<bndata::SceneNode> reader = dataManager.getScene("resources/DamagedHelmet.glb");
			if(!reader)
			{
				spdlog::error("Failed to open file '{}'.", reader->getDataInfo().name);
				assert(false);
				return EXIT_FAILURE;
			}
			reader->read();
		}
		spdlog::info("Resources initialized.");

		VkFormat depth = VK_FORMAT_UNDEFINED;
		std::vector<VkFormat> depthFormats{
			VK_FORMAT_D24_UNORM_S8_UINT,
			VK_FORMAT_D32_SFLOAT_S8_UINT,
			VK_FORMAT_D16_UNORM_S8_UINT,
		};
		for (auto format : depthFormats)
		{
			if (context.getFormatFlags(format) & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
			{
				depth = format;
				break;
			}
		}


		Node node(context);
		node.setPrimitiveType(PrimitiveType::triangles);
		node.setScissor({0.0f, 0.0f}, window.getDimensions());
		node.setViewport({0.0f, 0.0f}, {window.getDimensions().x, window.getDimensions().y}, 0.0f, 1.0f);
		node.setImageFormat(window.getSurfaceFormat());
		node.setDepthFormat(depth);

		if(!node.initialize(shaders.data(), shaders.size(), layout))
		{
			spdlog::error("Failed to init node.");
			assert(false);
			return EXIT_FAILURE;
		}
		spdlog::info("Nodes initialized.");

		SwapChain swapchain(context);
		swapchain.setFormat(window.getSurfaceFormat());
		swapchain.setResolution(window.getDimensions());
		swapchain.setMinimumImageCount(3);
		swapchain.setPresentMode(VK_PRESENT_MODE_MAILBOX_KHR);
		swapchain.setImageSpace(VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);

		if (!swapchain.initialize(surface))
		{
			spdlog::error("Failed to init swapchain.");
			assert(false);
			return EXIT_FAILURE;
		}

		if(!surface.initializeFramebuffer(node, swapchain, descriptorLayoutSet.get(), layout))
		{
			spdlog::error("Failed to init framebuffer.");
			assert(false);
			return EXIT_FAILURE;
		}
		spdlog::info("All ready.");

		BasicRenderer renderer(context);

		window.show();

		auto last = bncommon::Clock::now();
		auto now = last;
		size_t units = 0;

		size_t frame = 0;
		bool running = true;
		while(true)
		{
			if ((frame % 1000) == 0)
			{
				now = bncommon::Clock::now();

				units = std::chrono::duration_cast<bncommon::TimeUnit>(now - last).count(); 
				fps = 1.0 / (((double)units) * 1e-9 / 1000.0);
				window.setTitle(fmt::format("{0} - {1}fps", BUILD_PROJECT_NAME, fps));
				last = now;
			}
			if(!window.update())
			{
				break;
			}
			if(!surface.draw(swapchain))
			{
				spdlog::error("Failed to draw.");
				assert(false);
				return EXIT_FAILURE;
			}

			++frame;
		}
		if(!context.finish())
		{
			spdlog::error("Failed to finish context.");
			assert(false);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}
