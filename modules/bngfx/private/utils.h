
#ifndef BONA_GRAPHICS_UTILS_H_ABV
#define BONA_GRAPHICS_UTILS_H_ABV

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bntypes/image.h>
#include <bncommon/bitflags.h>
#include <string>

namespace bngfx 
{
std::string toString(VkResult result);

VkShaderStageFlagBits shaderTypeToVkShaderStageFlagBits(ShaderType type);

VkPrimitiveTopology primitiveTypeToVkPrimitiveTopology(PrimitiveType type);

VkCullModeFlagBits faceTypeToVkCullModeFlagBits(FaceType type);

VkFrontFace windingTypeToVkFrontFace(WindingType type);

VkFormat dataFormatToVkFormat(bntypes::DataFormat format);

VkImageType imageTypeToVkImageType(bntypes::ImageType type);

VkShaderStageFlagBits bitFlagsToVkShaderStageFlags(const BitFlags<ShaderType>& flags);
} // ns bngfx

#endif // BONA_GRAPHICS_UTILS_H_ABV