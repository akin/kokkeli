#include <bngfx/common.h>

namespace bngfx
{
// TODO, this is an ever growing list of formats, 
const std::vector<DataFormatName> gc_dataFormats
{
    {"float", static_cast<DataFormat>(bntypes::ImageFormat::R_sfloat32)},
    {"vec2", static_cast<DataFormat>(bntypes::ImageFormat::RG_sfloat32)},
    {"vec3", static_cast<DataFormat>(bntypes::ImageFormat::RGB_sfloat32)},
    {"vec4", static_cast<DataFormat>(bntypes::ImageFormat::RGBA_sfloat32)},
};
} // ns bngfx
