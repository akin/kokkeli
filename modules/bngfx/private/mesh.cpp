#include <bngfx/resource/mesh.h>
#include <bngfx/resource/resourceblock.h>
#include <bngfx/resource/buffer.h>
#include <bngfx/vkcommon.h>
#include "../utils.h"

namespace bngfx
{
void Mesh::initialize(ResourceBlock& block)
{
    m_vertexBuffer = block.createBuffer();
    m_vertexBuffer->setUsage(VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
    m_vertexBuffer->setSize<Vertex>(vertices.size());

    m_indexBuffer = block.createBuffer();
    m_indexBuffer->setUsage(VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
    m_indexBuffer->setSize<uint16_t>(indices.size());
}

uint32_t Mesh::getVertexCount() const
{
    return static_cast<uint32_t>(m_vertices.size());
}

uint32_t Mesh::getIndicesCount() const
{
    return static_cast<uint32_t>(indices.size());
}

const IntrusivePtr<Buffer>& Mesh::getVertexBuffer() const
{
    return m_vertexBuffer;
}

const IntrusivePtr<Buffer>& Mesh::getIndexBuffer() const
{
    return m_indexBuffer;
}

void Mesh::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Mesh::getName() const
{
    return m_name;
}

void Mesh::incrementReferenceCount()
{
    ++m_refCount;
}

void Mesh::decrementReferenceCount()
{
    --m_refCount;
    if(m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
