#include <bngfx/descriptor/descriptorsetlayout.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
DescriptorSetLayout::DescriptorSetLayout(Context& context)
: m_context(context)
{
}

DescriptorSetLayout::~DescriptorSetLayout()
{
    destroy();
}

bool DescriptorSetLayout::isInitialized() const
{
    return m_layout != VK_NULL_HANDLE;
}

void DescriptorSetLayout::addBufferBinding(uint32_t slot, uint32_t count, VkShaderStageFlags flags)
{
    VkDescriptorSetLayoutBinding binding = {};
    binding.binding = slot;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    binding.descriptorCount = count;
    binding.stageFlags = flags;
    m_bindings.push_back(binding);
}

void DescriptorSetLayout::addImageBinding(uint32_t slot, uint32_t count, VkShaderStageFlags flags)
{
    VkDescriptorSetLayoutBinding binding = {};
    binding.binding = slot;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    binding.descriptorCount = count;
    binding.stageFlags = flags;
    binding.pImmutableSamplers = nullptr;
    m_bindings.push_back(binding);
}

size_t DescriptorSetLayout::getDescriptorTypeCount(VkDescriptorType type) const
{
    size_t count = 0;
    for(const auto& binding : m_bindings)
    {
        if(binding.descriptorType == type)
        {
            count += binding.descriptorCount;
        }
    }
    return count;
}
    
const VkDescriptorSetLayout& DescriptorSetLayout::getLayout() const
{
    return m_layout;
}
    
bool DescriptorSetLayout::initialize()
{
    if(m_layout != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;
    
    VkDescriptorSetLayoutCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = static_cast<uint32_t>(m_bindings.size());
    createInfo.pBindings = m_bindings.data();
    
    error = vkCreateDescriptorSetLayout(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_layout);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreateDescriptorSetLayout '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void DescriptorSetLayout::destroy()
{
	if (m_layout != VK_NULL_HANDLE)
	{
		vkDestroyDescriptorSetLayout(m_context.getDevice(), m_layout, NO_ALLOCATOR);
		m_layout = VK_NULL_HANDLE;
	}
}

void DescriptorSetLayout::incrementReferenceCount()
{
    ++m_refCount;
}

void DescriptorSetLayout::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
