#include <bngfx/descriptor/attributelayout.h>

namespace bngfx
{

const VkPipelineVertexInputStateCreateInfo& AttributeLayout::getInputStateCreateInfo() const
{
    return m_vertexInputinfo;
}
    
bool AttributeLayout::initialize(const std::vector<AttributeData>& attributeData)
{
    m_vertexInputBindingDescriptions.clear();
    m_vertexInputAttributeDescriptions.clear();
    for (const AttributeData& attr : attributeData)
    {
        const AttributeData& attr = attrs[i];

        // TODO
        // THIS might be terrible misunderstanding from my part!
        // but, to my understanding, if I bind to exact same binding as location
        // then each buffer should be bound to those locations as well.. this creates
        // binding == location.. 
        uint32_t binding = attr.location;
        uint32_t location = attr.location;

        VkVertexInputBindingDescription bindingDescription = {};
        bindingDescription.binding = binding;
        bindingDescription.stride = bntypes::getFormatSizeInBytes(attr.format);
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        m_vertexInputBindingDescriptions.push_back(bindingDescription);

        VkVertexInputAttributeDescription attributeDescription = {};
        attributeDescription.binding = binding;
        attributeDescription.location = location;
        attributeDescription.format = dataFormatToVkFormat(attr.format);
        attributeDescription.offset = 0;
        m_vertexInputAttributeDescriptions.push_back(attributeDescription);
    }
    m_vertexInputinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    m_vertexInputinfo.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexInputBindingDescriptions.size());
    m_vertexInputinfo.pVertexBindingDescriptions = vertexInputBindingDescriptions.data();
    m_vertexInputinfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexInputAttributeDescriptions.size());
    m_vertexInputinfo.pVertexAttributeDescriptions = vertexInputAttributeDescriptions.data();

    return true;
}

void AttributeLayout::incrementReferenceCount()
{
    ++m_refCount;
}

void AttributeLayout::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
