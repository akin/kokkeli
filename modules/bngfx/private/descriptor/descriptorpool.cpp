#include <bngfx/descriptor/descriptorpool.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
DescriptorPool::DescriptorPool(Context& context)
: m_context(context)
{
}

DescriptorPool::~DescriptorPool()
{
    destroy();
}

void DescriptorPool::setDescriptorPoolSize(VkDescriptorType type, size_t value)
{
    m_descriptorPoolSizes[type] = value;
}

void DescriptorPool::setDescriptorSetCount(size_t value)
{
    m_descriptorCount = value;
}
    
const VkDescriptorPool& DescriptorPool::getPool() const
{
    return m_pool;
}
    
bool DescriptorPool::allocate()
{
    if(m_pool != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;

    std::vector<VkDescriptorPoolSize> poolSizes;
    for(auto iter : m_descriptorPoolSizes)
    {
        VkDescriptorPoolSize poolSize = {};
        poolSize.type = iter.first;
        poolSize.descriptorCount = static_cast<uint32_t>(iter.second);
        poolSizes.push_back(poolSize);
    }
    
    VkDescriptorPoolCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    createInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    createInfo.pPoolSizes = poolSizes.data();
    createInfo.maxSets = static_cast<uint32_t>(m_descriptorCount);
    
    error = vkCreateDescriptorPool(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_pool);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreateDescriptorPool '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void DescriptorPool::destroy()
{
	if (m_pool != VK_NULL_HANDLE)
	{
		vkDestroyDescriptorPool(m_context.getDevice(), m_pool, NO_ALLOCATOR);
		m_pool = VK_NULL_HANDLE;
	}
}

void DescriptorPool::incrementReferenceCount()
{
    ++m_refCount;
}

void DescriptorPool::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
