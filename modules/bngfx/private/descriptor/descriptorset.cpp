#include <bngfx/descriptor/descriptorset.h>
#include <bngfx/resources/buffer.h>
#include <bngfx/resources/image.h>
#include <bngfx/resources/sampler.h>
#include <bngfx/context.h>
#include <bngfx/descriptor/descriptorpool.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
DescriptorSet::DescriptorSet(Context& context)
: m_context(context)
{
}

DescriptorSet::~DescriptorSet()
{
    destroy();
}

const VkDescriptorSet& DescriptorSet::getDescriptorSet() const
{
    return m_set;
}

void DescriptorSet::setBuffer(size_t bindingIndex, const Buffer* buffer)
{
    VkDescriptorBufferInfo bufferInfo = {};
    bufferInfo.buffer = buffer->getBuffer();
    bufferInfo.offset = 0;
    bufferInfo.range = buffer->getByteSize();

    VkWriteDescriptorSet descriptorWrite = {};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = m_set;
    descriptorWrite.dstBinding = static_cast<uint32_t>(bindingIndex);
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrite.descriptorCount = 1;
    
    descriptorWrite.pBufferInfo = &bufferInfo;
    descriptorWrite.pImageInfo = nullptr; 
    descriptorWrite.pTexelBufferView = nullptr; 

    vkUpdateDescriptorSets(m_context.getDevice(), 1, &descriptorWrite, 0, nullptr);
}

void DescriptorSet::setImageViewSampler(size_t bindingIndex, const ImageView* view, const Sampler* sampler)
{
    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = view->getView();
    imageInfo.sampler = sampler->getSampler();

    VkWriteDescriptorSet descriptorWrite = {};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = m_set;
    descriptorWrite.dstBinding = static_cast<uint32_t>(bindingIndex);
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrite.descriptorCount = 1;
    
    descriptorWrite.pBufferInfo = nullptr;
    descriptorWrite.pImageInfo = &imageInfo; 
    descriptorWrite.pTexelBufferView = nullptr; 

    vkUpdateDescriptorSets(m_context.getDevice(), 1, &descriptorWrite, 0, nullptr);
}

void DescriptorSet::setImageSampler(size_t bindingIndex, Image* image, const Sampler* sampler)
{
    setImageViewSampler(bindingIndex, image->getView(), sampler);
}

bool DescriptorSet::initialize(const IntrusivePtr<DescriptorPool>& pool, const IntrusivePtr<DescriptorSetLayout>& layout)
{
    if(m_set)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }
	VkResult error;

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = pool->getPool();
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &layout->getLayout();

    error = vkAllocateDescriptorSets(m_context.getDevice(), &allocInfo, &m_set);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkAllocateDescriptorSets '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void DescriptorSet::destroy()
{
    m_set = VK_NULL_HANDLE;
}

void DescriptorSet::incrementReferenceCount()
{
    ++m_refCount;
}

void DescriptorSet::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
