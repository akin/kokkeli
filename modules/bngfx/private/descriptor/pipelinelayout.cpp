#include <bngfx/descriptor/pipelinelayout.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
PipelineLayout::PipelineLayout(Context& context)
: m_context(context)
{
}

PipelineLayout::~PipelineLayout()
{
    destroy();
}
    
const VkPipelineLayout& PipelineLayout::getLayout() const
{
    return m_layout;
}
    
bool PipelineLayout::initialize(const std::vector<IntrusivePtr<DescriptorSetLayout>>& layouts)
{
    if(m_layout != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    std::vector<VkDescriptorSetLayout> vklayouts;
    vklayouts.reserve(layouts.size());
    for(const IntrusivePtr<DescriptorSetLayout>& layout : layouts)
    {
        vklayouts.push_back(layout->getLayout());
    }

	VkResult error;

    VkPipelineLayoutCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    createInfo.setLayoutCount = static_cast<uint32_t>(vklayouts.size());
    createInfo.pSetLayouts = vklayouts.data();
    createInfo.pushConstantRangeCount = 0;
    createInfo.pPushConstantRanges = nullptr;
    
    error = vkCreatePipelineLayout(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_layout);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreatePipelineLayout '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void PipelineLayout::destroy()
{
	if (m_layout != VK_NULL_HANDLE)
	{
		vkDestroyPipelineLayout(m_context.getDevice(), m_layout, NO_ALLOCATOR);
		m_layout = VK_NULL_HANDLE;
	}
}
} // ns bngfx
