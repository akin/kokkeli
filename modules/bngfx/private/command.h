
#ifndef BONA_GRAPHICS_COMMAND_H_ABV
#define BONA_GRAPHICS_COMMAND_H_ABV

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/resources/image.h>

namespace bngfx 
{
class Command
{
public:
    Command(const VkDevice& device, const VkCommandPool& pool, const VkQueue& queue);

    VkResult beginOneTimeCommand(VkCommandBuffer& commandBuffer);
    VkResult endOneTimeCommand(VkCommandBuffer& commandBuffer);

    void transitionImageLayout(VkCommandBuffer& commandBuffer, Image* image, VkImageLayout oldLayout, VkImageLayout newLayout);
private:
    const VkDevice& m_device;
    const VkCommandPool& m_pool;
    const VkQueue& m_queue;
};
} // ns bngfx

#endif // BONA_GRAPHICS_COMMAND_H_ABV