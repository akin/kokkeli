#include <bngfx/materiallayer.h>
#include <bngfx/resource/image.h>
#include <bngfx/resource/imageview.h>
#include <bngfx/resource/sampler.h>
#include <bngfx/resource/buffer.h>

#include <spdlog/spdlog.h>
#include "utils.h"

namespace bngfx
{
void MaterialLayer::setBuffer(DescriptorID id, const Buffer* buffer)
{
    m_buffers.push_back({
        id,
        buffer
    });
}

bool MaterialLayer::getBuffer(DescriptorID id, IntrusivePtr<Buffer>& buffer)
{
    for(auto& item : m_buffers)
    {
        if(item.id == id)
        {
            buffer = item.buffer;
            return true;
        }
    }
    return false;
}

void MaterialLayer::setImage(DescriptorID id, const Image* image, const ImageView* view, const Sampler* sampler)
{
    m_images.push_back({
        id,
        image,
        sampler,
        view
    });
}

bool MaterialLayer::getImage(DescriptorID id, IntrusivePtr<ImageView>& view, IntrusivePtr<Sampler>& sampler)
{
    for(auto& item : m_images)
    {
        if(item.id == id)
        {
            view = item.view;
            sampler = item.sampler;
            return true;
        }
    }
    return false;
}

void MaterialLayer::add(RenderTypeID id)
{
    m_rendertypes.insert(id);
}

void MaterialLayer::remove(RenderTypeID id)
{
    auto iter = m_rendertypes.find(id);
    if(iter !=  m_rendertypes.end())
    {
        m_rendertypes.erase(iter);
    }
}

bool MaterialLayer::isCompatible(RenderTypeID id) const
{
    return m_rendertypes.find(id) != m_rendertypes.end();
}

void MaterialLayer::setLayer(uint32_t layer)
{
    m_layer = layer;
}

uint32_t MaterialInfo::getLayer() const
{
    return m_layer;
}
} // ns bngfx
