#include <bngfx/swapchain.h>
#include <bngfx/context.h>
#include <bngfx/surface.h>

#include <bncommon/math.h>

#include <spdlog/spdlog.h>
#include "utils.h"
#include "command.h"

namespace bngfx
{
SwapChain::SwapChain(Context& context)
: m_context(context)
{
}

SwapChain::~SwapChain()
{
    destroy();
}

void SwapChain::setMinimumImageCount(size_t count)
{
    m_minimumImageCount = static_cast<uint32_t>(count);
}

void SwapChain::setPresentMode(VkPresentModeKHR presentMode)
{
    m_presentMode = presentMode;
}

void SwapChain::setResolution(uvec2 value)
{
    m_extent.width = value.x;
    m_extent.height = value.y;
}

void SwapChain::setFormat(bntypes::ImageFormat format)
{
    m_surfaceFormat.format = dataFormatToVkFormat(format);;
}

void SwapChain::setImageSpace(VkColorSpaceKHR colorSpace)
{
    m_surfaceFormat.colorSpace = colorSpace;
}

uvec2 SwapChain::getResolution() const
{
    return { m_extent.width, m_extent.height };
}

VkFormat SwapChain::getFormat() const
{
    return m_surfaceFormat.format;
}

uint32_t SwapChain::getImageCount() const
{
    return m_imageCount;
}

const VkSwapchainKHR& SwapChain::getSwapChain() const
{
    return m_swapChain;
}

const VkImage& SwapChain::getImage(size_t index) const
{
    return m_images[index];
}

bool SwapChain::initialize(const Surface& surface)
{
    if (m_swapChain != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized swapchain object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;
    uint32_t count;
    // Present modes
    error = vkGetPhysicalDeviceSurfacePresentModesKHR(m_context.getPhysicalDevice(), surface.getSurface(), &count, nullptr);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkGetPhysicalDeviceSurfacePresentModesKHR '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    std::vector<VkPresentModeKHR> presentModes;
    presentModes.resize(count);
    if(count > 0)
    {
        error = vkGetPhysicalDeviceSurfacePresentModesKHR(m_context.getPhysicalDevice(), surface.getSurface(), &count, presentModes.data());
        if (error != VK_SUCCESS) 
        {
            spdlog::error("{0}:{1} Failed vkGetPhysicalDeviceSurfacePresentModesKHR '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }
    {
        bool supported = false;
        for(VkPresentModeKHR mode : presentModes)
        {
            if(mode == m_presentMode)
            {
                supported = true;
                break;
            }
        }
        if(!supported)
        {
            spdlog::error("{0}:{1} PresentMode not available for surface <--> physicaldevice'.", __FILE__, __LINE__);
            assert(false);
            return false;
        }
    }

    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface.getSurface();
    createInfo.minImageCount = m_minimumImageCount;
    createInfo.imageFormat = m_surfaceFormat.format;
    createInfo.imageColorSpace = m_surfaceFormat.colorSpace;
    createInfo.imageExtent = m_extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    // TODO earlier we made assumption of indices.graphicsFamily == indices.presentFamily
    // here we reap on that assumption
    // https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Swap_chain#page_Creating-the-swap-chain
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.queueFamilyIndexCount = 0;
    createInfo.pQueueFamilyIndices = nullptr;

    createInfo.preTransform = surface.getCapabilities().currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = m_presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    error = vkCreateSwapchainKHR(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_swapChain) ;
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreateSwapchainKHR '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    error = vkGetSwapchainImagesKHR(m_context.getDevice(), m_swapChain, &m_imageCount, nullptr);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkGetSwapchainImagesKHR '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    m_images.resize(m_imageCount);
    error = vkGetSwapchainImagesKHR(m_context.getDevice(), m_swapChain, &m_imageCount, m_images.data());
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkGetSwapchainImagesKHR '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    return true;
}

void SwapChain::destroy()
{
    if (m_swapChain != VK_NULL_HANDLE)
    {
        vkDestroySwapchainKHR(m_context.getDevice(), m_swapChain, NO_ALLOCATOR);
        m_swapChain = VK_NULL_HANDLE;
    }
}
} // ns bngfx
