#include <bngfx/level.h>
#include <bngfx/vkcommon.h>
#include <bngfx/context.h>

namespace bngfx
{

const std::vector<Vertex> vertices = {
    {{-0.5f, -0.5f, 0.0f, 1.0f}, {1.0f, 0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
    {{0.5f, -0.5f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},
    {{0.5f, 0.5f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
    {{-0.5f, 0.5f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}
};
const std::vector<uint16_t> indices = {
    0, 1, 2, 2, 3, 0
};
size_t verticesSize;
size_t indicesSize;


Level::Level(Context& context)
: m_context(context)
{
    m_block = new ResourceBlock(m_context);
    m_block->setMemoryPropertyFlags(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
}

Level::~Level()
{
    destroy();
}

IntrusivePtr<ResourceBlock> Level::getBlock()
{
    return m_block;
}

IntrusivePtr<Sampler> Level::getSampler()
{
    return m_sampler;
}

Mesh *Level::createMesh()
{
    Mesh* mesh = new Mesh{};
    m_meshes.push_back(mesh);

    return mesh;
}

Image *Level::createImage()
{
    return m_block->createImage();
}

bool Level::initialize()
{
    {
        Mesh *mesh = createMesh();
        mesh->vertices = vertices;
        mesh->indices = indices;

        image = createImage();
        image->setup(m_imageReader->getImageInfo());
        image->setUsage(VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    }
    
    m_sampler = new Sampler(m_context);
    m_sampler->initialize();

    for(auto* mesh : m_meshes)
    {
        mesh->initialize(*m_block.get());
    }

    return m_block->allocate();
}

void Level::destroy()
{
    m_block.reset();

    for(auto* mesh : m_meshes)
    {
        delete mesh;
    }
    m_meshes.clear();
}

bool Level::apply(std::function<bool (Mesh*)> func)
{
    for(auto* mesh : m_meshes)
    {
        if(!func(mesh))
        {
            return false;
        }
    }
    return true;
}

size_t Level::getSize() const
{
    return m_meshes.size();
}

void Level::incrementReferenceCount()
{
    ++m_refCount;
}

void Level::decrementReferenceCount()
{
    assert(m_refCount != 0 && "Ref count 0, impossible.");
    --m_refCount;
    if(m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
