#include <bngfx/render/renderobject.h>
#include <bngfx/render/subpass.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
RenderObject::RenderObject(Context& context)
: m_context(context)
{
}

RenderObject::~RenderObject()
{
    destroy();
}

void RenderObject::setDirty(bool state)
{
    m_dirty = state;
}

bool RenderObject::isDirty() const
{
    return m_dirty;
}

void RenderObject::addChunk(Chunk* chunk)
{
    m_chunks.push_back(chunk);
}

void RenderObject::destroy()
{
}

const DescriptorSet& RenderObject::getDescriptorSet() const
{
    return m_set;
}

void RenderObject::setMaterial(const InstrusivePtr<Material>& material)
{
    m_material = material;
}

void RenderObject::setMesh(const InstrusivePtr<Mesh>& mesh)
{
    m_mesh = mesh;
}

void RenderObject::incrementReferenceCount()
{
    ++m_refCount;
}

void RenderObject::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
