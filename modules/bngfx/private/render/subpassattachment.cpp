#include <bngfx/render/subpassattachment.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
bool SubPassAttachment::initialize()
{
    return true;
}

void SubPassAttachment::destroy()
{
}

const VkAttachmentReference& SubPassAttachment::getAttachmentReference() const
{
    return m_reference;
}

void SubPassAttachment::setIndex(uint32_t index)
{
    m_reference.attachment = index;
}

void SubPassAttachment::setLayout(VkImageLayout layout)
{
    m_reference.layout = layout;
}

bool SubPassAttachment::isDepthStencil() const
{
    switch(m_reference.layout)
    {
        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : return true;
        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL : return true;
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL : return true;
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL : return true;
        default:
            break;
    }
    return false;
}

bool SubPassAttachment::isColor() const
{
    switch(m_reference.layout)
    {
        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL : return true;
        default:
            break;
    }
    return false;
}

VkImageLayout SubPassAttachment::getLayout() const
{
    return m_reference.layout;
}

uint32_t SubPassAttachment::getIndex() const
{
    return m_reference.attachment;
}

} // ns bngfx
