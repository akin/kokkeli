#include <bngfx/render/passattachment.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{

const VkAttachmentDescription& PassAttachment::getAttachmentDescription() const
{
    return m_attachment;
}

void PassAttachment::setFormat(VkFormat format)
{
    m_attachment.format = format;
}

void PassAttachment::setSamples(VkSampleCountFlagBits samples)
{
    m_attachment.samples = samples;
}

void PassAttachment::setLoadStoreOp(VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp)
{
    m_attachment.loadOp = loadOp;
    m_attachment.storeOp = storeOp;
}

void PassAttachment::setStencilLoadStoreOp(VkAttachmentLoadOp loadOp, VkAttachmentStoreOp storeOp)
{
    m_attachment.stencilLoadOp = loadOp;
    m_attachment.stencilStoreOp = storeOp;
}

void PassAttachment::setLayout(VkImageLayout initialLayout, VkImageLayout finalLayout)
{
    m_attachment.initialLayout = initialLayout;
    m_attachment.finalLayout = finalLayout;
}

void PassAttachment::setIndex(uint32_t index)
{
    m_index = index;
}

uint32_t PassAttachment::getIndex() const
{
    return m_index;
}

} // ns bngfx
