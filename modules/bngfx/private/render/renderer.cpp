#include <bngfx/render/renderer.h>
#include <bngfx/context.h>
#include "../utils.h"

namespace bngfx
{
Renderer::Renderer(Context& context)
: m_context(context)
{
}

Renderer::~Renderer()
{
    destroy();
}

const VkPipeline& Renderer::getPipeline() const
{
    return m_pipeline;
}

void Renderer::setInfo(const IntrusivePtr<RendererInfo>& info)
{
    m_info = info;
}

const IntrusivePtr<RendererInfo>& Renderer::getInfo() const
{
    return m_info;
}

RenderTypeID Renderer::getRenderTypeID() const
{
    return m_info->getRenderTypeID();
}

bool Renderer::initialize(const IntrusivePtr<Renderer>& previousRenderer, const SubPass* subpass)
{
    if (m_pipeline != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized pipeline object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }
    if (m_info != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize object, missing RenderInfo object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    VkResult error;

    std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
    {
        const std::vector<IntrusivePtr<Shader>>& shaders = m_info->getShaders();
        shaderStages.reserve(shaders.size());
        for (const IntrusivePtr<Shader>& shader : shaders)
        {
            VkPipelineShaderStageCreateInfo info{};
            info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            info.stage = shaderTypeToVkShaderStageFlagBits(shader->getType());
            info.pName = shader->getEntryPoint().c_str();
            info.module = shader->getShader();
            shaderStages.push_back(info);
        }
    }
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = {};
    {
        // TODO inputprimitive should come from chunks, but, at initialization we have no chunks yet.. or we do.. but we dont.. ahh.. CRABS!
        // anyhow, fix later, when we actually know what I am doing.
        inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyInfo.topology = primitiveTypeToVkPrimitiveTopology(m_info->getInputPrimitive());
        inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;
    }

    // Setup Viewport state
    VkPipelineViewportStateCreateInfo viewportInfo = {};
    std::vector<VkViewport> viewports;
    std::vector<VkRect2D> scissors;
    {
        subpass.getViewports(viewports);
        subpass.getScissors(scissors);

        viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportInfo.viewportCount = static_cast<uint32_t>(viewports.size());
        viewportInfo.pViewports = viewports.data();
        viewportInfo.scissorCount = static_cast<uint32_t>(scissors.size());
        viewportInfo.pScissors = scissors.data();
    }

    // Setup rasterizer
    // https://vulkan-tutorial.com/Drawing_a_triangle/Graphics_pipeline_basics/Fixed_functions#page_Rasterizer
    VkPipelineRasterizationStateCreateInfo rasterizationInfo = {};
    {
        // TODO! lots of things put into RendererInfo, invent a better place!
        rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizationInfo.depthClampEnable = VK_FALSE;

        // discard output, "disables output to framebuffer".
        rasterizationInfo.rasterizerDiscardEnable = m_info->getDiscardState() ? VK_TRUE : VK_FALSE;
        rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizationInfo.lineWidth = 1.0f;
        rasterizationInfo.cullMode = faceTypeToVkCullModeFlagBits(m_info->getCullFace());
        rasterizationInfo.frontFace = windingTypeToVkFrontFace(m_info->getWindingType());
    }

    // TODO!!
    // Multisampling AA
    VkPipelineMultisampleStateCreateInfo multiSampleInfo = {};
    {
        multiSampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multiSampleInfo.sampleShadingEnable = VK_FALSE;
        multiSampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multiSampleInfo.minSampleShading = 1.0f; // Optional
        multiSampleInfo.pSampleMask = nullptr; // Optional
        multiSampleInfo.alphaToCoverageEnable = VK_FALSE; // Optional
        multiSampleInfo.alphaToOneEnable = VK_FALSE; // Optional
    }

    // Depth and stencil testing
    VkPipelineDepthStencilStateCreateInfo depthAndStencilInfo = {};
    {
        depthAndStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthAndStencilInfo.depthTestEnable = VK_TRUE;
        depthAndStencilInfo.depthWriteEnable = VK_TRUE;
        depthAndStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
        depthAndStencilInfo.depthBoundsTestEnable = VK_FALSE;
        depthAndStencilInfo.minDepthBounds = 0.0f; // Optional
        depthAndStencilInfo.maxDepthBounds = 1.0f; // Optional
        depthAndStencilInfo.stencilTestEnable = VK_FALSE;
        depthAndStencilInfo.front = {}; // Optional
        depthAndStencilInfo.back = {}; // Optional
    }

    // Blending
    // complex topic.. even in opengl..
    // https://vulkan-tutorial.com/Drawing_a_triangle/Graphics_pipeline_basics/Fixed_functions#page_Color-blending
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    VkPipelineColorBlendStateCreateInfo colorBlendingInfo = {};
    {
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;
        colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; 
        colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; 
        colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; 
        colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; 
        colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; 
        colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; 

        colorBlendingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendingInfo.logicOpEnable = VK_FALSE;
        colorBlendingInfo.logicOp = VK_LOGIC_OP_COPY;
        colorBlendingInfo.attachmentCount = 1;
        colorBlendingInfo.pAttachments = &colorBlendAttachment;
        colorBlendingInfo.blendConstants[0] = 0.0f;
        colorBlendingInfo.blendConstants[1] = 0.0f;
        colorBlendingInfo.blendConstants[2] = 0.0f;
        colorBlendingInfo.blendConstants[3] = 0.0f; 
    }

    // Dynamic states
    // https://vulkan-tutorial.com/Drawing_a_triangle/Graphics_pipeline_basics/Fixed_functions#page_Dynamic-state
    VkPipelineDynamicStateCreateInfo dynamicStateInfo = {};
    std::vector<VkDynamicState> dynamicStates;
    {
        dynamicStates.push_back(VK_DYNAMIC_STATE_VIEWPORT);
        dynamicStates.push_back(VK_DYNAMIC_STATE_LINE_WIDTH);

        dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicStateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
        dynamicStateInfo.pDynamicStates = dynamicStates.data();
    }

    {
        VkGraphicsPipelineCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.stageCount = static_cast<uint32_t>(shaderStages.size()); // RendererInfo
        createInfo.pStages = shaderStages.data(); // RendererInfo
        createInfo.pVertexInputState = &m_info->getVertexInputAttributeLayout()->getInputStateCreateInfo(); // RendererInfo
        createInfo.pInputAssemblyState = &inputAssemblyInfo; // RendererInfo
        createInfo.pViewportState = &viewportInfo; // SubPass
        createInfo.pRasterizationState = &rasterizationInfo; // ?RendererInfo?
        createInfo.pMultisampleState = &multiSampleInfo; // ?RendererInfo?
        createInfo.pDepthStencilState = &depthAndStencilInfo; // ?RendererInfo?
        createInfo.pColorBlendState = &colorBlendingInfo; // ?RendererInfo?
        createInfo.pDynamicState = nullptr; // &dynamicStateInfo;  // ???
        createInfo.layout = m_info->getPipelineLayout()->getLayout(); // RendererInfo

        createInfo.renderPass = subpass->getRenderPass(); // SubPass
        createInfo.subpass = subpass->getIndex(); // SubPass

        createInfo.basePipelineHandle = VK_NULL_HANDLE; // ?previousRenderer?
        createInfo.basePipelineIndex = -1;

        error = vkCreateGraphicsPipelines(m_context.getDevice(), VK_NULL_HANDLE, 1, &createInfo, NO_ALLOCATOR, &m_pipeline);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateGraphicsPipelines '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    return true;
}

void Renderer::destroy()
{
    if (m_pipeline != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(m_context.getDevice(), m_pipeline, NO_ALLOCATOR);
        m_pipeline = VK_NULL_HANDLE;
    }
}

void Renderer::incrementReferenceCount()
{
    ++m_refCount;
}

void Renderer::decrementReferenceCount()
{
    --m_refCount;
    if(m_refCount == 0)
    {
        delete this;
    }
}

} // ns bngfx
