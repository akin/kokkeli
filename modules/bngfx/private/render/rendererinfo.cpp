#include <bngfx/render/rendererinfo.h>
#include <bngfx/shader/shader.h>
#include <bngfx/context.h>
#include "../utils.h"

namespace bngfx
{
RendererInfo::RendererInfo(Context& context)
: m_context(context)
{
}

RendererInfo::~RendererInfo()
{
    destroy();
}

const std::vector<IntrusivePtr<Shader>>& RendererInfo::getShaders() const
{
    return m_shaders;
}

bngfx::IntrusivePtr<PipelineLayout> RendererInfo::getPipelineLayout() const
{
    return m_pipelineLayout;
}

bngfx::IntrusivePtr<AttributeLayout> RendererInfo::getVertexInputAttributeLayout() const
{
    return m_vertexInputAttributeLayout;
}

size_t RendererInfo::getDescriptorSetCount() const
{
    return m_descriptorSetLayouts.size();
}

size_t RendererInfo::hasDescriptorSet(DescriptorSetID id) const
{
    uint32_t idc = static_cast<uint32_t>(id);
    return m_descriptorSetLayouts.size() > idc; 
}

const DescriptorData* RendererInfo::getDescriptorData() const
{
    return m_descriptors.data();
}

size_t RendererInfo::getDescriptorCount() const
{
    return m_descriptors.size();
}

RenderTypeID RendererInfo::getRenderTypeID() const
{
    return m_type;
}

PrimitiveType RendererInfo::getInputPrimitive() const
{
    return m_primitive;
}

void RendererInfo::setRenderTypeID(RenderTypeID id)
{
    m_type = id;
}

bool RendererInfo::getDiscardState() const
{
    return true;
}

FaceType RendererInfo::getCullFace() const
{
    return FaceType::back;
}

WindingType RendererInfo::getWindingType() const
{
    return WindingType::ccw;
}

bool RendererInfo::initialize(const std::vector<IntrusivePtr<Shader>>& shaders)
{
    // parse reflection data & shaders
    m_shaders = shaders;

    // Descriptors are a bit oddball, we have to find out what
    // shaders are interested of the descriptor & add that to the info.
    size_t descriptorSetCount = 0;
    std::unordered_map<DescriptorID, size_t> descriptorsIndexes;
    for(const IntrusivePtr<Shader>& shader : m_shaders)
    {
        ShaderType shaderType = shader->getType();
        // Descriptor schenanigans.
        {
            // TODO validation if previous data exists!
            // so that different shaders do not create conflicting descriptors
            const auto* items = shader->getDescriptorData();
            for (size_t i = 0; i < shader->getDescriptorSize(); ++i)
            {
                auto& item = items[i];
                auto iter = descriptorsIndexes.find(item.id);
                if (iter != descriptorsIndexes.end())
                {
                    // Found at iter.second!
                    // Add info that this shader stage is interested of that data.
                    m_descriptors[iter.second].shader.set(shaderType);
                    continue;
                }

                // Not found! lets add
                // Notice that m_descriptors will be the ones retaining real data
                // the m_descriptorsMap is just for casual reflections.
                descriptorsIndexes[item.id] = m_descriptors.size();
                m_descriptors.push_back(item);
                m_descriptorsMap[item.id] = item;

                if(item.set >= descriptorSetCount)
                {
                    descriptorSetCount = item.set + 1;
                }
            }
        }

        // Get vertex shader input attributes
        if(shaderType == ShaderType::vertex)
        {
            m_vertexInputAttributeLayout = new AttributeLayout{};
            m_vertexInputAttributeLayout->initialize(shader->getInputAttributeData());
        }
    }

    // DescriptorSetLayouts
    m_descriptorSetLayouts.reserve(descriptorSetCount);
    for(size_t i = 0 ; i < descriptorSetCount ; ++i)
    {
        m_descriptorSetLayouts.push_back(new DescriptorSetLayout{m_context});
    }
    
    // populate
    for(const DescriptorData& item : m_descriptors)
    {
        DescriptorLayout& layout = m_descriptorSetLayouts[item.set];
        if(item.type == DescriptorType::image)
        {
            layout.addImageBinding(item.binding, item.count, bitFlagsToVkShaderStageFlags(item.shader));
        }
        else if(item.type == DescriptorType::buffer)
        {
            layout.addBufferBinding(item.binding, item.count, bitFlagsToVkShaderStageFlags(item.shader));
        }
        else
        {
            assert(false && "Unknown item.type!");
        }
    }
    for(const auto& layout : m_descriptorSetLayouts)
    {
        if(!layout->initialize())
        {
            spdlog::error("{0}:{1} Failed to initialize DescriptorSetLayout.", __FILE__, __LINE__);
            assert(false);
            return false;
        }
    }

    // PipelineLayout
    m_pipelineLayout = new PipelineLayout{ m_context };
    if(!m_pipelineLayout->initialize(m_descriptorSetLayouts))
    {
        spdlog::error("{0}:{1} Failed to initialize PipelineLayout.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    // DescriptorPool
    m_descriptorPool = new DescriptorPool(m_context);
    for (size_t i = 0; i < ((size_t)VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT); ++i)
    {
        VkDescriptorType type = (VkDescriptorType)i;
        size_t count = 0;
        for(const auto& layout : m_descriptorSetLayouts)
        {
            count += layout->getDescriptorTypeCount(type);
        }
        if (count > 0)
        {
            m_descriptorPool->setDescriptorPoolSize(type, count * funnyNumber);
        }
    }       
    m_descriptorPool->setDescriptorSetCount(funnyNumber);
    if (!m_descriptorPool->allocate())
    {
        spdlog::error("{0}:{1} Failed to initialize descriptor pool.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    return true;
}

void RendererInfo::destroy()
{
}

void RendererInfo::setName(const std::string& name)
{
    m_name = name;
}

const std::string& RendererInfo::getName() const
{
    return m_name;
}

void RendererInfo::incrementReferenceCount()
{
    ++m_refCount;
}

void RendererInfo::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
