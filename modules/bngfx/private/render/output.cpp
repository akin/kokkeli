#include <bngfx/render/output.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Output::Output(Context& context)
: Node(context)
{
}

Output::~Output()
{
    destroy();
}

bool Output::initialize()
{
    VkResult error;
    {
        VkSemaphoreCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        error = vkCreateSemaphore(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_semaphore);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateSemaphore '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    return true;
}

void Output::destroy()
{
    if (m_semaphore != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(m_context.getDevice(), m_semaphore, NO_ALLOCATOR);
        m_semaphore = VK_NULL_HANDLE;
    }
}
} // ns bngfx
