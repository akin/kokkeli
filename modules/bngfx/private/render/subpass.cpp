#include <bngfx/render/subpass.h>
#include <bngfx/render/pass.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Subpass::Subpass(Context& context, Pass& pass)
: m_context(context)
, m_pass(pass)
{
}

Subpass::~Subpass()
{
    destroy();
}

const VkSubpassDescription& Subpass::getSubpassDescription() const
{
    return m_subpassDescription;
}

uint32_t Subpass::getIndex() const
{
    return 0;
}

bool Subpass::initialize()
{
    return true;
}

void Subpass::destroy()
{
}

const VkRenderPass& Subpass::getRenderPass() const
{
    return m_pass.getRenderPass();
}

void Subpass::getViewports(std::vector<VkViewport>& viewports) const
{
    m_pass.getViewports(viewports);
}

void Subpass::getScissors(std::vector<VkRect2D>& scissors) const
{
    m_pass.getScissors(scissors);
}

void Subpass::set(const IntrusivePtr<Renderer>& renderer)
{
    m_renderer = renderer;
}

void Subpass::record(const VkCommandBuffer& commandBuffer)
{
    if(!m_renderer)
    {
        return;
    }
    if(m_chunks.empty())
    {
        return;
    }

    std::vector<VkDescriptorSet> descriptorsets;
    descriptorsets.resize(DescriptorSetID::MAX);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->getPipeline());
    for(const auto& chunk : m_chunks)
    {
        const IntrusivePtr<Mesh>& mesh = chunk.getMesh();
        if(!mesh)
        {
            continue;
        }

        // Setup descriptorsets
        chunk->getDescriptorSets(descriptorsets.data());

        const IntrusivePtr<Buffer>& vertexes = mesh->getVertexBuffer();
        const IntrusivePtr<Buffer>& indexes = mesh->getIndexBuffer();

        // TODO! Binding multiple offsets of a buffer is more performant.
        // Render chunk
        // TODO! Chunk should be multiple materials, same mesh.. -> so Chunk isnt really complete.
        // though, for now, I think I just should move on, do the simplest, and then complicate it.
        VkBuffer vertexBuffers[] = {vertexes->getBuffer()};
        VkDeviceSize offsets[] = {vertexes->getOffset()};
        uint32_t firstBinding = 0;
        uint32_t bindingCount = 1;
        vkCmdBindVertexBuffers(commandBuffer, firstBinding, bindingCount, vertexBuffers, offsets);

        // I am puzzled _why_ the indexbuffer differs from the vertexbuffer in this way.
        vkCmdBindIndexBuffer(commandBuffer, indexes->getBuffer(), indexes->getOffset(), VK_INDEX_TYPE_UINT16);

        const uint32_t vertexCount = mesh->getVertexCount();
        const uint32_t instanceCount = 1;
        const uint32_t firstVertex = 0;
        const uint32_t vertexOffset = 0;
        const uint32_t firstInstance = 0;

        uint32_t firstSet = 0;
        vkCmdBindDescriptorSets(
            commandBuffer, 
            VK_PIPELINE_BIND_POINT_GRAPHICS, 
            m_renderer->getInfo()->getPipelineLayout()->getLayout(), 
            firstSet, 
            DescriptorSetID::MAX, 
            descriptorsets.data(), 
            0, 
            nullptr);

        vkCmdDrawIndexed(commandBuffer, 
            mesh->getIndicesCount(), 
            instanceCount, 
            firstVertex, 
            vertexOffset, 
            firstInstance);
    }
}

bool Subpass::attach(RenderObject& renderObject)
{
    if(!m_renderer)
    {
        spdlog::error("{0}:{1} Trying to attach object to subpass without renderer.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    // Material contains everything, RendererInfo defines what it wants to use
    // reflection parsing here.
    // Material & ReflectionData = Chunk descriptorset
    {
        const IntrusivePtr<RendererInfo>& info = m_renderer->getInfo();
        size_t count = info->getDescriptorCount();
        const DescriptorData* datas = info->getDescriptorData();

        for(const MaterialLayer& material : renderObject.getMaterial()->getLayers())
        {
            // Check material compatibility, if not compatible, move on.
            if(!material.isCompatible(info->getRenderTypeID()))
            {
                continue;
            }

            // If it is compatible, need to create a chunk and all
            Chunk *chunk = new Chunk(m_context, renderObject);
            DescriptorSet& descriptor = chunk->getDescriptorSet();

            // Populate descriptor.
            for(size_t index = 0 ; index < count ; ++index)
            {
                const DescriptorData& data = datas[index];
                if(data.type == DescriptorType::buffer)
                {
                    IntrusivePtr<Buffer> buffer;
                    if(!material.getBuffer(data.id, buffer))
                    {
                        // TODO!
                        // !Not found!
                        // Get DEFAULT buffer for this ID
                        spdlog::error("{0}:{1} Could not find requested buffer from material to be populated into chunk.", __FILE__, __LINE__);
                        assert(false);
                        return false;
                    }
                    descriptor.setBuffer(data.binding, buffer.get());
                }
                else if(data.type == DescriptorType::image)
                {
                    IntrusivePtr<ImageView> view; 
                    IntrusivePtr<Sampler> sampler;
                    
                    if(!material.getImage(data.id, view, sampler))
                    {
                        // TODO!
                        // !Not found!
                        // Get DEFAULT image for this ID
                        spdlog::error("{0}:{1} Could not find requested image from material to be populated into chunk.", __FILE__, __LINE__);
                        assert(false);
                        return false;
                    }
                    descriptor.setImageViewSampler(data.binding, view.get(), sampler.get());
                }
            }

            m_chunks.push_back(chunk);
            renderObject.addChunk(chunk);
        }
    }

    return true;
}
} // ns bngfx
