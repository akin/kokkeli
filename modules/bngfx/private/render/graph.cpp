#include <bngfx/render/graph.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Graph::Graph(Context& context)
: m_context(context)
{
}

Graph::~Graph()
{
    destroy();
}

bool Graph::initialize()
{
    return true;
}

void Graph::destroy()
{
}

void Graph::beginFrame()
{
    // Record command buffers.
}

void Graph::endFrame()
{
    // Send command primary buffer to the pipeline
}

const VkCommandPool& Graph::getCommandPool() const
{
    return m_commandPool;
}

void Graph::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Graph::getName() const
{
    return m_name;
}

void Graph::incrementReferenceCount()
{
    ++m_refCount;
}

void Graph::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
