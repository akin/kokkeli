#include <bngfx/render/chunk.h>
#include <bngfx/context.h>
#include "../utils.h"

namespace bngfx
{
Chunk::Chunk(Context& context, const IntrusivePtr<RenderObject>& object)
: m_context(context)
, m_set(context)
, m_object(object)
{
}

Chunk::~Chunk()
{
    destroy();
}

void Chunk::destroy()
{
}

void Chunk::getDescriptorSets(VkDescriptorSet* sets)
{
    sets[DescriptorSetID::object] = m_object->getDescriptorSet().getDescriptorSet();
    sets[DescriptorSetID::material] = m_set.getDescriptorSet();
}

const IntrusivePtr<Mesh>& Chunk::getMesh() const
{
    return m_object->getMesh();
}

DescriptorSet& Chunk::getDescriptorSet()
{
    return m_set;
}

void Chunk::incrementReferenceCount()
{
    ++m_refCount;
}

void Chunk::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
