#include <bngfx/render/frame.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
void Frame::setSize(size_t size)
{
    m_attachments.resize(size);
}

VkAttachmentDescription& Frame::get(size_t index)
{
    if(m_attachments.size() <= index)
    {
        m_attachments.resize(index + 1);
    }
    return m_attachments[index];
}

size_t Frame::getSize() const
{
    return m_attachments.size();
}
} // ns bngfx
