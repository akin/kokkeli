#include <bngfx/render/node.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Node::Node(Context& context)
: m_context(context)
{
}

Node::~Node()
{
}

VkFormat Node::getImageFormat() const
{
    return m_imageFormat;
}

VkFormat Node::getDepthFormat() const
{
    return m_depthFormat;
}

VkSemaphore Node::getSemaphore() const
{
    return m_semaphore;
}

void Node::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Node::getName() const
{
    return m_name;
}

void Node::incrementReferenceCount()
{
    ++m_refCount;
}

void Node::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
