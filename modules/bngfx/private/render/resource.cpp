#include <bngfx/render/resource.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Resource::Resource(Context& context)
: Node(context)
{
}

Resource::~Resource()
{
    destroy();
}

bool Resource::initialize()
{
    VkResult error;
    {
        VkSemaphoreCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        error = vkCreateSemaphore(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_semaphore);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateSemaphore '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    return true;
}

void Resource::destroy()
{
    if (m_semaphore != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(m_context.getDevice(), m_semaphore, NO_ALLOCATOR);
        m_semaphore = VK_NULL_HANDLE;
    }
}
} // ns bngfx
