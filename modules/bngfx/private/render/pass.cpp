#include <bngfx/render/pass.h>
#include <bngfx/render/graph.h>
#include <bngfx/commondata.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Pass::Pass(Context& context, Graph& graph)
: m_context(context)
, m_graph(graph)
{
}

Pass::~Pass()
{
    destroy();
}

bool Pass::attach(RenderObject& renderObject)
{
    // Subpass does the actual rendering.
    for(auto subpass : m_subpasses)
    {
        // Subpass creates chunks. if object attaches to anything.
        subpass.attach(renderObject);
    }
}

void Pass::setPrimitiveType(PrimitiveType value)
{
    m_inputPrimitive = value;
}

void Pass::setScissor(ivec2 offset, uvec2 size)
{
    m_scissorOffset = offset;
    m_scissorSize = size;
}

void Pass::setViewport(vec2 position, vec2 size, float minDepth, float maxDepth)
{
    m_viewportPosition = position;
    m_viewportSize = size;
    m_viewportMinDepth = minDepth;
    m_viewportMaxDepth = maxDepth;
}

void Pass::setRasterizerDiscard(bool value)
{
    m_rasterizerDiscardEnabled = value;
}

void Pass::setFrontFaceWinding(WindingType value)
{
    m_windingMode = value;
}

void Pass::setCullingMode(FaceType value)
{
    m_cullFace = value;
}

void Pass::setImageFormat(bntypes::ImageFormat format)
{
    m_imageFormat = dataFormatToVkFormat(format);
}

void Pass::setDepthFormat(VkFormat value)
{
    m_depthFormat = value;
}

void Pass::getViewports(std::vector<VkViewport>& viewports) const
{
    VkViewport viewport = {};
    viewport.x = m_viewportPosition.x;
    viewport.y = m_viewportPosition.y;
    viewport.width = m_viewportSize.x;
    viewport.height = m_viewportSize.y;
    viewport.minDepth = m_viewportMinDepth;
    viewport.maxDepth = m_viewportMaxDepth;
    viewports.push_back(viewport);
}

void Pass::getScissors(std::vector<VkRect2D>& scissors) const
{
    VkRect2D scissor = {};
    VkOffset2D offset = { m_scissorOffset.x, m_scissorOffset.y };
    VkExtent2D extent = { m_scissorSize.x, m_scissorSize.y };
    scissor.offset = offset;
    scissor.extent = extent;
    scissors.push_back(scissor);
}

const vec2& Pass::getViewportSize() const
{
    return m_viewportSize;
}

VkFormat Pass::getImageFormat() const
{
    return m_imageFormat;
}

VkFormat Pass::getDepthFormat() const
{
    return m_depthFormat;
}

void Pass::addOutput(IntrusivePtr<Resource> resource)
{
}

void Pass::removeOutput(IntrusivePtr<Resource> resource)
{
}

const VkRenderPass& Pass::getRenderPass() const
{
    return m_renderPass;
}

VkCommandBuffer Pass::getCommandBuffer()
{
    return m_commandBuffer;
}

bool Pass::initialize(const IntrusivePtr<Effect>& effect)
{
    if (m_pipeline != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized pipeline object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }
    if (m_renderPass != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized renderpass object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    VkResult error;

    // Pass setup
    // TODO, make it stronger!
    if (m_imageFormat == VK_FORMAT_UNDEFINED)
    {
        spdlog::error("{0}:{1} Pass to imageformat is set to undefined.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

    std::vector<VkAttachmentDescription> attachments;
    {
        // Color attachment
        VkAttachmentDescription attachment = {};
        attachment.format = m_imageFormat;
        attachment.samples = VK_SAMPLE_COUNT_1_BIT;

        // Color & Depth
        attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

        // Stencil is special unicorn? .. odd..
        attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

        attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        attachments.push_back(attachment);
    }
    {
        // Depth stencil attachment
        VkAttachmentDescription attachment = {};
        attachment.format = m_depthFormat;
        attachment.samples = VK_SAMPLE_COUNT_1_BIT;

        // Color & Depth
        attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

        // Stencil is special unicorn? .. odd..
        attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

        attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        attachments.push_back(attachment);
    }

    // subpasses
    std::vector<VkSubpassDescription> subpasses;
    std::vector<VkAttachmentReference> subpassesAttachmentRefColor;
    VkAttachmentReference subpassesAttachmentRefDepth = {};
    {
        {
            // Color
            VkAttachmentReference attachmentRef = {};
            attachmentRef.attachment = 0;
            attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            subpassesAttachmentRefColor.push_back(attachmentRef);
        }
        {
            // Depth Stencil
            subpassesAttachmentRefDepth.attachment = 1;
            subpassesAttachmentRefDepth.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        }

        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = static_cast<uint32_t>(subpassesAttachmentRefColor.size());
        subpass.pColorAttachments = subpassesAttachmentRefColor.data();
        subpass.pDepthStencilAttachment = &subpassesAttachmentRefDepth; // There can be only one.

        subpasses.push_back(subpass);
    }
    
    // subpass dependency
    std::vector<VkSubpassDependency> subpassDependencies;
    {
        // https://vulkan-tutorial.com/en/Drawing_a_triangle/Drawing/Rendering_and_presentation#page_Subpass-dependencies
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        subpassDependencies.push_back(dependency);
    }

    VkRenderPassCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    createInfo.pAttachments = attachments.data();
    createInfo.subpassCount = static_cast<uint32_t>(subpasses.size());
    createInfo.pSubpasses = subpasses.data();
    createInfo.dependencyCount = static_cast<uint32_t>(subpassDependencies.size());
    createInfo.pDependencies = subpassDependencies.data();

    error = vkCreatePass(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_renderPass);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkCreatePass '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    return true;
}

void Pass::destroy()
{
    if (m_renderFinished != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(m_context.getDevice(), m_renderFinished, NO_ALLOCATOR);
        m_renderFinished = VK_NULL_HANDLE;
    }
    if (m_renderPass != VK_NULL_HANDLE)
    {
        vkDestroyPass(m_context.getDevice(), m_renderPass, NO_ALLOCATOR);
        m_renderPass = VK_NULL_HANDLE;
    }
    if (m_pipeline != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(m_context.getDevice(), m_pipeline, NO_ALLOCATOR);
        m_pipeline = VK_NULL_HANDLE;
    }
}
    
bool Pass::record()
{
    VkResult error;
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = m_graph.getCommandPool();
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    allocInfo.commandBufferCount = (uint32_t) 1;

    error = vkAllocateCommandBuffers(device, &allocInfo, &m_commandBuffer);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkAllocateCommandBuffers '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Command_buffers#page_Starting-command-buffer-recording
    {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
        beginInfo.pInheritanceInfo = nullptr; // Optional
        error = vkBeginCommandBuffer(m_commandBuffer, &beginInfo);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkBeginCommandBuffer '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }
    // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Command_buffers#page_Starting-a-render-pass
    {
        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = renderPass;
        renderPassInfo.framebuffer = swapChainFramebuffers[i];
        renderPassInfo.renderArea.offset = {0, 0};
        renderPassInfo.renderArea.extent = swapChainExtent;
        
        VkClearValue clearColor = {0.0f, 0.0f, 0.0f, 1.0f};
        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues = &clearColor;

        vkCmdBeginRenderPass(m_commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    }

    size_t subpassCount = m_subpasses.size();
    if(subpassCount > 0)
    {
        m_subpasses.front().record(m_commandBuffer);
    }
    for(size_t index = 1 ; index < m_subpasses.size() ; ++index)
    {
        vkCmdNextSubpass(m_commandBuffer, VK_SUBPASS_CONTENTS_INLINE);
        m_subpasses[index].record(m_commandBuffer);
    }

    vkCmdEndRenderPass(m_commandBuffer);

    return true;
}

} // ns bngfx
