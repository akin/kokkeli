#include <bngfx/runtime.h>
#include <bngfx/context.h>
#include <algorithm>
#include <chrono>

#include <bncommon/math.h>

#include <spdlog/spdlog.h>
#include "utils.h"
#include "command.h"

namespace bngfx
{
/*
const std::vector<Vertex> vertices = {
    {{-0.5f, -0.5f, 0.0f, 1.0f}, {1.0f, 0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
    {{0.5f, -0.5f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},
    {{0.5f, 0.5f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
    {{-0.5f, 0.5f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}
};
const std::vector<uint16_t> indices = {
    0, 1, 2, 2, 3, 0
};
size_t verticesSize;
size_t indicesSize;

Runtime::Runtime(Context& context)
: m_context(context)
{
}

Runtime::~Runtime()
{
    destroy();
}

bool Runtime::initialize()
{
	VkResult error;
    uint32_t count;

    // Create swapchain
    {
        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = m_surface;
        createInfo.minImageCount = m_swapImageCount;
        createInfo.imageFormat = m_surfaceFormat.format;
        createInfo.imageColorSpace = m_surfaceFormat.colorSpace;
        createInfo.imageExtent = m_extent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        // TODO earlier we made assumption of indices.graphicsFamily == indices.presentFamily
        // here we reap on that assumption
        // https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Swap_chain#page_Creating-the-swap-chain
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = nullptr;

        createInfo.preTransform = m_capabilities.currentTransform;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = m_presentMode;
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        error = vkCreateSwapchainKHR(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_swapChain) ;
        if (error != VK_SUCCESS) 
        {
            spdlog::error("{0}:{1} Failed vkCreateSwapchainKHR '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }
    
    // Setup Level
    Image* image = nullptr;
    {
        m_level = new Level(m_context);

        Mesh *mesh = m_level->createMesh();
        mesh->vertices = vertices;
        mesh->indices = indices;

        image = m_level->createImage();
        image->setup(m_imageReader->getImageInfo());
        image->setUsage(VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

        if (!m_level->initialize())
        {
            spdlog::error("{0}:{1} Failed to initialize level.", __FILE__, __LINE__);
            assert(false);
            return false;
        }
    }

    // Create command pool
    {
        VkCommandPoolCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        createInfo.queueFamilyIndex = m_context.m_graphics.familyIndex;
        createInfo.flags = 0;

        error = vkCreateCommandPool(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_commandPoolGraphics);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateCommandPool '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    // Create command pool
    {
        VkCommandPoolCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        createInfo.queueFamilyIndex = m_context.m_transfer.familyIndex;
        createInfo.flags = 0;

        error = vkCreateCommandPool(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_commandPoolTransfer);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateCommandPool '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    uint32_t framebufferCount = 0;
    {
        // Setup frames
        std::vector<VkImage> images;
        error = vkGetSwapchainImagesKHR(m_context.getDevice(), m_swapChain, &framebufferCount, nullptr);
        if (error != VK_SUCCESS) 
        {
            spdlog::error("{0}:{1} Failed vkGetSwapchainImagesKHR '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
        images.resize(framebufferCount);
        error = vkGetSwapchainImagesKHR(m_context.getDevice(), m_swapChain, &framebufferCount, images.data());
        if (error != VK_SUCCESS) 
        {
            spdlog::error("{0}:{1} Failed vkGetSwapchainImagesKHR '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }

        // Command Buffers
        std::vector<VkCommandBuffer> commandBuffers;
        commandBuffers.resize(framebufferCount);
        VkCommandBufferAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocateInfo.commandPool = m_commandPoolGraphics;
        allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocateInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
        error = vkAllocateCommandBuffers(m_context.getDevice(), &allocateInfo, commandBuffers.data());
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkAllocateCommandBuffers '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
        
        m_frameGpuBlock = new ResourceBlock(m_context);
        m_frameGpuBlock->setMemoryPropertyFlags(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        m_frameCpuBlock = new ResourceBlock(m_context);
        m_frameCpuBlock->setMemoryPropertyFlags(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

        m_frames.resize(framebufferCount);
        for(size_t i = 0 ; i < framebufferCount ; ++i)
        {
            auto& frame = m_frames[i];

            // Image from swapchain
            frame.image = images[i];

            // View to image
            frame.view.reset(new ImageView(m_context));
            frame.view->initialize(frame.image, VK_IMAGE_VIEW_TYPE_2D, m_surfaceFormat.format);

            // Depth
            frame.depthImage = m_frameGpuBlock->createImage();
            frame.depthImage->setType(VK_IMAGE_TYPE_2D);
            frame.depthImage->setFormat(node.getDepthFormat());
            frame.depthImage->setSize(m_resolution);
            frame.depthImage->setDepth(1);
            frame.depthImage->setMipCount(1);
            frame.depthImage->setArrayCount(1);
            frame.depthImage->setUsage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);
            frame.depthImage->setAspectMask(VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT);

            // UBO
            frame.ubo = m_frameCpuBlock->createBuffer();
            frame.ubo->setByteSize(sizeof(ModelViewProjection));
            frame.ubo->setUsage(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);

            // CommandBuffers
            frame.commandBuffer = commandBuffers[i];
        }

        // Allocate blocks
        if (!m_frameGpuBlock->allocate())
        {
            spdlog::error("{0}:{1} Failed to initialize frame gpu block.", __FILE__, __LINE__);
            assert(false);
            return false;
        }
        if (!m_frameCpuBlock->allocate())
        {
            spdlog::error("{0}:{1} Failed to initialize frame cpu block.", __FILE__, __LINE__);
            assert(false);
            return false;
        }

        // Setup framebuffers round#2, descriptorsets
        {
            // Setup descriptor layouts into a set
            m_descriptorLayoutSet = descriptorLayoutSet;

            // Create pool
            m_descriptorPool = new DescriptorPool(m_context);

            // In total we will have framebufferCount count of binding types
            for (size_t i = 0; i < ((size_t)VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT); ++i)
            {
                VkDescriptorType type = (VkDescriptorType)i;
                size_t count = descriptorLayoutSet->getDescriptorTypeCount(type) * framebufferCount;
                if (count > 0)
                {
                    m_descriptorPool->setDescriptorPoolSize(type, count);
                }
            }
            // In total we will have framebufferCount count of descriptorsets.
            m_descriptorPool->setDescriptorSetCount(framebufferCount);
            if (!m_descriptorPool->allocate())
            {
                spdlog::error("{0}:{1} Failed to initialize descriptor pool.", __FILE__, __LINE__);
                assert(false);
                return false;
            }
        }
        for (size_t i = 0; i < framebufferCount; ++i)
        {
            auto& frame = m_frames[i];

            // FrameBuffer
            std::vector<VkImageView> attachments;
            attachments.push_back(frame.view->getView());
            attachments.push_back(frame.depthImage->getView()->getView());

            VkFramebufferCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            createInfo.renderPass = node.getRenderPass();
            createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
            createInfo.pAttachments = attachments.data();
            createInfo.width = m_extent.width;
            createInfo.height = m_extent.height;
            createInfo.layers = 1;

            error = vkCreateFramebuffer(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &frame.frameBuffer);
            if (error != VK_SUCCESS)
            {
                spdlog::error("{0}:{1} Failed vkCreateFramebuffer '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }

            // Descriptorset
            frame.descriptorSet = new DescriptorSet(m_context);
            if (!frame.descriptorSet->initialize(m_descriptorPool.get(), m_descriptorLayoutSet.get()))
            {
                spdlog::error("{0}:{1} Failed to initialize descriptor set.", __FILE__, __LINE__);
                assert(false);
                return false;
            }
            frame.descriptorSet->setDescriptorSetBuffer(0, 0, frame.ubo);
            frame.descriptorSet->setDescriptorSetImageSampler(0, 1, image, m_level->getSampler().get());
        }
    }

    // Copy data from STAGING to GPU
    {
        m_stagingBlock = new ResourceBlock(m_context);
        m_stagingBlock->setMemoryPropertyFlags(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

        m_stagingBuffer = m_stagingBlock->createBuffer();
        m_stagingBuffer->setByteSize(STAGING_BUFFER_SIZE);
        m_stagingBuffer->setUsage(VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
        if (!m_stagingBlock->allocate())
        {
            spdlog::error("{0}:{1} Failed to initialize staging block.", __FILE__, __LINE__);
            assert(false);
            return false;
        }

        // Vertexes & indexes;
        {
            // Stage data
            verticesSize = vertices.size() * sizeof(Vertex);
            indicesSize = indices.size() * sizeof(uint16_t);
            if (!m_stagingBuffer->setData(vertices.data(), 0, verticesSize))
            {
                spdlog::error("{0}:{1} Failed to set vertex data to buffer.", __FILE__, __LINE__);
                assert(false);
                return false;
            }
            if (!m_stagingBuffer->setData(indices.data(), verticesSize, indicesSize))
            {
                spdlog::error("{0}:{1} Failed to set indices data to buffer.", __FILE__, __LINE__);
                assert(false);
                return false;
            }
        }

        Command command(m_context.getDevice(), m_commandPoolTransfer, m_context.m_transfer.queue);
        VkCommandBuffer commandBuffer;
        error = command.beginOneTimeCommand(commandBuffer);
        if (error != VK_SUCCESS) 
        {
            spdlog::error("{0}:{1} Failed beginOneTimeCommand '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }

        if(!m_level->apply([&](Mesh* mesh) -> bool {
            // Send data to Vertex buffer
            mesh->m_vertexBuffer->setData(m_stagingBuffer, commandBuffer, 0, 0, verticesSize);

            // Send data to Index buffer
            mesh->m_indexBuffer->setData(m_stagingBuffer, commandBuffer, verticesSize, 0, indicesSize);
            return true;
        })) {
            spdlog::error("{0}:{1} Failed to stage mesh data.", __FILE__, __LINE__);
            assert(false);
            return false;
        }
        error = command.endOneTimeCommand(commandBuffer);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed endOneTimeCommand '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }

        // Setup image
        // Imagedata:
        {
            // Stage data
            const auto& dataInfo = m_imageReader->getDataInfo();
            void *target = nullptr;
            if (!m_stagingBuffer->map(target, 0, dataInfo.byteSize))
            {
                spdlog::error("{0}:{1} Failed to set image data to buffer.", __FILE__, __LINE__);
                assert(false);
                return false;
            }

            if (m_imageReader->readBytes(target, dataInfo.byteSize, 0) != dataInfo.byteSize)
            {
                spdlog::error("{0}:{1} Failed to read image data to buffer.", __FILE__, __LINE__);
                assert(false);
                return false;
            }
            m_stagingBuffer->unmap(); 
        }

        {
            Command command(m_context.getDevice(), m_commandPoolTransfer, m_context.m_transfer.queue);
            VkCommandBuffer commandBuffer;
            error = command.beginOneTimeCommand(commandBuffer);
            if (error != VK_SUCCESS)
            {
                spdlog::error("{0}:{1} Failed beginOneTimeCommand '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }

            const auto& info = m_imageReader->getImageInfo();

            // Do commands
            // Transform the image layout from undefined to transfer dst layout.
            command.transitionImageLayout(commandBuffer, image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
            image->setData(m_stagingBuffer, commandBuffer, info.width, info.height, info.depth, 0, 0, 1);
            // Transform the image layout from transfer dst layout to shader read only.
            command.transitionImageLayout(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

            error = command.endOneTimeCommand(commandBuffer);
            if (error != VK_SUCCESS)
            {
                spdlog::error("{0}:{1} Failed endOneTimeCommand '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }
        }

        // Transition depthbuffers to Optimal layout
        {
            Command command(m_context.getDevice(), m_commandPoolTransfer, m_context.m_transfer.queue);
            VkCommandBuffer commandBuffer;
            error = command.beginOneTimeCommand(commandBuffer);
            if (error != VK_SUCCESS)
            {
                spdlog::error("{0}:{1} Failed beginOneTimeCommand '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }

            for (auto& frame : m_frames)
            {
                command.transitionImageLayout(commandBuffer, frame.depthImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
            }

            error = command.endOneTimeCommand(commandBuffer);
            if (error != VK_SUCCESS)
            {
                spdlog::error("{0}:{1} Failed endOneTimeCommand '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }
        }
    }

    // Record Command buffers
    {
        for (size_t i = 0; i < framebufferCount; ++i) 
        {
            auto& frame = m_frames[i];

            // Begin recording.
            VkCommandBufferBeginInfo beginInfo = {};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
            beginInfo.pInheritanceInfo = nullptr;

            error = vkBeginCommandBuffer(frame.commandBuffer, &beginInfo);
            if (error != VK_SUCCESS) 
            {
                spdlog::error("{0}:{1} Failed vkBeginCommandBuffer '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }
            
            {
                // Start renderpass
                std::vector<VkClearValue> clearValues;
                {
                    VkClearValue clear{};
                    clear.color = { 0.0f, 0.0f, 0.0f, 1.0f };
                    clearValues.push_back(clear);
                }
                {
                    VkClearValue clear{};
                    clear.depthStencil = { 1.0f, 0 };
                    clearValues.push_back(clear);
                }

                VkRenderPassBeginInfo renderPassInfo = {};
                renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                renderPassInfo.renderPass = node.getRenderPass();
                renderPassInfo.framebuffer = frame.frameBuffer;
                renderPassInfo.renderArea.offset = {0, 0}; // TODO what is this?
                renderPassInfo.renderArea.extent = m_extent; // TODO what is this?

                // clearing the data at ClearOp with the values:
                renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
                renderPassInfo.pClearValues = clearValues.data();
                vkCmdBeginRenderPass(frame.commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

                // Bind pipeline
                vkCmdBindPipeline(frame.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, node.getPipeline());

                // Draw 
                {
                    if(!m_level->apply([&](Mesh* mesh) -> bool {

                        VkBuffer vertexBuffers[] = {mesh->m_vertexBuffer->getBuffer()};
                        VkDeviceSize offsets[] = {0};
                        uint32_t firstBinding = 0;
                        uint32_t bindingCount = 1;
                        vkCmdBindVertexBuffers(frame.commandBuffer, firstBinding, bindingCount, vertexBuffers, offsets);

                        uint32_t indexBufferOffset = 0;
                        vkCmdBindIndexBuffer(frame.commandBuffer, mesh->m_indexBuffer->getBuffer(), indexBufferOffset, VK_INDEX_TYPE_UINT16);

                        const uint32_t vertexCount = static_cast<uint32_t>(mesh->vertices.size());
                        const uint32_t instanceCount = 1;
                        const uint32_t firstVertex = 0;
                        const uint32_t vertexOffset = 0;
                        const uint32_t firstInstance = 0;

                        //vkCmdDraw(m_commandBuffers[i], vertexCount, instanceCount, firstVertex, firstInstance);
                        uint32_t firstSet = 0;
                        vkCmdBindDescriptorSets(frame.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, layout.getLayout(), firstSet, static_cast<uint32_t>(frame.descriptorSet->size()), frame.descriptorSet->data(), 0, nullptr);

                        vkCmdDrawIndexed(frame.commandBuffer, static_cast<uint32_t>(mesh->indices.size()), instanceCount, firstVertex, vertexOffset, firstInstance);

                        return true;
                    })) {
                        spdlog::error("{0}:{1} Failed to create mesh draw commands.", __FILE__, __LINE__);
                        assert(false);
                        return false;
                    }
                }

                // End renderpass
                vkCmdEndRenderPass(frame.commandBuffer);
            }

            // End recording.
            error = vkEndCommandBuffer(frame.commandBuffer);
            if (error != VK_SUCCESS) 
            {
                spdlog::error("{0}:{1} Failed vkEndCommandBuffer '{2}'.", __FILE__, __LINE__, toString(error));
                assert(false);
                return false;
            }
        }
    }
    
    {
        VkSemaphoreCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        error = vkCreateSemaphore(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_imageAvailable);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateSemaphore '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
        
        error = vkCreateSemaphore(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_renderFinished);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateSemaphore '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    {
        VkFenceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

        error = vkCreateFence(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_frameFence);
        if (error != VK_SUCCESS)
        {
            spdlog::error("{0}:{1} Failed vkCreateFence '{2}'.", __FILE__, __LINE__, toString(error));
            assert(false);
            return false;
        }
    }

    return true;
}

void Runtime::destroy()
{
    for (auto& frame : m_frames)
    {
        vkDestroyFramebuffer(m_context.getDevice(), frame.frameBuffer, NO_ALLOCATOR);
    }
    m_frameGpuBlock.reset();
    m_frameCpuBlock.reset();

    m_stagingBlock.reset();
    m_level.reset();
    m_descriptorLayoutSet.reset();
    m_descriptorPool.reset();

    if (m_frameFence != VK_NULL_HANDLE)
    {
        vkDestroyFence(m_context.getDevice(), m_frameFence, NO_ALLOCATOR);
        m_frameFence = VK_NULL_HANDLE;
    }
    if (m_imageAvailable != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(m_context.getDevice(), m_imageAvailable, NO_ALLOCATOR);
        m_imageAvailable = VK_NULL_HANDLE;
    }
    if (m_renderFinished != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(m_context.getDevice(), m_renderFinished, NO_ALLOCATOR);
        m_renderFinished = VK_NULL_HANDLE;
    }


    if (m_commandPoolTransfer != VK_NULL_HANDLE)
    {
        vkDestroyCommandPool(m_context.getDevice(), m_commandPoolTransfer, NO_ALLOCATOR);
        m_commandPoolTransfer = VK_NULL_HANDLE;
    }
    if (m_commandPoolGraphics != VK_NULL_HANDLE)
    {
        vkDestroyCommandPool(m_context.getDevice(), m_commandPoolGraphics, NO_ALLOCATOR);
        m_commandPoolGraphics = VK_NULL_HANDLE;
    }

    if (m_swapChain != VK_NULL_HANDLE)
    {
        vkDestroySwapchainKHR(m_context.getDevice(), m_swapChain, NO_ALLOCATOR);
        m_swapChain = VK_NULL_HANDLE;
    }
    
    if (m_surface != VK_NULL_HANDLE)
    {
        vkDestroySurfaceKHR(m_context.getInstance(), m_surface, NO_ALLOCATOR);
        m_surface = VK_NULL_HANDLE;
    }
}

bool Runtime::draw()
{
	VkResult error;

    error = vkResetFences(m_context.getDevice(), 1, &m_frameFence);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkResetFences '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    uint32_t imageIndex;
    error = vkAcquireNextImageKHR(m_context.getDevice(), m_swapChain, UINT64_MAX, m_imageAvailable, VK_NULL_HANDLE, &imageIndex);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkAcquireNextImageKHR '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    auto& frame = m_frames[imageIndex];
    updateUbo(imageIndex);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    std::vector<VkSemaphore> waitSemaphores;
    waitSemaphores.push_back(m_imageAvailable);

    std::vector<VkSemaphore> finishedSemaphores;
    finishedSemaphores.push_back(m_renderFinished);

    std::vector<VkCommandBuffer> commandBuffers;
    commandBuffers.push_back(frame.commandBuffer);

    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.pWaitDstStageMask = waitStages;

    submitInfo.waitSemaphoreCount = static_cast<uint32_t>(waitSemaphores.size());
    submitInfo.pWaitSemaphores = waitSemaphores.data();

    submitInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
    submitInfo.pCommandBuffers = commandBuffers.data();

    submitInfo.signalSemaphoreCount = static_cast<uint32_t>(finishedSemaphores.size());
    submitInfo.pSignalSemaphores = finishedSemaphores.data();

    error = vkQueueSubmit(m_context.m_graphics.queue, 1, &submitInfo, m_frameFence);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkQueueSubmit '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    std::vector<VkSwapchainKHR> swapChains;
    swapChains.push_back(m_swapChain);

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = static_cast<uint32_t>(finishedSemaphores.size());
    presentInfo.pWaitSemaphores = finishedSemaphores.data();
    presentInfo.swapchainCount = static_cast<uint32_t>(swapChains.size());
    presentInfo.pSwapchains = swapChains.data();
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    error = vkQueuePresentKHR(m_context.m_present.queue, &presentInfo);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkQueuePresentKHR '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    error = vkWaitForFences(m_context.getDevice(), 1, &m_frameFence, VK_TRUE, UINT64_MAX);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkWaitForFences '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }    

    return true;
}

void Runtime::incrementReferenceCount()
{
}

void Runtime::decrementReferenceCount()
{
}

void Runtime::updateUbo(uint32_t index)
{
    auto& frame = m_frames[index];
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    ModelViewProjection mvp;

    mvp.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    mvp.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    mvp.projection = glm::perspective(glm::radians(45.0f), m_resolution.x / (float) m_resolution.y, 0.1f, 10.0f);

    mvp.projection[1][1] *= -1;

    frame.ubo->setData(&mvp, 0, sizeof(ModelViewProjection));
}
*/
} // ns bngfx
