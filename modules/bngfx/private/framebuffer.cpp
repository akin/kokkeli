#include <bngfx/framebuffer.h>
#include <bngfx/context.h>

#include <bncommon/math.h>

#include <spdlog/spdlog.h>
#include "utils.h"
#include "command.h"

namespace bngfx
{
FrameBuffer::FrameBuffer(Context& context)
: m_context(context)
{
}

FrameBuffer::~FrameBuffer()
{
    destroy();
}

void FrameBuffer::attach(const ImageView *view)
{
    m_attachments.push_back(view);
}

const VkFramebuffer& FrameBuffer::getFrameBuffer() const
{
    return m_frameBuffer;
}
/*
bool FrameBuffer::initialize(const Node& node)
{
    if (m_frameBuffer != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized framebuffer object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;

    std::vector<VkImageView> attachments;
    attachments.reserve(m_attachments.size());
    for(auto ptr : m_attachments)
    {
        attachments.push_back(ptr->getView());
    }

    auto& viewSize = node.getViewportSize();

    VkFramebufferCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    createInfo.renderPass = node.getRenderPass();
    createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    createInfo.pAttachments = attachments.data();
    createInfo.width = static_cast<uint32_t>(viewSize.x);
    createInfo.height = static_cast<uint32_t>(viewSize.y);
    createInfo.layers = 1;

    error = vkCreateFramebuffer(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_frameBuffer);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkCreateFramebuffer '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }

    return true;
}
/**/
void FrameBuffer::destroy()
{
    if (m_frameBuffer != VK_NULL_HANDLE)
    {
        vkDestroyFramebuffer(m_context.getDevice(), m_frameBuffer, NO_ALLOCATOR);
        m_frameBuffer = VK_NULL_HANDLE;
    }
}
} // ns bngfx
