#include "utils.h"
#include <volk.c>

namespace bngfx 
{
std::string toString(VkResult result)
{
    #define CASE_STR(dname) case dname : return #dname
    switch(result)
    {
        CASE_STR(VK_SUCCESS);
        CASE_STR(VK_NOT_READY);
        CASE_STR(VK_TIMEOUT);
        CASE_STR(VK_EVENT_SET);
        CASE_STR(VK_EVENT_RESET);
        CASE_STR(VK_INCOMPLETE);
        CASE_STR(VK_ERROR_OUT_OF_HOST_MEMORY);
        CASE_STR(VK_ERROR_OUT_OF_DEVICE_MEMORY);
        CASE_STR(VK_ERROR_INITIALIZATION_FAILED);
        CASE_STR(VK_ERROR_DEVICE_LOST);
        CASE_STR(VK_ERROR_MEMORY_MAP_FAILED);
        CASE_STR(VK_ERROR_LAYER_NOT_PRESENT);
        CASE_STR(VK_ERROR_EXTENSION_NOT_PRESENT);
        CASE_STR(VK_ERROR_FEATURE_NOT_PRESENT);
        CASE_STR(VK_ERROR_INCOMPATIBLE_DRIVER);
        CASE_STR(VK_ERROR_TOO_MANY_OBJECTS);
        CASE_STR(VK_ERROR_FORMAT_NOT_SUPPORTED);
        CASE_STR(VK_ERROR_SURFACE_LOST_KHR);
        CASE_STR(VK_SUBOPTIMAL_KHR);
        CASE_STR(VK_ERROR_OUT_OF_DATE_KHR);
        CASE_STR(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
        CASE_STR(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
        CASE_STR(VK_ERROR_VALIDATION_FAILED_EXT);
        default:
            break;
    }
    #undef CASE_STR
    assert(false);
    return "";
}

VkShaderStageFlagBits shaderTypeToVkShaderStageFlagBits(ShaderType type)
{
    #define CASE_TYPE(AA, BB) case ShaderType::AA : return BB
    switch(type)
    {
        CASE_TYPE(vertex, VK_SHADER_STAGE_VERTEX_BIT);
        CASE_TYPE(fragment, VK_SHADER_STAGE_FRAGMENT_BIT);
        CASE_TYPE(compute, VK_SHADER_STAGE_COMPUTE_BIT);
        CASE_TYPE(tesselationControl, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT);
        CASE_TYPE(tesselationEvaluation, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT);
        CASE_TYPE(geometry, VK_SHADER_STAGE_GEOMETRY_BIT);
        CASE_TYPE(mesh, VK_SHADER_STAGE_MESH_BIT_NV);
        CASE_TYPE(rayGeneration, VK_SHADER_STAGE_RAYGEN_BIT_NV);
        CASE_TYPE(rayIntersection, VK_SHADER_STAGE_INTERSECTION_BIT_NV);
        CASE_TYPE(rayAnyHit, VK_SHADER_STAGE_ANY_HIT_BIT_NV);
        CASE_TYPE(rayClosestHit, VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV);
        CASE_TYPE(rayMiss, VK_SHADER_STAGE_MISS_BIT_NV);
        CASE_TYPE(rayCallable, VK_SHADER_STAGE_CALLABLE_BIT_NV);
        default:
            break;
    }
    #undef CASE_TYPE
    assert(false);
    return VK_SHADER_STAGE_VERTEX_BIT;
}

VkPrimitiveTopology primitiveTypeToVkPrimitiveTopology(PrimitiveType type)
{
    #define CASE_TYPE(AA, BB) case PrimitiveType::AA : return BB
    switch(type)
    {
        CASE_TYPE(points, VK_PRIMITIVE_TOPOLOGY_POINT_LIST);
        CASE_TYPE(triangles, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
        default:
            break;
    }
    #undef CASE_TYPE
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
}

VkCullModeFlagBits faceTypeToVkCullModeFlagBits(FaceType type)
{
#define CASE_TYPE(AA, BB) case FaceType::AA : return BB
    switch (type)
    {
        CASE_TYPE(back, VK_CULL_MODE_BACK_BIT);
        CASE_TYPE(front, VK_CULL_MODE_FRONT_BIT);
        CASE_TYPE(backAndFront, VK_CULL_MODE_FRONT_AND_BACK);
    default:
        break;
    }
#undef CASE_TYPE
    return VK_CULL_MODE_NONE;
}

VkFrontFace windingTypeToVkFrontFace(WindingType type)
{
#define CASE_TYPE(AA, BB) case WindingType::AA : return BB
    switch (type)
    {
        CASE_TYPE(cw, VK_FRONT_FACE_CLOCKWISE);
        CASE_TYPE(ccw, VK_FRONT_FACE_COUNTER_CLOCKWISE);
    default:
        break;
    }
#undef CASE_TYPE
    assert(false);
    return VK_FRONT_FACE_CLOCKWISE;
}


#define MATCH_TYPES(UTILSType, COUNT, VKType) \
    case bntypes::ImageFormat::R_##UTILSType##COUNT : return VK_FORMAT_R##COUNT##_##VKType; \
    case bntypes::ImageFormat::RG_##UTILSType##COUNT : return VK_FORMAT_R##COUNT##G##COUNT##_##VKType; \
    case bntypes::ImageFormat::RGB_##UTILSType##COUNT : return VK_FORMAT_R##COUNT##G##COUNT##B##COUNT##_##VKType; \
    case bntypes::ImageFormat::RGBA_##UTILSType##COUNT : return VK_FORMAT_R##COUNT##G##COUNT##B##COUNT##A##COUNT##_##VKType

VkFormat dataFormatToVkFormat(bntypes::DataFormat format)
{
    if (format >= static_cast<DataFormat>(bntypes::ImageFormat::count))
    {
        assert(false);
        return VK_FORMAT_UNDEFINED;
    }
    bntypes::ImageFormat imageformat = static_cast<bntypes::ImageFormat>(format);
    switch(imageformat)
    {
        MATCH_TYPES(uint, 8, UINT);
        MATCH_TYPES(sint, 8, SINT);
        MATCH_TYPES(unorm, 8, UNORM);
        MATCH_TYPES(snorm, 8, SNORM);
        MATCH_TYPES(srgb, 8, SRGB);

        MATCH_TYPES(uint, 16, UINT);
        MATCH_TYPES(sint, 16, SINT);
        MATCH_TYPES(sfloat, 16, SFLOAT);
        MATCH_TYPES(unorm, 16, UNORM);
        MATCH_TYPES(snorm, 16, SNORM);

        MATCH_TYPES(uint, 32, UINT);
        MATCH_TYPES(sint, 32, SINT);
        MATCH_TYPES(sfloat, 32, SFLOAT);
        default:
            break;
    }
    assert(false);
    return VK_FORMAT_UNDEFINED;
}
#undef MATCH_TYPES

VkImageType imageTypeToVkImageType(bntypes::ImageType type)
{
    switch(type)
    {
        case bntypes::ImageType::type1D: return VK_IMAGE_TYPE_1D;
        case bntypes::ImageType::type2D: return VK_IMAGE_TYPE_2D;
        case bntypes::ImageType::type3D: return VK_IMAGE_TYPE_3D;
        default:
            break;
    }
    assert(false);
    return VK_IMAGE_TYPE_2D;
}


VkShaderStageFlagBits bitFlagsToVkShaderStageFlags(const BitFlags<ShaderType>& flags)
{
    VkShaderStageFlagBits ret;

    ret |= flags.get(ShaderType::vertex) ? VK_SHADER_STAGE_VERTEX_BIT : 0;
    ret |= flags.get(ShaderType::fragment) ? VK_SHADER_STAGE_FRAGMENT_BIT   : 0;
    ret |= flags.get(ShaderType::compute) ? VK_SHADER_STAGE_COMPUTE_BIT  : 0;
    ret |= flags.get(ShaderType::tesselationControl) ? VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT  : 0;
    ret |= flags.get(ShaderType::tesselationEvaluation) ? VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT  : 0;
    ret |= flags.get(ShaderType::geometry) ? VK_SHADER_STAGE_GEOMETRY_BIT   : 0;
    ret |= flags.get(ShaderType::mesh) ? VK_SHADER_STAGE_MESH_BIT_NV  : 0;
    ret |= flags.get(ShaderType::rayGeneration) ? VK_SHADER_STAGE_RAYGEN_BIT_NV  : 0;
    ret |= flags.get(ShaderType::rayIntersection) ? VK_SHADER_STAGE_INTERSECTION_BIT_NV  : 0;
    ret |= flags.get(ShaderType::rayAnyHit) ? VK_SHADER_STAGE_ANY_HIT_BIT_NV  : 0;
    ret |= flags.get(ShaderType::rayClosestHit) ? VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV  : 0;
    ret |= flags.get(ShaderType::rayMiss) ? VK_SHADER_STAGE_MISS_BIT_NV  : 0;
    ret |= flags.get(ShaderType::rayCallable) ? VK_SHADER_STAGE_CALLABLE_BIT_NV  : 0;

    return ret;
}


} // ns bngfx
