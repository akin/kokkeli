#include <bngfx/resource/sampler.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Sampler::Sampler(Context& context)
: m_context(context)
{
}

Sampler::~Sampler()
{
    destroy();
}

void Sampler::setFilters(VkFilter min, VkFilter mag)
{
    m_filterMin = min;
    m_filterMag = mag;
}

void Sampler::setMipmapMode(VkSamplerMipmapMode value)
{
    m_mipmapMode = value;
}

void Sampler::setMipMapLod(float bias, float min, float max)
{
    m_mipLodBias = bias;
    m_mipLodMin = min;
    m_mipLodMax = max;
}

void Sampler::setAddressmodes(VkSamplerAddressMode modeU, VkSamplerAddressMode modeV, VkSamplerAddressMode modeW)
{
    m_addressModeU = modeU;
    m_addressModeV = modeV;
    m_addressModeW = modeW;
}

void Sampler::setAnisotropy(bool enabled, float max)
{
    m_anisotropyEnabled = enabled;
    m_anisotropyMax = max;
}

void Sampler::setCompareOp(bool enabled, VkCompareOp op)
{
    m_compareOpEnabled = enabled;
    m_compareOp = op;
}

void Sampler::setUnnormalizedCoordinates(bool value)
{
    m_unnormalizedCoordinates = value;
}

const VkSampler& Sampler::getSampler() const
{
    return m_sampler;
}

bool Sampler::initialize()
{
    if(m_sampler != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object.", __FILE__, __LINE__);
        assert(false);
        return false;
    }

	VkResult error;

    VkSamplerCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    createInfo.magFilter = m_filterMin;
    createInfo.minFilter = m_filterMag;
    createInfo.mipmapMode = m_mipmapMode;
    createInfo.addressModeU = m_addressModeU;
    createInfo.addressModeV = m_addressModeV;
    createInfo.addressModeW = m_addressModeW;
    createInfo.mipLodBias = m_mipLodBias;
    createInfo.anisotropyEnable = m_anisotropyEnabled ? VK_TRUE : VK_FALSE;
    createInfo.maxAnisotropy = m_anisotropyMax;
    createInfo.compareEnable = m_compareOpEnabled ? VK_TRUE : VK_FALSE;
    createInfo.compareOp = m_compareOp;
    createInfo.minLod = m_mipLodMin;
    createInfo.maxLod = m_mipLodMax;
    createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    createInfo.unnormalizedCoordinates = m_unnormalizedCoordinates ? VK_TRUE : VK_FALSE;

    error = vkCreateSampler(m_context.getDevice(), &createInfo, NO_ALLOCATOR, &m_sampler);
    if (error != VK_SUCCESS) 
    {
        spdlog::error("{0}:{1} Failed vkCreateSampler '{2}'.", __FILE__, __LINE__, toString(error));
        assert(false);
        return false;
    }
    return true;
}

void Sampler::destroy()
{
	if (m_sampler != VK_NULL_HANDLE)
	{
		vkDestroySampler(m_context.getDevice(), m_sampler, NO_ALLOCATOR);
		m_sampler = VK_NULL_HANDLE;
	}
}

void Sampler::incrementReferenceCount()
{
    ++m_refCount;
}

void Sampler::decrementReferenceCount()
{
    --m_refCount;
    if (m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
