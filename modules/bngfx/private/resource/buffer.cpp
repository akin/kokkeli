#include <bngfx/resources/buffer.h>
#include <bngfx/resourceblock.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
Buffer::Buffer(ResourceBlock& block)
: m_block(block)
{
}

Buffer::~Buffer()
{
    destroy();
}

void Buffer::setByteSize(size_t size)
{
    m_byteSize = size;
}

void Buffer::setUsage(VkBufferUsageFlags value)
{
    m_usage = value;
}

size_t Buffer::getAllocatedByteSize() const
{
    return m_allocatedByteSize;
}

size_t Buffer::getAllocatedByteOffset() const
{
    return m_allocatedByteOffset;
}

size_t Buffer::getByteSize() const
{
    return m_byteSize;
}

uint32_t Buffer::getBufferOffset() const
{
    // TODO for the future,
    // this should be replaced with bufferView concept.
    return 0;
}

bool Buffer::map(void*& target, size_t offset, size_t size)
{
    auto& context = m_block.getContext();
    VkResult error;

    VkMemoryMapFlags flags = 0;
    error = vkMapMemory(context.getDevice(), m_block.getMemory(), m_allocatedByteOffset + offset, size, flags, &target);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkMapMemory ('{3}') '{2}'.", __FILE__, __LINE__, toString(error), getName());
        assert(false);
        return false;
    }

    return true;
}

void Buffer::unmap()
{
    auto& context = m_block.getContext();
    vkUnmapMemory(context.getDevice(), m_block.getMemory());
}

bool Buffer::setData(const void* data, size_t offset, size_t size)
{
    auto& context = m_block.getContext();
    VkResult error;
    
    void* target;
    VkMemoryMapFlags flags = 0;
    error = vkMapMemory(context.getDevice(), m_block.getMemory(), m_allocatedByteOffset + offset, size, flags, &target);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkMapMemory ('{3}') '{2}'.", __FILE__, __LINE__, toString(error), getName());
        assert(false);
        return false;
    }

    memcpy(target, data, size);
    vkUnmapMemory(context.getDevice(), m_block.getMemory());

    return true;
}

void Buffer::setData(const Buffer* src, VkCommandBuffer& commandBuffer, size_t srcOffset, size_t dstOffset, size_t dataSize)
{
    auto& context = m_block.getContext();
    
    // Copy buffer happens at VkBuffer level, not VkMemory level.
    VkBufferCopy bufferCopy = {};
    bufferCopy.srcOffset = srcOffset;
    bufferCopy.dstOffset = dstOffset;
    bufferCopy.size = dataSize;
    vkCmdCopyBuffer(commandBuffer, src->getBuffer(), getBuffer(), 1, &bufferCopy);
}
    
const VkBuffer& Buffer::getBuffer() const
{
    return m_buffer;
}

void Buffer::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Buffer::getName() const
{
    return m_name;
}

void Buffer::incrementReferenceCount()
{
}

void Buffer::decrementReferenceCount()
{
}

bool Buffer::initialize()
{
    auto& context = m_block.getContext();
    if (m_buffer != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object ('{2}').", __FILE__, __LINE__, getName());
        assert(false);
        return false;
    }

    VkResult error;

    VkBufferCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    createInfo.size = m_byteSize;
    createInfo.usage = m_usage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    error = vkCreateBuffer(context.getDevice(), &createInfo, NO_ALLOCATOR, &m_buffer);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkCreateBuffer ('{3}') '{2}'.", __FILE__, __LINE__, toString(error), getName());
        assert(false);
        return false;
    }

    return true;
}

bool Buffer::postInitialize()
{
    return true;
}

void Buffer::destroy()
{
    auto& context = m_block.getContext();
    if (m_buffer != VK_NULL_HANDLE)
    {
        vkDestroyBuffer(context.getDevice(), m_buffer, NO_ALLOCATOR);
        m_buffer = VK_NULL_HANDLE;
    }
}

void Buffer::setAllocatedByteSize(size_t size)
{
    m_allocatedByteSize = size;
}
    
void Buffer::setAllocatedByteOffset(size_t offset)
{
    m_allocatedByteOffset = offset;
}
} // ns bngfx
