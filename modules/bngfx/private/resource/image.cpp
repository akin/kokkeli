#include <bngfx/resource/image.h>
#include <bngfx/resource/buffer.h>
#include <bngfx/resource/imageview.h>
#include <bngfx/resource/resourceblock.h>
#include <bngfx/context.h>
#include <spdlog/spdlog.h>
#include "../utils.h"

namespace bngfx
{
namespace {
VkImageViewType imageTypeToViewType(VkImageType type)
{
    switch(type)
    {
        case VK_IMAGE_TYPE_1D:
            return VK_IMAGE_VIEW_TYPE_1D;
        case VK_IMAGE_TYPE_2D:
            return VK_IMAGE_VIEW_TYPE_2D;
        case VK_IMAGE_TYPE_3D:
            return VK_IMAGE_VIEW_TYPE_3D;
        default:
            break;
    }
    assert(false);
    return VK_IMAGE_VIEW_TYPE_2D;
}
}

Image::Image(ResourceBlock& block)
: m_block(block)
{
}

Image::~Image()
{
    destroy();
}

void Image::setup(const bntypes::ImageInfo& info)
{
    m_type = imageTypeToVkImageType(info.type);
    m_dimensions.x = info.width;
    m_dimensions.y = info.height;
    m_depth = info.depth;
    m_mipCount = info.mipCount;
    m_arrayCount = info.arrayCount;
    m_format = dataFormatToVkFormat(info.format);
}

void Image::setType(VkImageType value)
{
    m_type = value;
}

void Image::setFormat(VkFormat value)
{
    m_format = value;
}

void Image::setSize(uvec2 value)
{
    m_dimensions = value;
}

void Image::setDepth(uint32_t value)
{
    m_depth = value;
}

void Image::setMipCount(uint32_t value)
{
    m_mipCount = value;
}

void Image::setArrayCount(uint32_t value)
{
    m_arrayCount = value;
}

void Image::setUsage(VkImageUsageFlags value)
{
    m_usage = value;
}

void Image::setAspectMask(VkImageAspectFlags value)
{
    m_aspectFlags = value;
}

void Image::setData(const Buffer* src, VkCommandBuffer& commandBuffer, uint32_t width, uint32_t height, uint32_t depth, uint32_t mipLevel, uint32_t arrayLevel, uint32_t arrayCount)
{
    VkBufferImageCopy bufferImageCopy = {};
    bufferImageCopy.bufferOffset = 0;
    bufferImageCopy.bufferRowLength = 0;
    bufferImageCopy.bufferImageHeight = 0;

    bufferImageCopy.imageSubresource.aspectMask = m_aspectFlags;
    bufferImageCopy.imageSubresource.mipLevel = mipLevel;
    bufferImageCopy.imageSubresource.baseArrayLayer = arrayLevel;
    bufferImageCopy.imageSubresource.layerCount = arrayCount;

    bufferImageCopy.imageOffset = { 0, 0, 0 };
    bufferImageCopy.imageExtent = {
        width,
        height,
        depth
    };

    vkCmdCopyBufferToImage(
        commandBuffer,
        src->getBuffer(),
        m_image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &bufferImageCopy
    );
}

uint32_t Image::getMipCount() const
{
    return m_mipCount;
}

uint32_t Image::getArrayCount() const
{
    return m_arrayCount;
}

size_t Image::getAllocatedByteSize() const
{
    return m_allocatedByteSize;
}

size_t Image::getAllocatedByteOffset() const
{
    return m_allocatedByteOffset;
}

VkFormat Image::getFormat() const
{
    return m_format;
}

const VkImage& Image::getImage() const
{
    return m_image;
}

const IntrusivePtr<ImageView>& Image::getView()
{
    return m_view;
}

void Image::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Image::getName() const
{
    return m_name;
}

void Image::incrementReferenceCount()
{
}

void Image::decrementReferenceCount()
{
}

bool Image::initialize()
{
    auto& context = m_block.getContext();
    if (m_image != VK_NULL_HANDLE)
    {
        spdlog::error("{0}:{1} Trying to initialize already initialized object ('{2}').", __FILE__, __LINE__, getName());
        assert(false);
        return false;
    }

    VkResult error;

    // VkFormatFeatureFlagBits check for support..
    // VkFormatFeatureFlags flags = context.getFormatFlags(m_format);

    VkImageCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    createInfo.flags = 0; // TODO? Cubemaps?
    createInfo.imageType = m_type;
    createInfo.extent.width = static_cast<uint32_t>(m_dimensions.x);
    createInfo.extent.height = static_cast<uint32_t>(m_dimensions.y);
    createInfo.extent.depth = m_depth;
    createInfo.mipLevels = m_mipCount;
    createInfo.arrayLayers = m_arrayCount;
    createInfo.format = m_format;
    createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    createInfo.usage = m_usage;
    createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    error = vkCreateImage(context.getDevice(), &createInfo, NO_ALLOCATOR, &m_image);
    if (error != VK_SUCCESS)
    {
        spdlog::error("{0}:{1} Failed vkCreateImage ('{3}') '{2}'.", __FILE__, __LINE__, toString(error), getName());
        assert(false);
        return false;
    }

    return true;
}

bool Image::postInitialize()
{
    auto& context = m_block.getContext();
    m_view = new ImageView(context);
    m_view->setAspectMask(m_aspectFlags);
    if (!m_view->initialize(m_image, imageTypeToViewType(m_type),  m_format))
    {
        spdlog::error("{0}:{1} Failed to initialize default imageview ('{2}').", __FILE__, __LINE__, getName());
        assert(false);
        return false;
    }
    return true;
}

void Image::destroy()
{
    auto& context = m_block.getContext();
    delete m_view;
    m_view = nullptr;

    if (m_image != VK_NULL_HANDLE)
    {
        vkDestroyImage(context.getDevice(), m_image, NO_ALLOCATOR);
        m_image = VK_NULL_HANDLE;
    }
}

void Image::setAllocatedByteSize(size_t size)
{
    m_allocatedByteSize = size;
}
    
void Image::setAllocatedByteOffset(size_t offset)
{
    m_allocatedByteOffset = offset;
}
} // ns bngfx
