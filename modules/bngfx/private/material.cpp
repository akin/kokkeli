#include <bngfx/material.h>

#include <spdlog/spdlog.h>
#include "utils.h"

namespace bngfx
{

const std::vector<MaterialLayer>& Material::getLayers() const
{
    return m_layers;
}

bool Material::initialize(size_t layerCount)
{
    m_layers.resize(layerCount);
    return true;
}

void Material::incrementReferenceCount()
{
    ++m_refCount;
}

void Material::decrementReferenceCount()
{
    --m_refCount;
    if(m_refCount == 0)
    {
        delete this;
    }
}
} // ns bngfx
