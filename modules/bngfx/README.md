
# rendering engine "abstraction"

![](bona.png)

## TODO
 *  everything

 ## Structure
  * public: contains public interface of the module, this means everything inside the public folder is seen from the outside. 
  * private: contains private implementation details, that should not leak outside of the module.
  * dev: development executable for all the module development needs. there are synergies to use dev & test as one, but if testing interferes with development (so if test framework does something that causes debugging to be impossible, that is a major problem). 
  * test: tests for the module.
  * resources: resources for the module.
  