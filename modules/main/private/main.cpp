#include <iostream>

#include <fmt/format.h>
#include "testbed .h"

int main(int argc, char *argv[])
{
	TestBed test;

	test.setName("Kokkeli");
	test.setVersion(0, 0, 1);

	test.run([](const std::string& info, float at) {
		std::cout << fmt::format("total {1} %, {0}", info, at * 100.0f) << std::endl;
	});

	test.save("results.json");

	return 0;
}
