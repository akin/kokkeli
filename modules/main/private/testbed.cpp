#include "testbed .h"
#include <fmt/format.h>
#include <fstream>
#include <cache_coherency/test.h>
#include <memcopy/test.h>
#include <thread>
#include <core/random.h>

namespace
{
	void runCacheTest(size_t itemCount, size_t iterationCount, nlohmann::json& root, const std::function<void(float at)>& callback)
	{
		auto array = nlohmann::json::array();

		auto runTestAndCreateResult_arrayOfStructs = [&array, itemCount, iterationCount](size_t threadCount, bool affinity)
		{
			auto results = CacheCoherecy::run_ArrayOfStructs(itemCount, iterationCount, threadCount, affinity, [](CacheCoherecy::ArrayOfStructs& data) {
				// Data reset
				Core::Random random(0, 100);
				Core::Random sclrnd(-1, 2);

				for (auto& transform : data)
				{
					transform.location = glm::vec3(random.get(), random.get(), random.get());
					transform.scale = glm::vec3(sclrnd.get(), sclrnd.get(), sclrnd.get());
					transform.rotation = glm::quat(random.get(), random.get(), random.get(), random.get());
					transform.dirty = true;
				}
			});

			auto item = nlohmann::json({});
			auto config = nlohmann::json({});

			config["affinity"] = affinity;
			config["thread count"] = threadCount;
			config["item count"] = itemCount;
			config["iteration count"] = iterationCount;
			config["max supported threads"] = std::thread::hardware_concurrency();
			config["type"] = "Array Of Structs";
			item["config"] = config;

			auto jsonresultarray = nlohmann::json::array();
			for (size_t i = 0; i < results.size(); ++i)
			{
				auto item = nlohmann::json({});
				item["thread"] = i;
				item["result"] = results[i];
				jsonresultarray.push_back(item);
			}
			item["result"] = jsonresultarray;
			array.push_back(item);
		};
		auto runTestAndCreateResult_structOfArrays = [&array, itemCount, iterationCount](size_t threadCount, bool affinity)
		{
			auto results = CacheCoherecy::run_StructOfArrays(itemCount, iterationCount, threadCount, affinity, [](CacheCoherecy::StructOfArrays& data) {
				// Data reset
				size_t count = data.entityId.size();
				Core::Random random(0, 100);
				Core::Random sclrnd(-1, 2);

				for (size_t i = 0; i < count; ++i)
				{
					setLocation(data, i, glm::vec3(random.get(), random.get(), random.get()));
					setScale(data, i, glm::vec3(sclrnd.get(), sclrnd.get(), sclrnd.get()));
					setRotation(data, i, glm::quat(random.get(), random.get(), random.get(), random.get()));
				}
			});

			auto item = nlohmann::json({});
			auto config = nlohmann::json({});

			config["affinity"] = affinity;
			config["thread count"] = threadCount;
			config["item count"] = itemCount;
			config["iteration count"] = iterationCount;
			config["max supported threads"] = std::thread::hardware_concurrency();
			config["type"] = "Struct Of Arrays";
			item["config"] = config;

			auto jsonresultarray = nlohmann::json::array();
			for (size_t i = 0; i < results.size(); ++i)
			{
				auto item = nlohmann::json({});
				item["thread"] = i;
				item["result"] = results[i];
				jsonresultarray.push_back(item);
			}
			item["result"] = jsonresultarray;
			array.push_back(item);
		};

		callback(0.0f);
		size_t totalthreads = std::thread::hardware_concurrency() * 2;
		if (totalthreads == 0)
		{
			totalthreads = 1;
		}
		for (size_t tr = 1; tr <= totalthreads; ++tr)
		{
			runTestAndCreateResult_arrayOfStructs(tr, true);
			runTestAndCreateResult_arrayOfStructs(tr, false);

			runTestAndCreateResult_structOfArrays(tr, true);
			runTestAndCreateResult_structOfArrays(tr, false);
			callback(tr / (float)totalthreads);
		}

		root["cache"] = array;
	}

	void runMemcopyTest(size_t itemSize, size_t itemCount, size_t iterationCount, nlohmann::json& root, const std::function<void(float at)>& callback)
	{
		auto array = nlohmann::json::array();

		auto runTestAndCreateResult_arrayOfStructs = [&array, itemSize, itemCount, iterationCount](size_t threadCount, bool affinity)
		{
			MemCopy::Test test;

			test.setItemSize(itemSize);
			test.setItemCount(itemCount);
			test.setIterationCount(iterationCount);
			test.setThreadCount(threadCount);
			test.setAffinity(affinity);

			bool success = test.run(
			// generate data
			[](uint8_t* data, size_t total) {
				for (size_t i = 0; i < total; ++i)
				{
					data[i] = i % 0xFF;
				}
			}, 
			// verify data
			[](uint8_t* data, size_t total) -> bool {
				for (size_t i = 0; i < total; ++i)
				{
					if (data[i] != i % 0xFF)
					{
						return false;
					}
				}
				return true;
			});

			auto item = nlohmann::json({});
			auto config = nlohmann::json({});

			config["affinity"] = affinity;
			config["thread count"] = threadCount;
			config["item size(bytes)"] = itemSize;
			config["item count"] = itemCount;
			config["iteration count"] = iterationCount;
			config["max supported threads"] = std::thread::hardware_concurrency();
			config["success"] = success;
			item["config"] = config;
			auto jsonresultarray = nlohmann::json::array();
			if(success)
			{
				const auto& results = test.getResult();
				for (size_t i = 0; i < results.size(); ++i)
				{
					auto item = nlohmann::json({});
					item["thread"] = i;
					item["result"] = results[i];
					jsonresultarray.push_back(item);
				}
			}
			item["result"] = jsonresultarray;
			array.push_back(item);
		};

		callback(0.0f);
		size_t totalthreads = std::thread::hardware_concurrency() * 2;
		if (totalthreads == 0)
		{
			totalthreads = 1;
		}
		for (size_t tr = 1; tr <= totalthreads; ++tr)
		{
			runTestAndCreateResult_arrayOfStructs(tr, true);
			runTestAndCreateResult_arrayOfStructs(tr, false);

			callback(tr / (float)totalthreads);
		}

		root["memcopy"] = array;
	}
}

void TestBed::run(const std::function<void(const std::string& info, float at)>& callback)
{
	float offset = 0.0f;
	float weight = 1.0f / 2;
	runCacheTest(1000, m_iterationCount, m_root, [callback, &offset, weight](float at) 
	{
		callback("Cache test", offset + at * weight);
	});
	offset = 0.5f;
	runMemcopyTest(1024, 1000, m_iterationCount, m_root, [callback, &offset, weight](float at)
	{
		callback("MemCopy test", offset + at * weight);
	});
}

void TestBed::setName(const std::string& name)
{
	m_name = name;
}

void TestBed::setVersion(int major, int minor, int patch)
{
	m_major = major;
	m_minor = minor;
	m_patch = patch;
}

void TestBed::setIterationCount(size_t count)
{
	m_iterationCount = count;
}

bool TestBed::TestBed::save(const std::string& filename)
{
	auto config = nlohmann::json({});

	config["name"] = m_name;
	config["version"] = fmt::format("{0}.{1}.{2}", m_major, m_minor, m_patch);
	config["hash"] = GIT_HASH;
	config["builder"] = BUILD_SYSTEM_NAME;
	config["builder_version"] = BUILD_SYSTEM_VERSION;
	config["builder_hostname"] = BUILD_SYSTEM_HOSTNAME;

	m_root["configuration"] = config;

	std::ofstream out(filename);
	out << m_root << std::endl;

	out.close();

	return true;
}
