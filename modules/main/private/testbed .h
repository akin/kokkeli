
#ifndef MAIN_TESTBED_H_
#define MAIN_TESTBED_H_

#include <string>
#include <nlohmann/json.hpp>
#include <functional>

class TestBed
{
private:
	size_t m_iterationCount = 20;
	std::string m_name = "test";
	int m_major = 0;
	int m_minor = 0;
	int m_patch = 0;
	nlohmann::json m_root;
public:
	void run(const std::function<void(const std::string& info, float at)>& callback);

	void setName(const std::string& name);
	void setVersion(int major, int minor, int patch);
	void setIterationCount(size_t count);

	bool save(const std::string& filename);
};

#endif // MAIN_TESTBED_H_
