#include "generator.h"

using namespace toy;

int main(void)
{
    MazeGenerator generator{{50, 50}};

    Maze maze = generator.createMaze({10,10}, {40,40}, 3, 5, 3);

    return 0;
}