#ifndef TOY_COMMON_H_
#define TOY_COMMON_H_

#include <cmath>
#include <vector>

namespace toy
{
struct vec2i
{
    size_t x = 0;
    size_t y = 0;

    bool operator !=(const vec2i& other) const 
    {
        return !((x == other.x) && (y == other.y));
    }
};

void getLinePoints(vec2i start, vec2i end, std::vector<vec2i>& points)
{
    // http://www.cse.yorku.ca/~amana/research/grid.pdf
    vec2i diff{end.x - start.x , end.y - start.y};

    vec2i current = start;
    vec2i step{ diff.x < 0 ? -1 : 1, diff.y < 0 ? -1 : 1 };

    float angle = std::atan2((float)-diff.x, (float)diff.y);

    // how much we need to move, to cross grid
    float tDeltaX = 1.0f / std::cos(angle);
    float tDeltaY = 1.0f / std::sin(angle);

    // where are we?
    float tMaxX = 0.0f;
    float tMaxY = 0.0f;
    while(current != end)
    {
        if (tMaxX < tMaxY) 
        {
            tMaxX += tDeltaX;
            current.x += step.x;
        } 
        else 
        {
            tMaxY += tDeltaY;
            current.y += step.y;
        }
        points.push_back(current);
    }
}

void getPathPoints(const std::vector<vec2i>& path, std::vector<vec2i>& points)
{
    if(path.empty())
    {
        return;
    }
    if(path.size() == 1)
    {
        points.push_back(path.front());
        return;
    }
    for(size_t i = 1 ; i < path.size() ; ++i)
    {
        getLinePoints(path[i - 1], path[i], points);
    }
}

} // ns toy

#endif // TOY_COMMON_H_