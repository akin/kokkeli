#ifndef TOY_TILE_H_
#define TOY_TILE_H_

#include <cstdint>

namespace toy
{

class Tile
{
public:
    Tile() = default;
    
    Tile(uint32_t type)
    : m_type(type)
    {
    }

    Tile(const Tile& other)
    : m_type(other.m_type)
    {
    }

    uint32_t getType() const
    {
        return m_type;
    }

    void setType(uint32_t type) 
    {
        m_type = type;
    }
private:
    uint32_t m_type = 0;
};

} // ns toy

#endif // TOY_TILE_H_