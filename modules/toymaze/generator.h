#ifndef TOY_GENERATOR_H_
#define TOY_GENERATOR_H_

#include <string>
#include <vector>
#include <unordered_map>
#include <random>
#include "common.h"
#include "maze.h"
#include "tile.h"

namespace toy
{
class MazeGenerator
{
public:
    MazeGenerator(vec2i size)
    : m_size(size)
    , m_xDist(1, size.x - 2)
    , m_yDist(1, size.y - 2)
    {
    }
        
    Maze createMaze(vec2i start, vec2i end, size_t pathComplexity, size_t falsePaths, size_t falsePathComplexity)
    {
        Maze maze{m_size};

        maze.setPoint("start", start);
        maze.setPoint("end", end);

        Tile empty(0);
        Tile wall(0xFF);

        maze.clear(wall);

        std::vector<vec2i> pathPoints;
        // Create true path
        {
            std::vector<vec2i> path;
            path.push_back(start);
            for(size_t i = 0 ; i < pathComplexity ; ++i)
            {
                path.push_back(randomPoint());
            }
            path.push_back(end);
            getPathPoints(path, pathPoints);
        }
        maze.set(pathPoints, empty);

        // Create false paths from true path
        {
            std::uniform_int_distribution<size_t> extraPathsRandom(1, falsePaths);
            size_t extraPaths = extraPathsRandom(m_generator);
            
            std::uniform_int_distribution<size_t> startPointsRandom(0, pathPoints.size());
            std::uniform_int_distribution<size_t> complexityRandom(0, falsePathComplexity);
            for(size_t i = 0 ; i < extraPaths ; ++i)
            {
                size_t complexity = complexityRandom(m_generator);

                std::vector<vec2i> path;
                path.push_back(pathPoints[startPointsRandom(m_generator)]);
                for(size_t i = 0 ; i < complexity ; ++i)
                {
                    path.push_back(randomPoint());
                }
                std::vector<vec2i> points;
                getPathPoints(path, points);
                maze.set(pathPoints, empty);
            }
        }

        return std::move(maze);
    }
private:

    vec2i randomPoint()
    {
        vec2i point;
        point.x = m_xDist(m_generator);
        point.y = m_yDist(m_generator);
        return std::move(point);
    }

    vec2i m_size;
    std::default_random_engine m_generator;
    std::uniform_int_distribution<size_t> m_xDist;
    std::uniform_int_distribution<size_t> m_yDist;
};

} // ns toy

#endif // TOY_MAZE_H_