#ifndef TOY_MAZE_H_
#define TOY_MAZE_H_

#include <string>
#include <vector>
#include <unordered_map>
#include "common.h"
#include "tile.h"

namespace toy
{
class Maze
{
public:
    Maze(vec2i size)
    : m_size(size)
	{
        m_tiles.resize(m_size.x * m_size.y);
	}

    void setPoint(const std::string& key, vec2i point)
    {
        m_points[key] = point;
    }

    bool getPoint(const std::string& key, vec2i& point)
    {
        auto iter = m_points.find(key);
        if(iter != m_points.end())
        {
            point = iter->second;
            return true;
        }
        return false;
    }

    Tile& get(const vec2i& pos)
    {
        return m_tiles[pos.y * m_size.x + pos.x];
    }

    const Tile& get(const vec2i& pos) const
    {
        return m_tiles[pos.y * m_size.x + pos.x];
    }

    const vec2i& getSize() const
    {
        return m_size;
    }

    void clear(const Tile& tile)
    {
        for(Tile& target : m_tiles)
        {
            target = tile;
        }
    }

    void set(const vec2i& pos, const Tile& tile)
    {
        m_tiles[pos.y * m_size.x + pos.x] = tile;
    }

    void set(const std::vector<vec2i>& positions, const Tile& tile)
    {
        for(const vec2i& pos : positions)
        {
            m_tiles[pos.y * m_size.x + pos.x] = tile;
        }
    }
private:
    vec2i m_size;

    std::unordered_map<std::string, vec2i> m_points;

    std::vector<Tile> m_tiles;
};
} // ns toy

#endif // TOY_MAZE_H_