
#ifndef CORE_RANDOM_H_
#define CORE_RANDOM_H_

#include <random>

namespace Core 
{
	class Random
	{
	private:
		std::random_device device;
		std::mt19937 engine;
		std::uniform_real_distribution<double> distribution;
	public:
		Random(double min, double max)
		: engine(device())
		, distribution(min, max)
		{
		}

		float get()
		{
			return (float)distribution(engine);
		}
	};
} // ns TestBed

#endif // CORE_RANDOM_H_
