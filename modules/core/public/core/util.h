
#ifndef CORE_UTIL_H_
#define CORE_UTIL_H_

#include <string>
#include <thread>

namespace Core 
{
    bool setAffinity(std::thread& thread, size_t core);
    std::string toString(double number);
} // ns Core

#endif // CORE_UTIL_H_
