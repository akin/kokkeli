
#ifndef CORE_COMMON_H_
#define CORE_COMMON_H_

#include <vector>
#include <memory>
#include <chrono>
#include <string>

namespace Core 
{
    using Float = float;
    using Time = float;

	// Times in nanoseconds
    using Times = std::vector<size_t>;
    using Clock = std::chrono::high_resolution_clock;
	using TimeUnit = std::chrono::nanoseconds;

	std::string getTimeUnit();
} // ns Core

#endif // CORE_COMMON_H_
