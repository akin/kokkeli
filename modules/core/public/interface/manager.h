
#ifndef CORE_MANAGER_H_
#define CORE_MANAGER_H_

namespace Interface {

class Manager 
{
public:
    virtual ~Manager() {}

    virtual void run() = 0;
    virtual void close() = 0;
};

} // ns Interface

#endif // CORE_MANAGER_H_