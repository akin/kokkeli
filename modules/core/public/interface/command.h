
#ifndef CORE_COMMAND_H_
#define CORE_COMMAND_H_

namespace Interface {

class Command {
public:
    virtual ~Command() {}

    virtual void execute() = 0;
    virtual void undo() = 0;
};

} // ns Interface

#endif // CORE_COMMAND_H_
