
#ifndef CORE_APPLICATION_H_
#define CORE_APPLICATION_H_

#include <string>

namespace Interface {

class Application
{
private:
    std::string m_name;
public:
    Application(const std::string& name = "");
    virtual ~Application() = 0;

    void parseArgs(int argc, char *argv[]);

    virtual bool init() = 0;

    virtual int run() = 0;
};

} // ns Interface

#endif // CORE_APPLICATION_H_