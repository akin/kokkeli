
#ifndef CORE_TASK_H_
#define CORE_TASK_H_

#include <string>

namespace Interface {

class Task {
public:
    virtual ~Task() {}

    virtual void run() = 0;
};

} // ns Interface

#endif // CORE_TASK_H_
