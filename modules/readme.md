
# Modules
 * project specific modules folder.

## bncommon
common tools for modules, things like common timestamp, clock, random, intrusivepointer etc.
some legacy?

## bndata
file loading abstractions, provides interfaces and implementations for loading files off the disk or elsewhere. provides interface for exact types, eg. image, mesh etc. not just a "stream of bytes".

## bngfx
graphics abstraction, abstracts vulkan behind some "sane" layers.

## bnray
raytracing build on top of bngfx.

## bnscene
scene presentation to build on top of kave rendering.

## bntypes
definitions for formats and numbers, defines concrete numbers for types.

## bnwindow
window abstraction.

## cache coherency
cache coherency testing with matrix calculations.

## core
core some legacy.

## dev
development playground. legacy, nowdays, all playing happens under the "test" projects.

## main
main module project?
legacy.

## memcopy
memcopy speed testing.
legacy.

## mop
flatbuffers based 3D mesh format. intended usecase would be in kave project, as the source format for pbr data resource.

## nn
neural networks sketching.
