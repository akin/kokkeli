#ifndef BONA_SHADERTOOLS_LOADER_H_
#define BONA_SHADERTOOLS_LOADER_H_

#include <bncommon/intrusiveptr.h>
#include <bngfx/shader/shadermanager.h>

namespace bnshadertools
{
bool load(const char* path, bngfx::Context& context, bngfx::ShaderManager& manager);
} // ns bnshadertools

#endif // BONA_SHADERTOOLS_LOADER_H_