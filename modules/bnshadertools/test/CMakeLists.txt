cmake_minimum_required(VERSION 3.11)
option(TESTS_bona_shadertools "Add Bona shadertools Tests" OFF)

if(NOT TESTS_bona_shadertools)
	return()
endif()

project(bnshadertools-tests C CXX)
add_executable(${PROJECT_NAME})

file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/*.h 
	${CMAKE_CURRENT_LIST_DIR}/*.cpp 
)
target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_SOURCES}
	)

find_package(Catch2 CONFIG REQUIRED)
target_link_libraries(
	${PROJECT_NAME}
	PRIVATE
		Catch2::Catch2
		fmt::fmt-header-only
		bnshadertools
)
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "tests")
