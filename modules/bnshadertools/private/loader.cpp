#include <bnshadertools/loader.h>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <bngfx/common.h>
#include <filesystem>
#include <fstream>

namespace bnshadertools
{
namespace {
bngfx::ShaderType stringToShaderType(const std::string& str)
{
    // defined in https://github.com/KhronosGroup/SPIRV-Cross/blob/master/spirv_reflect.cpp
    static const std::unordered_map<std::string, bngfx::ShaderType> conversion{
        {"vert", bngfx::ShaderType::vertex},
        {"frag", bngfx::ShaderType::fragment},
        {"comp", bngfx::ShaderType::compute},
        {"tesc", bngfx::ShaderType::tesselationControl},
        {"tese", bngfx::ShaderType::tesselationEvaluation},
        {"geom", bngfx::ShaderType::geometry},
        {"mesh", bngfx::ShaderType::mesh},
        {"rgen", bngfx::ShaderType::rayGeneration},
        {"rint", bngfx::ShaderType::rayIntersection},
        {"rahit", bngfx::ShaderType::rayAnyHit},
        {"rchit", bngfx::ShaderType::rayClosestHit},
        {"rmiss", bngfx::ShaderType::rayMiss},
        {"rcall", bngfx::ShaderType::rayCallable},
    };
    for (auto iter : conversion)
    {
        if (iter.first == str)
        {
            return iter.second;
        }
    }
    return bngfx::ShaderType::none;
}

bool parseString(nlohmann::json json, std::string& target)
{
    if (!json.is_string())
    {
        return false;
    }
    target = json.get<std::string>();
}

bool parseUint32(nlohmann::json json, uint32_t& target)
{
    if (!json.is_number_unsigned())
    {
        return false;
    }
    target = json.get<uint32_t>();
}

struct FileInfo
{
    std::string path;
    std::string filename;
    std::string folderpath;
};
}

bool readFile(const std::filesystem::path& path, std::vector<char>& data)
{
    if (!std::filesystem::exists(path))
    {
        return false;
    }
    size_t size = std::filesystem::file_size(path);
    data.resize(size);

    std::ifstream file(path, std::ios::binary | std::ios::beg);

    // TODO, what am I reading here?
    // char's? earlier I had uint8_t, but iostream didn't want to read to that,
    // apparently because windows char can be wchar? 16 bit char?.. I want bytes, utf8 bytes.
    // but unfortunately windows has screwed us all with causing braindamage to c++ api.
    // For now I assume that this will work, until it does not, to be fixed later!
    file.read(data.data() , size);

    return file.good();
}

bool reflectShader(bngfx::IntrusivePtr<bngfx::Shader>& shader, const std::filesystem::path& shaderPath, bngfx::ShaderManager& manager)
{
    std::filesystem::path reflectionSourcePath = shaderPath;
    reflectionSourcePath += ".refl";
    bngfx::ShaderType shaderType = bngfx::ShaderType::none;

    std::vector<char> data;
    if (!readFile(reflectionSourcePath, data))
    {
        spdlog::info("Shader '{0}': failed to read reflection data.", shader->getName());
        return false;
    }
    nlohmann::json content = nlohmann::json::parse(data);

    // The parsing here is a bit "inverted logic" with the if-else structures,
    // to give the user, or the debug phase, more information about errors in one go, 
    // rather than iterative "one error at a time".
    bool success = true;
    // entryPoints
    {
#ifdef EXAMPLE_DATA
    "entryPoints" : [
        {
            "name" : "main",
            "mode" : "vert"
        }
    ],
#endif
        auto items = content["entryPoints"];
        if (items.empty() || (!items.is_array()))
        {
            spdlog::info("Shader '{0}': reflection is missing entryPoint.", shader->getName());
            success = false;
        }
        else if (items.size() > 1)
        {
            // I am for now restricting the shader source to contain only _1_ shader type & entrypoint.
            spdlog::info("Shader '{0}': reflection contains more than 1 entryPoint.", shader->getName());
            success = false;
        }
        else
        {
            nlohmann::json entrypoint = items.at(0);
            if (!entrypoint.is_object())
            {
                spdlog::info("Shader '{0}': reflection contains invalid entryPoint.", shader->getName());
                success = false;
            }
            else
            {
                std::string name;
                std::string mode;

                if (!parseString(entrypoint["name"], name))
                {
                    spdlog::info("Shader '{0}': reflection contains invalid entryPoint name.", shader->getName());
                    success = false;
                }
                if (!parseString(entrypoint["mode"], mode))
                {
                    spdlog::info("Shader '{0}': reflection contains invalid entryPoint mode.", shader->getName());
                    success = false;
                }
                if (success)
                {
                    shaderType = stringToShaderType(mode);
                    if (shaderType == bngfx::ShaderType::none)
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid entryPoint mode.", shader->getName());
                        return false;
                    }
                }

                if(success)
                {
                    shader->setEntryPoint(name.c_str());
                    shader->setType(shaderType);
                }
            }
        }
    }
    // types
    {
#ifdef EXAMPLE_DATA
    "types" : {
        "_11" : {
            "name" : "gl_PerVertex",
            "members" : [
                {
                    "name" : "gl_Position",
                    "type" : "vec4"
                },
                {
                    "name" : "gl_PointSize",
                    "type" : "float"
                },
                {
                    "name" : "gl_ClipDistance",
                    "type" : "float",
                    "array" : [
                        1
                    ]
                },
                {
                    "name" : "gl_CullDistance",
                    "type" : "float",
                    "array" : [
                        1
                    ]
                }
            ]
        },
        "_17" : {
            "name" : "ModelViewProjection",
            "members" : [
                {
                    "name" : "model",
                    "type" : "mat4",
                    "offset" : 0
                },
                {
                    "name" : "view",
                    "type" : "mat4",
                    "offset" : 64
                },
                {
                    "name" : "projection",
                    "type" : "mat4",
                    "offset" : 128
                }
            ]
        },
        "_49" : {
            "name" : "dada",
            "members" : [
                {
                    "name" : "model",
                    "type" : "mat4",
                    "offset" : 0
                },
                {
                    "name" : "view",
                    "type" : "mat4",
                    "offset" : 64
                },
                {
                    "name" : "projection",
                    "type" : "mat4",
                    "offset" : 128
                }
            ]
        }
    },
#endif
        // TODO
    }
    // inputs
    {
#ifdef EXAMPLE_DATA
    "inputs" : [
        {
            "type" : "vec4",
            "name" : "position",
            "location" : 0
        },
        {
            "type" : "vec4",
            "name" : "inColor",
            "location" : 1
        },
        {
            "type" : "vec2",
            "name" : "inTexCoord",
            "location" : 2
        }
    ],
#endif
        auto items = content["inputs"];
        if (!items.empty() && items.is_array())
        {
            for (nlohmann::json& item : items)
            {
                if (!item.empty() && item.is_object())
                {
                    std::string format;
                    std::string name;
                    uint32_t location = {};

                    if (!parseString(item["type"], format))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid input type.", shader->getName());
                        success = false;
                    }
                    if (!parseString(item["name"], name))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid input name.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["location"], location))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid input location.", shader->getName());
                        success = false;
                    }
                    if (success)
                    {
                        AttributeData data{};
                        data.id = manager.getOrCreateAttributeIDForName(name);
                        data.location = location
                        data.format = manager.getOrCreateDataFormatForName(format);

                        shader->setInputAttribute(data);
                    }
                }
            }
        }
    }
    // outputs
    {
#ifdef EXAMPLE_DATA
    "outputs" : [
        {
            "type" : "vec3",
            "name" : "outColor",
            "location" : 0
        },
        {
            "type" : "vec2",
            "name" : "outTexCoord",
            "location" : 1
        }
    ],
#endif
        auto items = content["outputs"];
        if (!items.empty() && items.is_array())
        {
            for (nlohmann::json& item : items)
            {
                if (!item.empty() && item.is_object())
                {
                    std::string format;
                    std::string name;
                    uint32_t location = {};

                    if (!parseString(item["type"], format))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid output type.", shader->getName());
                        success = false;
                    }
                    if (!parseString(item["name"], name))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid output name.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["location"], location))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid output location.", shader->getName());
                        success = false;
                    }
                    if (success)
                    {
                        AttributeData data{};
                        data.id = manager.getOrCreateAttributeIDForName(name);
                        data.location = location
                        data.format = manager.getOrCreateDataFormatForName(format);

                        shader->setOutputAttribute(data);
                    }
                }
            }
        }
    }
    // uniforms
    {
#ifdef EXAMPLE_DATA
    "ubos" : [
        {
            "type" : "_17",
            "name" : "ModelViewProjection",
            "block_size" : 192,
            "set" : 0,
            "binding" : 0
        },
        {
            "type" : "_49",
            "name" : "dada",
            "block_size" : 192,
            "set" : 1,
            "binding" : 0
        }
    ]
#endif
        auto items = content["ubos"];
        if (!items.empty() && items.is_array())
        {
            for (nlohmann::json& item : items)
            {
                if (!item.empty() && item.is_object())
                {
                    std::string name;
                    std::string format;
                    uint32_t set = {};
                    uint32_t binding = {};
                    uint32_t blocksize = {};

                    if (!parseString(item["name"], name))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo name.", shader->getName());
                        success = false;
                    }
                    if (!parseString(item["type"], format))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo type.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["set"], set))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo set.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["binding"], binding))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo binding.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["block_size"], blocksize))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo block_size.", shader->getName());
                        success = false;
                    }
                    if (success)
                    {
                        bngfx::DescriptorData data{};
                        data.id = manager.getOrCreateDescriptorIDForName(name);
                        data.type = DescriptorType::buffer;
                        data.set = static_cast<bmgfx::DescriptorSetID>(set);
                        data.binding = binding;
                        data.count = 1;
                        data.format = manager.getOrCreateDataFormatForName(format);
                        data.blocksize = blocksize;
                        data.shader.set(shaderType);

                        shader->setDescriptor(desdata);
                    }
                }
            }
        }
    }
    // uniforms
    {
#ifdef EXAMPLE_DATA
    "textures" : [
        {
            "type" : "sampler2D",
            "name" : "inSampler",
            "set" : 0,
            "binding" : 1
        }
    ],
#endif
        auto items = content["textures"];
        if (!items.empty() && items.is_array())
        {
            for (nlohmann::json& item : items)
            {
                if (!item.empty() && item.is_object())
                {
                    std::string name;
                    std::string format;
                    uint32_t set = {};
                    uint32_t binding = {};
                    uint32_t blocksize = 0;

                    if (!parseString(item["name"], name))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo name.", shader->getName());
                        success = false;
                    }
                    if (!parseString(item["type"], format))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo type.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["set"], set))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo set.", shader->getName());
                        success = false;
                    }
                    if (!parseUint32(item["binding"], binding))
                    {
                        spdlog::info("Shader '{0}': reflection contains invalid ubo binding.", shader->getName());
                        success = false;
                    }
                    if (success)
                    {
                        bngfx::DescriptorData data{};
                        data.id = manager.getOrCreateDescriptorIDForName(name);
                        data.type = DescriptorType::image;
                        data.set = static_cast<bmgfx::DescriptorSetID>(set);
                        data.binding = binding;
                        data.count = 1;
                        data.format = manager.getOrCreateDataFormatForName(format);
                        data.blocksize = blocksize;
                        data.shader.set(shaderType);

                        shader->setDescriptor(data);
                    }
                }
            }
        }
    }

    return success;
}

bool loadShader(bngfx::IntrusivePtr<bngfx::Shader>& shader, const std::filesystem::path& shaderPath, bngfx::ShaderManager& manager)
{
    if (!reflectShader(shader, shaderPath, manager))
    {
        spdlog::info("Shader '{0}': failed to read reflection data.", shader->getName());
        return false;
    }

    // Load shader
    std::filesystem::path spirvSourcePath = shaderPath;
    spirvSourcePath += ".spv";
    std::vector<char> data;
    if (!readFile(spirvSourcePath, data))
    {
        spdlog::info("Shader '{0}': failed to read spirv data.", shader->getName());
        return false;
    }

    if (!shader->initialize(data.data(), data.size()))
    {
        spdlog::info("Shader '{0}' failed to inititalize.", shader->getName());
        return false;
    }

    return true;
}

bool loadRenderInfo(const std::filesystem::path& infoPath, bngfx::IntrusivePtr<bngfx::RenderInfo>& renderInfo, nlohmann::json& shaderSourceArray, bngfx::Context& context, bngfx::ShaderManager& manager)
{
    std::vector<bngfx::IntrusivePtr<bngfx::Shader>> shaders;
    shaders.reserve(shaderSourceArray.size());
    for (nlohmann::json& source : shaderSourceArray)
    {
        if (source.empty() || (!source.is_string()))
        {
            spdlog::info("RenderInfo '{0}': shader is missing a source.", renderInfo->getName());
            return false;
        }

        const std::filesystem::path shaderPath = infoPath.parent_path() / source.get<std::string>();
        std::string shaderName = source.get<std::string>();
        bngfx::IntrusivePtr<bngfx::Shader> shader = manager.getShader(shaderName);
        if(!shader)
        {
            shader = manager.createShader(shaderName);
            if (!loadShader(shader, shaderPath, manager))
            {
                spdlog::info("RenderInfo '{0}': shader '{1}' failed to load.", renderInfo->getName(), shader->getName());
                return false;
            }
        }
        shaders.push_back(shader);
    }
    return renderInfo->initialize(shaders);
}

bool load(const char* path, bngfx::Context& context, bngfx::ShaderManager& manager)
{
    std::vector<char> data;

    std::filesystem::path infoPath{ path };
    if (!readFile(infoPath, data))
    {
        return false;
    }

    bool totalResult = true;
    auto content = nlohmann::json::parse(data);
    {
        auto infos = content["RenderInfos"];
        if (infos.is_array())
        {
            for (nlohmann::json& info : infos)
            {
                bool result = true;
                auto name = info["name"];
                if (name.empty() || (!name.is_string()))
                {
                    spdlog::info("RenderInfo without a name.", name.get<std::string>());
                    result = false;
                }
                if (manager.getRenderInfo(name.get<std::string>()))
                {
                    spdlog::info("RenderInfo with name '{0}' already exists.", name.get<std::string>());
                    result = false;
                }

                auto type = info["type"];
                if (type.empty() || (!type.is_string()))
                {
                    spdlog::info("RenderInfo with name '{0}' is missing a type (RenderTypeID) string.", name.get<std::string>());
                    result = false;
                }

                auto source = info["source"];
                if (source.empty() || (!source.is_array()))
                {
                    spdlog::info("RenderInfo with name '{0}' is missing a source array.", name.get<std::string>());
                    result = false;
                }

                if (result)
                {
                    bngfx::IntrusivePtr<bngfx::RenderInfo>& renderInfo = manager.createRenderInfo(name.get<std::string>());
                    renderInfo->setRenderTypeID(manager.getOrCreateRenderTypeIDForName(type.get<std::string>()));

                    if (!loadRenderInfo(infoPath, renderInfo, source, context, manager))
                    {
                        spdlog::info("RenderInfo failed to load '{0}'.", name.get<std::string>());
                        result = false;
                    }
                }

                if (!result)
                {
                    totalResult = false;
                }
            }
        }
        else
        {
            spdlog::info("Json '{0}' is missing 'RenderInfos' array.", path);
            return false;
        }
    }

    return totalResult;
}
} // ns bnshadertools