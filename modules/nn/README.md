## Neural networks

```CPP
struct ManagerHandle;
struct GraphHandle;
struct NodeHandle;
struct TensorHandle;
```

##### wuth
 * Create manager.
 * Create graph.
 * Create tensors.
 * Create DescriptorSetLayouts.
 * Create nodes, assign DescriptorSetLayout.
 * Assign node order, so that they are a graph.
 * Assign tensors to DescriptorSet.
 * Bind DescriptorSet to each node.

#### Manager
 * Manages everything.
 * probably a good idea, to have backing storage allocation strategy.

#### Graph
 * Manager is used to create Graph
 * Contains inference "run" function
 * Contains Nodes, is used to create Nodes
 * getters for input/output nodes
Graph contains activation function.
list of nodes in the graph.
Input nodes.
Output nodes.

#### DescriptorSetLayout
 * defines a layout for a descriptorset
 * inputs/outputs
 * what kind of data comes in (tensors/static data/type of data)
DescriptorSetLayout has a list of objects(tensors) and a name for each "slot" defining layout.
 * I dont think Tensors should define data sizes, maybe dimensions, that it is 3D or 4D etc. but not like width, height, depth stuff.

#### DescriptorSet
 * a blob to be bound to node, which defines the data to be bound to that node
 * must abide to DescriptorSetLayout
 * 
DescriptorSet has a list of objects(tensors) and a name for each object.

#### Nodes
 * Graph is used to create Node
 * Nodes have tensor descriptorsetslayout.
 * possibly bound DescriptorSet that define input/outputs
Each activation of a layer/node binds the defined descriptor set & runs the operation.

#### NodeConfig
 * used to configure node
 * Layer/Node type (fully connected etc)
 * Layer/Node type specific configuration? (does such thing exist?), as descriptorset layout and tensors probably define what kind of data is.
 * Activation function
 * DescriptorSetLayout

#### Tensor
 * Manager is used to create Tensor
Tensors are defined with config, and data can be read/write to them.
Layout and all should be specified, not a necessity, but "nice" thing.

#### TensorConfig
 * Dimensions
 * sizes of dimensions, width height depth defined.
 * Data type

## Randoms

????? Are layers stateless ?????
if there is randomization, could that be assigned to mutable tensors? yes.
mutable tensors sounds awful, but, livable.