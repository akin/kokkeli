
/*
 * Nodes are "layers", in the sense of the normal nn definitions.
 */

#ifndef NN_NODE_H_
#define NN_NODE_H_

#include <nn/array.h>

namespace NNetwork 
{
    class Node
    {
    private:
        Array *m_input = nullptr;
        Array *m_output = nullptr;
    public:
        virtual ~Node() {}
    };
} // ns NNetwork

#endif // NN_NODE_H_
