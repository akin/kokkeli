
#ifndef NN_COMMON_H_
#define NN_COMMON_H_

namespace NNetwork 
{
    class GraphHandle;
    class NodeHandle;
    class TensorHandle;

    /*
    Create tensors.
    Assign tensors to NodeHandles

    Nodes have tensor descriptorsets & descriptorsetslayout.

    DescriptorSetLayout has a list of objects(tensors) and a name for each "slot" defining layout.
    DescriptorSet has a list of objects(tensors) and a name for each object.

    Each activation of a layer/node binds the defined descriptor set & runs the operation.

    ===========
    Tensors are defined with config, and data can be read/write to them.
    Layout and all should be specified, not a necessity, but "nice" thing.

    ===========
    Graph contains activation function.
    list of nodes in the graph.
    list of tensors in the graph.
    Input nodes.
    Output nodes.




    ????? Are layers stateless ?????
    if there is randomization, could that be assigned to mutable tensors? yes.
    mutable tensors sounds awful, but, livable.
    */
} // ns NNetwork

#endif // NN_COMMON_H_
