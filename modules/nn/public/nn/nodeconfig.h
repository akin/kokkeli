
#ifndef NN_NODECONFIG_H_
#define NN_NODECONFIG_H_

#include <string>
#include "tensor.h"

namespace NNetwork 
{
    enum class TensorUsage
    {
        INPUT = 0,
        OUTPUT = 1
    };

    class Dimension
    {
    public:
        int x;
        int y;
        int z;
        int w;
        int i;
    };

    class TensorConfig
    {
    public:
        std::string name;
        Dimension dimensions;
        size_t slot;
        TensorUsage usage;
        
        Tensor *tensor;
    };

    class NodeConfig
    {
    public:
        TensorConfig *tensorConfig;
        size_t tensorConfigCount;
        std::string name;
        std::string type;
    };
} // ns NNetwork

#endif // NN_NODECONFIG_H_
