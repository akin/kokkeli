
#ifndef NN_MANAGER_H_
#define NN_MANAGER_H_

#include "tensor.h"
#include "node.h"
#include "nodeconfig.h"

namespace NNetwork 
{
    class Manager
    {
    public:
        Tensor *createTensor(size_t bufferSize);

        Node *createNode(NodeConfig config);
        void destroyNode(Node *node);

        void allocate();
    };
} // ns NNetwork

#endif // NN_MANAGER_H_
