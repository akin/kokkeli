#ifndef BONA_SCENE_COMMON_H_
#define BONA_SCENE_COMMON_H_

#include <bncommon/intrusiveptr.h>
#include <vector>

namespace bnscene
{
    using namespace bncommon;
} // ns bnscene

#endif // BONA_SCENE_COMMON_H_