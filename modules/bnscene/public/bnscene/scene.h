#ifndef BONA_SCENE_SCENE_H_
#define BONA_SCENE_SCENE_H_

#include <bnscene/common.h>
#include <bngfx/context.h>
#include <bngfx/resources/mesh.h>
#include <bngfx/resourceblock.h>
#include <vector>
#include <functional>

namespace bnscene
{
class Scene
{
public:
    explicit Scene(bngfx::Context& context);
    ~Scene();

    bngfx::Mesh *createMesh();

    bool initialize();
    void destroy();

    bool apply(std::function<bool(bngfx::Mesh*)> func);

    size_t getSize() const;

    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    bngfx::Context& m_context;
    IntrusivePtr<bngfx::ResourceBlock> m_block;
    std::vector<bngfx::Mesh*> m_meshes;
private:
    size_t m_refCount = 0;
};
} // ns bnscene

#endif // BONA_SCENE_SCENE_H_