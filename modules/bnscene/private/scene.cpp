#include <bnscene/scene.h>
#include <bngfx/vkcommon.h>
#include <bngfx/context.h>

namespace bnscene
{
Scene::Scene(bngfx::Context& context)
: m_context(context)
{
    m_block = new bngfx::ResourceBlock(m_context);
    m_block->setMemoryPropertyFlags(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
}

Scene::~Scene()
{
    destroy();
}

bngfx::Mesh *Scene::createMesh()
{
    bngfx::Mesh* mesh = new bngfx::Mesh{};
    m_meshes.push_back(mesh);

    return mesh;
}

bool Scene::initialize()
{
    for(auto* mesh : m_meshes)
    {
        mesh->initialize(*m_block.get());
    }

    return m_block->allocate();
}

void Scene::destroy()
{
    m_block.reset();

    for(auto* mesh : m_meshes)
    {
        delete mesh;
    }
    m_meshes.clear();
}

bool Scene::apply(std::function<bool (bngfx::Mesh*)> func)
{
    for(auto* mesh : m_meshes)
    {
        if(!func(mesh))
        {
            return false;
        }
    }
    return true;
}

size_t Scene::getSize() const
{
    return m_meshes.size();
}

void Scene::incrementReferenceCount()
{
    ++m_refCount;
}

void Scene::decrementReferenceCount()
{
    assert(m_refCount != 0 && "Ref count 0, impossible.");
    --m_refCount;
    if(m_refCount == 0)
    {
        delete this;
    }
}
} // ns bnscene
