#pragma once
#ifndef WINDOW_ABC
#define WINDOW_ABC

#include <kave/platform.h>
#include <kave/common.h>
#include <glfw/glfw3.h>
#include <functional>
#include <kave/common.h>

class Window
{
private:
    GLFWwindow* m_window = nullptr;
    std::string m_title;
    kave::uvec2 m_resolution;
    bool m_resizable = false;
public:
    Window();
    virtual ~Window();

    void setTitle(const std::string& title);

    bool init(kave::uvec2 resolution);
    virtual void deinit();

    void show(bool state = true);

    uint32_t getWidth() const;
    uint32_t getHeight() const;
    kave::SurfaceFormat getSurfaceFormat() const;
    const kave::uvec2& getDimensions() const;

    kave::WindowHandle getHandle();

    bool update();
};

#endif // WINDOW_ABC