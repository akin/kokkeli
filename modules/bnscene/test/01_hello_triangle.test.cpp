
#include <catch2/catch.hpp>
#include <future>
#include "window.h"
#include <fmt/format.h>

#include <kave/context.h>
#include <kave/renderer/basicrenderer.h>

#include <kave/surface.h>
#include <kave/node/node.h>
#include <kave/descriptor/pipelinelayout.h>
#include <kave/descriptor/descriptorlayout.h>

#include <strii/datamanager.h>

using namespace kave;

TEST_CASE("01 Hello Triangle.", "[]") {
	Window window;

	window.setTitle(fmt::format("TestWindow {0}:{1}", __FILE__, __LINE__));

	REQUIRE(window.init({ 800, 600 }));

	Context context;
	Surface surface(context);

	context.addInstanceExtension(VK_KHR_SURFACE_EXTENSION_NAME);
	context.addInstanceExtension(PLATFORM_SURFACE_EXTENSION_NAME);

	strii::DataManager dataManager;
	kave::Settings settings;
	settings.validation = true;
	REQUIRE(context.initInstance(settings));

	const auto devices = context.getDevices();
	REQUIRE(devices.size() > 0);

	REQUIRE(surface.initialize(devices[0], window.getHandle(), VK_PRESENT_MODE_MAILBOX_KHR, { VK_FORMAT_B8G8R8A8_UNORM , VK_COLOR_SPACE_SRGB_NONLINEAR_KHR }, window.getDimensions(), 3));

	context.addDeviceExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	REQUIRE(context.initDevice(devices[0], surface));
	{
		IntrusivePtr<DescriptorLayoutSet> descriptorLayoutSet = new DescriptorLayoutSet(context);
        {
            DescriptorLayout *descriptorLayout = descriptorLayoutSet->add(0);
            descriptorLayout->addBufferBinding(0, 1, VK_SHADER_STAGE_VERTEX_BIT);
            descriptorLayout->addBufferBinding(1, 1, VK_SHADER_STAGE_FRAGMENT_BIT);
        }
		REQUIRE(descriptorLayoutSet->initialize());


		PipelineLayout layout(context);
		REQUIRE(layout.initialize(descriptorLayoutSet.get()));

		std::vector<Shader*> shaders;

		Shader vertex(context);
		{
			IntrusivePtr<strii::Reader> reader = dataManager.read("01_triangle.vert.spv");
			REQUIRE(reader);
			std::vector<char> data(reader->getDataInfo().byteSize);
			REQUIRE(reader->readBytes(data.data(), data.size(), 0) == data.size());

			REQUIRE(vertex.initialize(ShaderType::vertex, data.data(), data.size()));
			shaders.push_back(&vertex);
		}
		Shader fragment(context);
		{
			IntrusivePtr<strii::Reader> reader = dataManager.read("01_triangle.frag.spv");
			REQUIRE(reader);
			std::vector<char> data(reader->getDataInfo().byteSize);
			REQUIRE(reader->readBytes(data.data(), data.size(), 0) == data.size());

			REQUIRE(fragment.initialize(ShaderType::fragment, data.data(), data.size()));
			shaders.push_back(&fragment);
		}

		{
			IntrusivePtr<strii::ImageReader> reader = dataManager.readImage("resources/fubar.png");
			REQUIRE(reader);
		}

		Node node(context);
		node.setPrimitiveType(PrimitiveType::triangles);
		node.setScissor({0.0f, 0.0f}, window.getDimensions());
		node.setViewport({0.0f, 0.0f}, {window.getDimensions().x, window.getDimensions().y}, 0.0f, 1.0f);
		node.setImageFormat(surface.getFormat());

		REQUIRE(node.initialize(shaders.data(), shaders.size(), layout));

		REQUIRE(surface.initializeFramebuffer(node, descriptorLayoutSet.get(), layout));

		BasicRenderer renderer(context);

		window.show();

		size_t frame = 0;
		bool running = true;
		while(true)
		{
			if(!window.update())
			{
				break;
			}
			if(frame == 10000)
			{
				break;
			}
			REQUIRE(surface.draw());

			++frame;
		}
		REQUIRE(context.finish());
	}
}
