# download https://github.com/google/flatbuffers/releases/download/v1.11.0/flatc_windows_exe.zip
# unzip
# generate c++ files
$FileName = "flatc_windows.zip"
Invoke-WebRequest -Uri "https://github.com/google/flatbuffers/releases/download/v1.11.0/flatc_windows_exe.zip" -outfile $FileName
expand-archive -path $FileName -destinationpath '.'
if (Test-Path $FileName) 
{
    Remove-Item $FileName
}