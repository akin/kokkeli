# MOP Mesh / Animation / Scene formats
* against gltf
* for now, a toy
* based on flatbuffers 

#### ????
 * int16 normalized <-> int16 difference?
 * PBR algorithms, think them through
     * needs research and points on what is this.
     * algorithms and rationalisations of "why"

#### Mesh
 * name
 * ID
 * buffers
     - position buffer
     - uv coordinates buffer
         - type (vec1, vec2, vec3, vec4, vec1i, vec2i, vec3i, vec2i, vec4i)
         - data
     - uv2 coordinates buffer
         - type (vec1, vec2, vec3, vec4, vec1i, vec2i, vec3i, vec2i, vec4i)
         - data
     - normal buffer
         - normal (vec3)
         - tangent (vec3)
         - bitangent (vec3)
     - color buffer
         - type (vec1, vec2, vec3, vec4, vec1i, vec2i, vec3i, vec2i, vec4i)
         - data
     - index buffer
         - type (uint8, uint16, uint32)
         - data
 * primitives
     - type (triangle, polygon)
     - start (uint32)
     - count (uint32)
     - material

#### PBRMaterial
 * name
 * ID
 * color
     - constant/texture
 * metalness
     - constant/texture
 * specular
     - constant/texture
 * roughness
     - constant/texture
 * normal
     - constant/texture
 * transparency
     - constant/texture
 * emissive
     - constant/texture
 * glossiness
     - constant/texture
 * heightmap
     - constant/texture

#### Animations
 * Skeletal animations?
 * KeyFrame animations?

#### Lights
 * ???

#### Scene
 * camera
 * light
 * object
 * needs extensions for object attributes etc

#### Level of detail strategies
 * Distributed lod details, around the fileformat

#### Load file strategies
 * first comes low resolution data, for fast gaming
