# automatically generated by the FlatBuffers compiler, do not modify

# namespace: Mop

import flatbuffers

class ImageHeader(object):
    __slots__ = ['_tab']

    @classmethod
    def GetRootAsImageHeader(cls, buf, offset):
        n = flatbuffers.encode.Get(flatbuffers.packer.uoffset, buf, offset)
        x = ImageHeader()
        x.Init(buf, n + offset)
        return x

    # ImageHeader
    def Init(self, buf, pos):
        self._tab = flatbuffers.table.Table(buf, pos)

    # ImageHeader
    def Name(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(4))
        if o != 0:
            return self._tab.String(o + self._tab.Pos)
        return None

    # ImageHeader
    def Format(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(6))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Uint32Flags, o + self._tab.Pos)
        return 0

    # ImageHeader
    def Compression(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(8))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Uint32Flags, o + self._tab.Pos)
        return 0

    # ImageHeader
    def Flags(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(10))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Uint8Flags, o + self._tab.Pos)
        return 0

    # ImageHeader
    def Source(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(12))
        if o != 0:
            return self._tab.String(o + self._tab.Pos)
        return None

    # ImageHeader
    def Offset(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(14))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Uint32Flags, o + self._tab.Pos)
        return 0

    # ImageHeader
    def Length(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(16))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Uint32Flags, o + self._tab.Pos)
        return 0

def ImageHeaderStart(builder): builder.StartObject(7)
def ImageHeaderAddName(builder, name): builder.PrependUOffsetTRelativeSlot(0, flatbuffers.number_types.UOffsetTFlags.py_type(name), 0)
def ImageHeaderAddFormat(builder, format): builder.PrependUint32Slot(1, format, 0)
def ImageHeaderAddCompression(builder, compression): builder.PrependUint32Slot(2, compression, 0)
def ImageHeaderAddFlags(builder, flags): builder.PrependUint8Slot(3, flags, 0)
def ImageHeaderAddSource(builder, source): builder.PrependUOffsetTRelativeSlot(4, flatbuffers.number_types.UOffsetTFlags.py_type(source), 0)
def ImageHeaderAddOffset(builder, offset): builder.PrependUint32Slot(5, offset, 0)
def ImageHeaderAddLength(builder, length): builder.PrependUint32Slot(6, length, 0)
def ImageHeaderEnd(builder): return builder.EndObject()
