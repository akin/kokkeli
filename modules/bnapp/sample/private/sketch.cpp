#ifdef SKETCH
void makeGraph()
{
	ResourceManager resources;

	// Load effect ...
	{
		EffectResourceLoader loader(resources);
		if(!loader.load("defaultshaders", "effects.json"))
		{
			// Failed to load json file
			assert(false);
			return;
		}
	}
	IntrusivePtr<Effect> effect = resources.getEffect("opaqueDefault");

	IntrusivePtr<MaterialType> materialType = new MaterialType;
	materialType->setName("opaqueDefault");
	/// setup MaterialType, "opaque" is the stage at where this particular effect takes place
	/// -it is not yet clear how to set up effect for each stage of pipeline, but Ill figure it out at some point and I feel it should be supported, not just for "opaque stage", but for any other stages as well.
	/// -if a effect is not set for a stage, then this material is not rendered on that stage
	/// -also I am tempted on having materials latch on a graph, and get settings from graph, but on the otherhand, that sounds quite tight coupling.
	materialType.setEffect("opaque", effect);
	/// at initialize MaterialType gets finalized, all the effect reflection should now realize into descriptorsetlaouts for each stage.
	materialType.initialize();

	resources.set(materialType->getName(), materialType);

	/// Material is a child of MaterialType
	/// all the reflections are in MaterialType but the settings descriptorset is in the Material
	/// So each mesh can have unique Material instance of a MaterialType, or they can share one Material.
	IntrusivePtr<Material> material = materialType.create();

	// Load stuff ...
	{
		MeshResourceLoader loader(resources);
		if(!loader.load("helmet", "helmet.gltf"))
		{
			// Failed to load glft file
			assert(false);
			return;
		}
	}

	IntrusivePtr<Mesh> mesh = resources.getMesh("helmet");
	/*
	mesh.setMaterial(material);
	mesh.getMaterial()->getMaterialType().create();
	mesh.setVertexes(vertexes);
	mesh.setIndexes(vertexes);
	*/

	// Setup render graph.. this is bit hacky at the moment, in the future I hope itll get better.
	graph::Graph renderGraph;
	graph::Stage* stage = renderGraph.createStage("opaque");
	graph::Output ouput; // << Screen output
	stage->setOutput(&output);

	// Set hardcoded camera
	mat4 model;
	mat4 view;
	mat4 projection;

	while(true)
	{
		// Prepare to receive graphics related rendering commands, finish staging buffers and cleanup and be ready. flip the frame.
		renderGraph.beginFrame();
		
		// Send camera data? not sure if it has to be here..
		setCamera(view, projection);

		// Send Meshes renderpackages to be rendered in the graph
		renderMesh(model, mesh);

		// Render the graph
		renderGraph.endFrame();
	}
}
#endif