#include "simplepipeline.h"

namespace bnapp
{

SimplePipeline::SimplePipeline(Context& context)
: m_context(context)
{
}

SimplePipeline::~SimplePipeline()
{
    destroy();
}

void SimplePipeline::setOutput(const IntrusivePtr<Node>& output)
{
    m_output = output;
}

bool SimplePipeline::initialize()
{
    // Ok.. so lets start constructing a pipeline.
    // We need "Graph"
    m_graph = new bngfx::Graph(m_context);
    m_graph->setName("SimplePipeline graph");

    // We have 1 pass, 
    //  a) Opaque renderer
    // output to screen

    // render pass
    bngfx::Pass* pass = m_graph->createPass("SimplePipeline Pass");

    // Configure the pass Frame
    bngfx::Frame* frame = pass->getFrame();
    frame->set(0, RGB_888);
    frame->set(1, DEPTH_24);

    // How to set "output" to VkImage / VkImageView / FBO thing?
    frame->setOutput(0, m_output);

    bngfx::SubPass* opaque = pass->createSubPass("opaque");

    opaque->setFrameUsage(0, DONT_CARE, OPTIMAL_WRITE);
    opaque->setFrameUsage(1, DONT_CARE, OPTIMAL_WRITE);

    return true;
}

void SimplePipeline::destroy()
{
}

void SimplePipeline::addChunk(const IntrusivePtr<Chunk>& chunk)
{
    // Chunk is fully initialized, and contains data about rendering.
    m_graph->addChunk(chunk);
}

void SimplePipeline::execute()
{
    m_graph->render();
}

void SimplePipeline::incrementReferenceCount()
{
    ++m_refCount;
}

void SimplePipeline::decrementReferenceCount()
{
    --m_refCount;
    if(m_refCount == 0)
    {
        delete this;
    }
}
} // ns bnapp
