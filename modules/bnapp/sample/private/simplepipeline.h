#ifndef SAMPLE_APP_SIMPLEPIPELINE_H_
#define SAMPLE_APP_SIMPLEPIPELINE_H_

#include <bngfx/common.h>
#include <bngfx/vkcommon.h>
#include <bngfx/render/graph.h>

namespace bnapp
{
using namespace bngfx;

class SimplePipeline
{
public:
    SimplePipeline(Context& context);
    ~SimplePipeline();

    void setOutput(const IntrusivePtr<Node>& output);

    bool initialize();
    void destroy();

    void addChunk(const IntrusivePtr<Chunk>& chunk);

    void execute();
public: // IntrusivePtr
    void incrementReferenceCount();
    void decrementReferenceCount();
private:
    int32_t m_refCount = 0;
private:
    Context& m_context;

    IntrusivePtr<Graph> m_graph;
    IntrusivePtr<Node> m_output;
};
} // ns bnapp

#endif // SAMPLE_APP_SIMPLEPIPELINE_H_