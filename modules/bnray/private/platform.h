
#pragma once
#ifndef PLATFORM_ABC_
#define PLATFORM_ABC_

#ifndef NOMINMAX
# define NOMINMAX
#endif
#ifndef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

#endif // PLATFORM_ABC_