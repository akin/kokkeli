#ifndef COMMON_ABCD_H_
#define COMMON_ABCD_H_

// enable SSE2
#define GLM_FORCE_SSE2 1
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES 1

#define GLM_ENABLE_EXPERIMENTAL 1
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Math
{
    using index = uint32_t;

	using vec2 = glm::vec2;
	using vec3 = glm::vec3;
	using vec4 = glm::vec4;
	
	using uvec2 = glm::uvec2;
	using uvec3 = glm::uvec3;
	using uvec4 = glm::uvec4;

	using mat4 = glm::mat4;
}

#endif // COMMON_ABCD_H_