# Kokkeli
kokkeli was a project to run CPU speed / performance tests..
currently it is a collection of testprojects, ideas, and hubris.

### structure: 
 * cmake 
   * generic cmake scripts
 * ideas
   * ideas to toss around
 * libs
   * 3rd party libraries that are needed
 * modules
   * all the projects and their tests / development units
 * random
   * ideas that have graduated a bit from "ideas" level.
 * tools
   * embedded vcpkg

## Bona
collection of modules for engine(s)
* bngfx graphics abstraction

### dependencies
 * vulkan
 * cmake
 * platform specific ide

### building
 * typical:
   * git clone (recursive)
   * cmake configure
   * set settings
   * cmake generate
 * flag KOKKELI_INSTALL_DEPENDENCIES automatically installs all the required dependencies (according to dependencies.cmake)
 * modules/bnapp/sample is the main bona sample application
   * in visual studio it is located in folder bona-app/app-sample
 * TESTS_* are the test projects for each module (some might even work).

Cmake configuration:
![](cmake.png)

## TODO
 * everything
 * test on elsewhere than windows, active development is done 100% on windows, but everything is made in platform agnostic way.