cmake_minimum_required (VERSION 3.11)

function(compileShaders custom_target_name target_binary_folder source_file_list)
    # TODO unix/linux compatible version!
    
    if (WIN32)
        # WINDOWS
        set(GLSL_VALIDATOR "$ENV{VULKAN_SDK}/Bin/glslangValidator.exe")
        set(SPIRV_DISASSEMBLER "$ENV{VULKAN_SDK}/Bin/spirv-dis.exe")
        set(SPIRV_CROSS "$ENV{VULKAN_SDK}/Bin/spirv-cross.exe")
    elseif(UNIX AND NOT APPLE)
        # LINUX
    elseif(UNIX AND APPLE) 
        # APPLE
    else()
        # ???
    endif()

    foreach(GLSL ${source_file_list})
        get_filename_component(FILE_NAME ${GLSL} NAME)
        set(SPIRV "${target_binary_folder}/${FILE_NAME}.spv")
        set(SPIRV_REFL "${target_binary_folder}/${FILE_NAME}.refl")
        set(SPIRV_DIS "${SPIRV}.dis")
        set(ORIGINAL "${target_binary_folder}/${FILE_NAME}")
        #glslangValidator -G -V -S vertex.glsl -o spirv/vertex.spv
        # -G[ver] create SPIR-V binary, under OpenGL semantics
        # -V[ver] create SPIR-V binary, under Vulkan semantics
        # -S <stage> uses specified stage rather than parsing the file extension
        add_custom_command(
            OUTPUT ${SPIRV}
            COMMAND ${GLSL_VALIDATOR} -V ${GLSL} -o ${SPIRV}
            DEPENDS ${GLSL})
        list(APPEND SPIRV_BINARY_FILES ${SPIRV})
        
        # Reflect the spirv
        add_custom_command(
            OUTPUT ${SPIRV_REFL}
            COMMAND ${SPIRV_CROSS} SPIR-V ${SPIRV} --reflect --vulkan-semantics --output ${SPIRV_REFL}
            DEPENDS ${GLSL})
        list(APPEND SPIRV_BINARY_FILES ${SPIRV_REFL})
        
        # Disassemble the spirv for debugging
        add_custom_command(
            OUTPUT ${SPIRV_DIS}
            COMMAND ${SPIRV_DISASSEMBLER} ${SPIRV} -o ${SPIRV_DIS}
            DEPENDS ${GLSL})
        list(APPEND SPIRV_BINARY_FILES ${SPIRV_DIS})

        # Original for debugging
        add_custom_command(
            OUTPUT ${ORIGINAL}
            COMMAND ${CMAKE_COMMAND} -E copy
                    ${GLSL}
                    ${ORIGINAL})
        list(APPEND SPIRV_BINARY_FILES ${ORIGINAL})
    endforeach(GLSL)

    add_custom_target(
        ${custom_target_name}
        DEPENDS ${SPIRV_BINARY_FILES}
    )
    
    set_target_properties(${custom_target_name} PROPERTIES FOLDER "compile_shaders")
endfunction()