cmake_minimum_required (VERSION 3.11)

if(DEFINED ENV{VCPKG_DEFAULT_TRIPLET} AND NOT DEFINED VCPKG_TARGET_TRIPLET)
    set(VCPKG_TARGET_TRIPLET "$ENV{VCPKG_DEFAULT_TRIPLET}" CACHE STRING "" FORCE)
endif()

if(NOT DEFINED VCPKG_TARGET_TRIPLET)
    message(STATUS "VCPKG: no VCPKG_TARGET_TRIPLET set")
    set(VCPKG_ARCH "x64" CACHE STRING "" FORCE)

    if (WIN32)
        # WINDOWS
        set(VCPKG_OS "windows" CACHE STRING "" FORCE)
    elseif(UNIX AND NOT APPLE)
        # LINUX
        set(VCPKG_OS "linux" CACHE STRING "" FORCE)
    elseif(UNIX AND APPLE) 
        # APPLE
        set(VCPKG_OS "osx" CACHE STRING "" FORCE)
    else()
        # ???
    endif()

    if(DEFINED VCPKG_ARCH AND DEFINED VCPKG_OS)
        set(VCPKG_TARGET_TRIPLET "${VCPKG_ARCH}-${VCPKG_OS}" CACHE STRING "" FORCE)
        message(STATUS "VCPKG: VCPKG_TARGET_TRIPLET assuming ${VCPKG_TARGET_TRIPLET}")
    endif()
endif()

option(VCPKG_EMBEDDED "make the project use embedded vcpkg, not the system." OFF)
if(NOT DEFINED ${CMAKE_TOOLCHAIN_FILE})
    if(NOT DEFINED ENV{VCPKG_ROOT})
		set(VCPKG_EMBEDDED ON)
	endif()
	
    if(VCPKG_EMBEDDED)
		message(STATUS "VCPKG: project embedded")
        set(VCPKG_ROOT ${CMAKE_CURRENT_LIST_DIR}/../tools/vcpkg)
    else()
		message(STATUS "VCPKG: system")
        set(VCPKG_ROOT $ENV{VCPKG_ROOT})
    endif()
    set(VCPKG_TEST ${VCPKG_ROOT}/README.md)

    if(NOT EXISTS ${VCPKG_TEST})
        message(FATAL_ERROR "***** FATAL ERROR: Could not find vcpkg (${VCPKG_TEST}) *****")
    endif()

    if(WIN32)
        set(VCPKG_EXEC ${VCPKG_ROOT}/vcpkg.exe)
        set(VCPKG_BOOTSTRAP ${VCPKG_ROOT}/bootstrap-vcpkg.bat)
    else()
        set(VCPKG_EXEC ${VCPKG_ROOT}/vcpkg)
        set(VCPKG_BOOTSTRAP ${VCPKG_ROOT}/bootstrap-vcpkg.sh)
    endif()

    if(NOT EXISTS ${VCPKG_EXEC})
        message("Bootstrapping vcpkg in ${VCPKG_ROOT}")
        execute_process(COMMAND ${VCPKG_BOOTSTRAP} WORKING_DIRECTORY ${VCPKG_ROOT})
    endif()

    if(NOT EXISTS ${VCPKG_EXEC})
        message(FATAL_ERROR "***** FATAL ERROR: Could not bootstrap vcpkg *****")
    endif()

    set(CMAKE_TOOLCHAIN_FILE ${VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake CACHE STRING "" FORCE)
endif()

# This step is unfortunate, making vcpkg userwise..
option(VCPKG_USERWISE_INTEGRATION "make this vcpkg repo as 'user level' integrated." OFF)
option(VCPKG_REMOVE_USERWISE_INTEGRATION "remove vcpkg repo as 'user level' integrated." OFF)

if(VCPKG_REMOVE_USERWISE_INTEGRATION)
    message("Making 'vcpkg integrate remove'")
    execute_process(COMMAND ${VCPKG_EXEC} integrate remove WORKING_DIRECTORY ${VCPKG_ROOT})
endif()
if(VCPKG_USERWISE_INTEGRATION)
    message("Making 'vcpkg integrate install'")
    execute_process(COMMAND ${VCPKG_EXEC} integrate install WORKING_DIRECTORY ${VCPKG_ROOT})
endif()
